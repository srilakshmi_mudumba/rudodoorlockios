//
//  EAccessListDetails.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 01/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift
import TTLock

class EAccessListDetails: UIViewController {

    let url = Network()
    
    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var ValidLbl: UILabel!
    @IBOutlet weak var ReceivingLbl: UILabel!
    @IBOutlet weak var IssuedLbl: UILabel!
    @IBOutlet weak var TimeLbl: UILabel!
    
    @IBOutlet weak var BtnView: UIView!
    
    var KeyId = Int()
    var Name = String()
    var Validity = Int()
    var ReceivingAcc = String()
    var username = String()
    var TimeIssue = Int()
    var remarks = String()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
              topStatus()
        BtnView.layer.cornerRadius = 10
        
        NameLbl.text = Name
        ReceivingLbl.text = String(ReceivingAcc.dropFirst(5))
        IssuedLbl.text = String(username.dropFirst(5))
        
        let date = Date(timeIntervalSince1970: (Double(TimeIssue) / 1000.0))
         
        let date2 = Date(timeIntervalSince1970: (Double(Validity) / 1000.0))
        
         let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "YYYY-MM-dd HH:mm a"
            
        TimeLbl.text = dateFormatter.string(from: date)
        
          if Validity == 0 {
            ValidLbl.text = "Permanent"
          } else if remarks == "OneTime" {
            ValidLbl.text = "OneTime"
          } else {
        ValidLbl.text = dateFormatter.string(from: date2)
             if date2 < Date() {
            ValidLbl.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
             PostDelete()
        }}
    }
    
    
    
    @IBAction func Back(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func Delete(_ sender: UIButton) {
     self.view.makeToastActivity(.center)
          PostDelete()
        
   }}



extension EAccessListDetails {

  func PostDelete() {
    
    let ClientId = "091f3164004d424abebb4f9c0c822f26"
    let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let KeyID = KeyId
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.DeleteEaccessKey)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "keyId":KeyID,
                "date":date,
            ]
    
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
         }}
  do{
       let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
            print(jsonData)
         DispatchQueue.main.async {
         self.view.hideToastActivity()
        self.view.makeToast("Delete Success",position:.center)
    self.navigationController?.popViewController(animated: true)
        }} catch let error{
    print(error.localizedDescription)
        }
    }.resume()
}}







