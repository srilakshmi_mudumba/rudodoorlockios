//
//  EAccessListVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift
import TTLock

class EAccessListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    let url = Network()
    
    var LockId = Int()
    
    var KeyList = [[String:AnyObject]]()
    
    @IBOutlet weak var eAccessListTable: UITableView!
    @IBOutlet weak var ContainerNOD: UIView!
    
    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        topStatus()
     eAccessListTable.register(UINib(nibName: "eAccessCell", bundle: nil), forCellReuseIdentifier: "eAccessCell")
        
        eAccessListTable.rowHeight = 100
        eAccessListTable.separatorStyle = .none
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        eAccessListTable.addSubview(refreshControl)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.makeToastActivity(.center)
                 EAccessList()
        self.KeyList.removeAll()
        self.eAccessListTable.reloadData()
    }
    
    @objc func refresh(_ sender: AnyObject) {
       self.view.makeToastActivity(.center)
           EAccessList()
       self.KeyList.removeAll()
       self.eAccessListTable.reloadData()
   }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        KeyList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eAccessCell") as! eAccessCell
        cell.NameLBL.text = KeyList[indexPath.row]["keyName"] as? String
        let KeyID = KeyList[indexPath.row]["lockId"] as? Int
        cell.LBL.text = KeyID?.description
        let type = KeyList[indexPath.row]["endDate"] as? Int
        let One = KeyList[indexPath.row]["remarks"] as? String
          if type == 0 {
        cell.LBL2.text = "(Permanent)"
         } else if One == "OneTime" {
        cell.LBL2.text = "(OneTime)"
         } else {
        cell.LBL2.text = "(Timed)"
         }
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     let GetDetals = self.storyboard?.instantiateViewController(withIdentifier: "EAccessListDetails") as! EAccessListDetails
    self.navigationController?.pushViewController(GetDetals, animated: true)
     GetDetals.Name = KeyList[indexPath.row]["keyName"] as! String
     GetDetals.Validity = KeyList[indexPath.row]["endDate"] as!   Int
     GetDetals.ReceivingAcc = KeyList[indexPath.row]["username"] as! String
     GetDetals.username = KeyList[indexPath.row]["senderUsername"] as! String
     GetDetals.TimeIssue = KeyList[indexPath.row]["date"] as! Int
     GetDetals.KeyId = KeyList[indexPath.row]["keyId"] as! Int
     GetDetals.remarks = KeyList[indexPath.row]["remarks"] as! String
     }
    
    
    @IBAction func BackBtn(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
  }
    
    
    
}
extension EAccessListVC {

  func EAccessList() {
    
    let ClientId = "091f3164004d424abebb4f9c0c822f26"
    let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockId
    let pageNo = "1"
    let pageSize = "50"
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.EAccessKeyList)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "lockId":lockId,
                "pageNo":pageNo,
                "pageSize":pageSize,
                "date":date,
            ]
    
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
         }}
  do{
    if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
   if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
    
           if arrayJson.count == 0 {
        DispatchQueue.main.async {
    self.view.hideToastActivity()
    self.refreshControl.endRefreshing()
          let storyboard = UIStoryboard(name: "Main", bundle: nil)
      let startvc = storyboard.instantiateViewController(withIdentifier: "NoDataVC") as! NoDataVC
          self.addChild(startvc)
          self.ContainerNOD.addSubview(startvc.view)
          self.ContainerNOD.constrainToEdges(startvc.view)
          startvc.didMove(toParent: self)
        }} else {
           for json in arrayJson {
          //self.KeyList.append(json)
         self.KeyList.insert(json, at: 0)
              DispatchQueue.main.async {
        self.view.hideToastActivity()
        self.refreshControl.endRefreshing()
          self.eAccessListTable.reloadData()
          }}}
    }}}catch{
      print("Error in get json data")
          }
      }.resume()
  }}







