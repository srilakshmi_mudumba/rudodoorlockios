//
//  AddFingerP.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift

class AddFingerP: UIViewController {

    @IBOutlet weak var EnterName: UITextField!
    @IBOutlet weak var OkView: UIView!
    
      let url = Network()
      var LockId = Int()
      var LockData = String()
      var fingNumber = String()
  
      var FingerPrintID = Int()
    
         override func viewDidLoad() {
            super.viewDidLoad()
            topStatus()

            OkView.layer.cornerRadius = 10
        }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
        
        @IBAction func OKButton(_ sender: UIButton) {
            
            if EnterName.text == "" {
        self.view.makeToast("Plz EnterName",duration:3.0,position:.center)
            } else {
        self.view.makeToastActivity(.center)
              ADDF()
            }
        }
        
        
        func ADDF() {
            
            let startDate = 0
            let endDate = 0
            
        TTLock.addFingerprintStartDate(Int64(startDate), endDate: Int64(endDate), lockData: LockData, progress: { state, remanentPressTimes in
               print(state)
            }, success: { fingerprintNumber in
            self.fingNumber = fingerprintNumber!
                self.PostFingerprint()
        }, failure: { errorCode, errorMsg in
            self.view.hideToastActivity()
        self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration:3.0,position:.center)
            })
        }
        
        
    func PostFingerprint() {
            
        let clientId = "091f3164004d424abebb4f9c0c822f26"
            let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
            let lockId = LockId
            let fingerprintNumber = fingNumber
            let fingerprintName = EnterName.text!
            let startDate = Int(0)
            let endDate = Int(0)
            let date = Date().millisecondsSince1970
             
                
        let url = URL(string:self.url.AddFingerPrint)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
           let parameters: [String : Any] = [
                "clientId":clientId,
                "accessToken":accessToken,
                "lockId":lockId,
                "fingerprintNumber":fingerprintNumber,
                "fingerprintName":fingerprintName,
                "startDate":startDate,
                "endDate":endDate,
                "date":date,
        ]
                
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
                
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
            }
                return
    }
    if let response = response as? HTTPURLResponse{
    guard (200 ... 299) ~= response.statusCode else {
         print("Status code :- \(response.statusCode)")
                print(response)
                return
        }}
    do{
        let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
    let ID =  json.object(forKey: "fingerprintId") as! Int
                self.FingerPrintID = ID
        DispatchQueue.main.async {
        self.view.hideToastActivity()
        self.displayAlertMessage(userMessage: "FingerPrint Added Successfully")
            }
        } catch let error{
      print(error.localizedDescription)
            }
        }.resume()
    }
    
    func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
        let myAlert = UIAlertController(title:"Finger Print", message:userMessage, preferredStyle: UIAlertController.Style.alert);
            
        let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
         self.navigationController?.popViewController(animated: true)
       }
          myAlert.addAction(action);
     self.present(myAlert, animated:true, completion:nil);
    }}
}

