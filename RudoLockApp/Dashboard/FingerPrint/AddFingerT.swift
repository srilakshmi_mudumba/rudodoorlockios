//
//  AddFingerT.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift

class AddFingerT: UIViewController {

    @IBOutlet weak var EnterName: UITextField!
    @IBOutlet weak var StartT: UITextField!
    @IBOutlet weak var EndTime: UITextField!
    
    @IBOutlet weak var OkView: UIView!
       
          let url = Network()
        var LockId = Int()
        var LockData = String()

        var FingPN = String()
        var FingerPID = Int()
       
       var datePicker = UIDatePicker()
       var datePic2 = UIDatePicker()
       
       override func viewDidLoad() {
            super.viewDidLoad()
            topStatus()

         OkView.layer.cornerRadius = 10
        
       let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
           view.addGestureRecognizer(tap)
                  
             creatdatePicker()
             creatdatePicker2()
       }
       
       override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
       }
       
       func creatdatePicker() {
           datePicker.datePickerMode = .dateAndTime
           datePicker.minimumDate = Date()
           datePicker.backgroundColor = .white
           
           let toolbar = UIToolbar()
           toolbar.sizeToFit()
           toolbar.barTintColor = .white
           
           
           let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneac))
           toolbar.setItems([dponebutton], animated: true)
           StartT.inputAccessoryView = toolbar
           StartT.inputView = datePicker
       }
       
       @objc func doneac() {
              let formatter = DateFormatter()
              formatter.timeStyle = DateFormatter.Style.short
              formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
        StartT.text = formatter.string(from: datePicker.date)
           self.view.endEditing(true)
       }
       
       func creatdatePicker2() {
           datePic2.datePickerMode = .dateAndTime
           datePic2.minimumDate = Date()
           datePic2.backgroundColor = .white

           let toolbar = UIToolbar()
           toolbar.sizeToFit()
           toolbar.barTintColor = .white
           
           
           let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
           toolbar.setItems([dponebutton], animated: true)
           EndTime.inputAccessoryView = toolbar
           EndTime.inputView = datePic2
       }
       
       @objc func done() {
           let formatter = DateFormatter()
           formatter.timeStyle = DateFormatter.Style.short
           formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
          if formatter.string(from: datePicker.date) >= formatter.string(from: datePic2.date) {
              DispatchQueue.main.async {
               self.view.endEditing(true)
        self.view.makeToast("Invalid ! End Time should be greater than Start Time")
              }
          } else {
        EndTime.text = formatter.string(from: datePic2.date)
           self.view.endEditing(true)
     }}
       
       
       func convertDateInMiliseconds(date: Date) -> Int {
           let since1970 = date.timeIntervalSince1970
           return Int(since1970 * 1000)
       }
           
           
    @IBAction func OKButton(_ sender: UIButton) {
               
        if EnterName.text == "" || StartT.text == "" || EndTime.text == "" {
        self.view.makeToast("Plz Enter Detalis",duration:3.0,position:.center)
               } else {
     self.view.makeToastActivity(.center)
            ADDF()
    }}
           
           
    func ADDF() {
               
    let startDate = convertDateInMiliseconds(date:datePicker.date)
    let endDate = convertDateInMiliseconds(date:datePic2.date)
               
        TTLock.addFingerprintStartDate(Int64(startDate), endDate: Int64(endDate), lockData: LockData, progress: { state, remanentPressTimes in
               print(state)
            }, success: { fingerprintNumber in
            self.FingPN = fingerprintNumber!
                self.PostFingerprint()
        }, failure: { errorCode, errorMsg in
            self.view.hideToastActivity()
        self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration:3.0,position:.center)
            })
        }
        
           
 func PostFingerprint() {
          
      let clientId = "091f3164004d424abebb4f9c0c822f26"
          let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
          let lockId = LockId
          let fingerprintNumber = FingPN
          let fingerprintName = EnterName.text!
          let startDate = convertDateInMiliseconds(date:datePicker.date)
          let endDate = convertDateInMiliseconds(date:datePic2.date)
          let date = Date().millisecondsSince1970
           
              
      let url = URL(string:self.url.AddFingerPrint)!
          var request = URLRequest(url: url)
          request.httpMethod = "POST"
  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
         let parameters: [String : Any] = [
              "clientId":clientId,
              "accessToken":accessToken,
              "lockId":lockId,
              "fingerprintNumber":fingerprintNumber,
              "fingerprintName":fingerprintName,
              "startDate":startDate,
              "endDate":endDate,
              "date":date,
      ]
              
  request.httpBody = parameters.percentEscaped().data(using: .utf8)
              
      URLSession.shared.dataTask(with: request) { (data, response, error) in
          guard let data = data else {
              if error == nil{
      print(error?.localizedDescription ?? "Unknown Error")
          }
              return
  }
  if let response = response as? HTTPURLResponse{
  guard (200 ... 299) ~= response.statusCode else {
       print("Status code :- \(response.statusCode)")
              print(response)
              return
      }}
  do{
      let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
              print(json)
   let ID =  json.object(forKey: "fingerprintId") as! Int
              self.FingerPID = ID
       DispatchQueue.main.async {
      self.view.hideToastActivity()
        self.displayAlertMessage(userMessage: "FingerPrint Added Successfully")
          }
      } catch let error{
    print(error.localizedDescription)
          }
      }.resume()
  }
    
    func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
        let myAlert = UIAlertController(title:"Finger Print", message:userMessage, preferredStyle: UIAlertController.Style.alert);
            
        let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
         self.navigationController?.popViewController(animated: true)
       }
          myAlert.addAction(action);
     self.present(myAlert, animated:true, completion:nil);
    }}
}

