//
//  GeneratePasscodeVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Parchment

class GeneratePasscodeVC: UIViewController {

    @IBOutlet weak var ContainView: UIView!

    var LockID = Int()
    var KeyVersion = Int()
    var LockData = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
             topStatus()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let first = storyboard.instantiateViewController(withIdentifier: "PassPVC") as! PassPVC
           first.Lockid = LockID
           first.Version = KeyVersion
        let second = storyboard.instantiateViewController(withIdentifier: "PassOTVC") as! PassOTVC
          second.Lockid = LockID
          second.Version = KeyVersion
        let Third = storyboard.instantiateViewController(withIdentifier: "PassTVC") as! PassTVC
          Third.Lockid = LockID
          Third.Version = KeyVersion
        let Fourth = storyboard.instantiateViewController(withIdentifier: "PassCustomVC") as! PassCustomVC
          Fourth.Lockid = LockID
          Fourth.Version = KeyVersion
          Fourth.Lockdata = LockData


        let pagingViewController = PagingViewController(viewControllers: [
          first,second,Third,Fourth ])
        addChild(pagingViewController)
        ContainView.addSubview(pagingViewController.view)
        ContainView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
        
    }
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    

}
