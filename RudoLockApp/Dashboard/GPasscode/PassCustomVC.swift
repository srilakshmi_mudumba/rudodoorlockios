//
//  PassCustomVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 21/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock

class PassCustomVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var StartDate: UITextField!
    @IBOutlet weak var EndDate: UITextField!
    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var Passcode: UITextField!
    @IBOutlet weak var setView: UIView!
    
    
    let url = Network()
    
    var Lockid = Int()
    var Version = Int()
    var Lockdata = String()
    
    var datePicker = UIDatePicker()
    var datePic2 = UIDatePicker()
    
    
    var customPasscode = String()
    
    @IBOutlet weak var StView: UIView!
    
     override func viewDidLoad() {
           super.viewDidLoad()
            topStatus()
       let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
           view.addGestureRecognizer(tap)
                  
             creatdatePicker()
             creatdatePicker2()
            Passcode.delegate = self
           setView.layer.cornerRadius = 20
           
       }
       
       override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
       }
    
      func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          let updatedString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
          return updatedString.count <= 9
      }
       
       func creatdatePicker() {
           datePicker.datePickerMode = .dateAndTime
           datePicker.minimumDate = Date()
           datePicker.backgroundColor = .white
           
           let toolbar = UIToolbar()
           toolbar.sizeToFit()
           toolbar.barTintColor = .white
           
           
           let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneac))
           toolbar.setItems([dponebutton], animated: true)
           StartDate.inputAccessoryView = toolbar
           StartDate.inputView = datePicker
       }
       
       @objc func doneac() {
        
        let formatter = DateFormatter()
        formatter.timeStyle = DateFormatter.Style.short
        formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
        StartDate.text = formatter.string(from: datePicker.date)
           self.view.endEditing(true)
       }
       
       func creatdatePicker2() {
           datePic2.datePickerMode = .dateAndTime
           datePic2.minimumDate = Date()
           datePic2.backgroundColor = .white

           let toolbar = UIToolbar()
           toolbar.sizeToFit()
           toolbar.barTintColor = .white
           
           
           let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
           toolbar.setItems([dponebutton], animated: true)
           EndDate.inputAccessoryView = toolbar
           EndDate.inputView = datePic2
       }
       
       @objc func done() {
        
          let formatter = DateFormatter()
          formatter.timeStyle = DateFormatter.Style.short
          formatter.dateFormat = "YYYY-MM-dd HH:mm a"
           
        if formatter.string(from: datePicker.date) >= formatter.string(from: datePic2.date) {
               DispatchQueue.main.async {
                self.view.endEditing(true)
            self.view.makeToast("Invalid ! End Time should be greater than Start Time")
               }
           } else {
        EndDate.text = formatter.string(from: datePic2.date)
           self.view.endEditing(true)
        }}
       
       
       func convertDateInMiliseconds(date: Date) -> Int {
           let since1970 = date.timeIntervalSince1970
           return Int(since1970 * 1000)
       }

    
    @IBAction func Switch(_ sender: UISwitch) {
        
        if sender.isOn {
            StView.isHidden = true
            StartDate.text = "\(0)"
            EndDate.text = "\(0)"
        } else {
            StView.isHidden = false
        }}
    
    
    @IBAction func SetPasscode(_ sender: UIButton) {
        
        if Name.text! == "" || Passcode.text! == "" || StartDate.text! == "" || EndDate.text! == "" {
       self.view.makeToast("Enter Details", duration: 3.0, position: .center)
        } else if Passcode.text!.count == 3 {
    self.view.makeToast("Enter 4-9 digits", duration: 3.0, position: .center)
         } else {
     self.view.makeToastActivity(.center)
        if StView.isHidden == true {
          let keyboardPwd = Passcode.text!
          let startD = StartDate.text!
          let endD = EndDate.text!
            
    TTLock.createCustomPasscode(keyboardPwd, startDate: Int64(startD)!, endDate: Int64(endD)!, lockData:self.Lockdata, success: {
            self.customPasscode = keyboardPwd
          self.AddPasscodePermanent()
            }, failure: { errorCode, errorMsg in
          self.view.hideToastActivity()
    self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration: 3.0,position:.center)
        })
            
        } else {
             let keyboardPwd = Passcode.text!
             let startDate = convertDateInMiliseconds(date:datePicker.date)
             let endDate = convertDateInMiliseconds(date:datePic2.date)
    TTLock.createCustomPasscode(keyboardPwd, startDate: Int64(startDate), endDate: Int64(endDate), lockData:self.Lockdata, success: {
                    self.customPasscode = keyboardPwd
                 self.AddPasscode()
            }, failure: { errorCode, errorMsg in
                     self.view.hideToastActivity()
          self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration: 3.0,position:.center)
                 })
        }}}
    
    func displayAlertMessage(userMessage:String) {
          DispatchQueue.main.async{
      let myAlert = UIAlertController(title:"Custom Passcode", message:userMessage, preferredStyle: UIAlertController.Style.alert);
                  
      let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
      self.navigationController?.popViewController(animated: true)
          }
          myAlert.addAction(action);
      self.present(myAlert, animated:true, completion:nil);
      }}


    
}
extension PassCustomVC {
   
   func AddPasscode() {
       
       let clientId = "091f3164004d424abebb4f9c0c822f26"
       let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
       let lockId = Lockid
       let keyboardPwd = Passcode.text!
       let keyboardPwdName = Name.text!
       let startDate = convertDateInMiliseconds(date:datePicker.date)
       let endDate = convertDateInMiliseconds(date:datePic2.date)
       let date = Date().millisecondsSince1970
       
        let url = URL(string:self.url.GetCustomPasscode)!
               var request = URLRequest(url: url)
               request.httpMethod = "POST"
               request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
               let parameters: [String : Any] = [
                   "clientId":clientId,
                   "accessToken":accessToken,
                   "lockId":lockId,
                   "keyboardPwd":keyboardPwd,
                   "keyboardPwdName":keyboardPwdName,
                   "startDate":startDate,
                   "endDate":endDate,
                   "addType":1,
                   "date":date,
               ]
       
       request.httpBody = parameters.percentEscaped().data(using: .utf8)
       
       URLSession.shared.dataTask(with: request) { (data, response, error) in
               guard let data = data else {
               if error == nil{
           print(error?.localizedDescription ?? "Unknown Error")
              }
               return
          }
           if let response = response as? HTTPURLResponse{
           guard (200 ... 299) ~= response.statusCode else {
               print("Status code :- \(response.statusCode)")
               print(response)
               return
            }}
     do{
         let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
               print(json)
        
    if json.object(forKey: "errmsg") as? String == "Invalid Parameter." {
        let msg = json.object(forKey: "description")
            DispatchQueue.main.async {
        self.view.hideToastActivity()
        self.view.makeToast("\(msg ?? " start Time should be greater than end Time")", duration: 3.0, position: .bottom)
        }} else {
            DispatchQueue.main.async {
          self.view.hideToastActivity()
        self.displayAlertMessage(userMessage: "Successfully Add Passcode")
//         let keyboardPwdId =  json.object(forKey: "keyboardPwdId") as! Int
//            print(keyboardPwdId)
          }
            }} catch let error{
       print(error.localizedDescription)
           }
       }.resume()
   }
    
    func AddPasscodePermanent() {
        
        let clientId = "091f3164004d424abebb4f9c0c822f26"
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = Lockid
        let keyboardPwd = Passcode.text!
        let keyboardPwdName = Name.text!
        let startDate = StartDate.text!
        let endDate = EndDate.text!
        let date = Date().millisecondsSince1970
        
         let url = URL(string:self.url.GetCustomPasscode)!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                let parameters: [String : Any] = [
                    "clientId":clientId,
                    "accessToken":accessToken,
                    "lockId":lockId,
                    "keyboardPwd":keyboardPwd,
                    "keyboardPwdName":keyboardPwdName,
                    "startDate":startDate,
                    "endDate":endDate,
                    "addType":1,
                    "date":date,
                ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                if error == nil{
            print(error?.localizedDescription ?? "Unknown Error")
               }
                return
           }
            if let response = response as? HTTPURLResponse{
            guard (200 ... 299) ~= response.statusCode else {
                print("Status code :- \(response.statusCode)")
                print(response)
                return
             }}
      do{
          let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        self.displayAlertMessage(userMessage: "Successfully Add Passcode")
        }
        //let keyboardPwdId =  json.object(forKey: "keyboardPwdId") as! Int
            } catch let error{
        print(error.localizedDescription)
            }
        }.resume()
    }}

 
