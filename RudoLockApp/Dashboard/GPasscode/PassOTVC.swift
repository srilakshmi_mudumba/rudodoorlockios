//
//  PassOTVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift


class PassOTVC: UIViewController {

    @IBOutlet weak var BtnView: UIView!
    
    @IBOutlet weak var EnterName: UITextField!
    
    let url = Network()
    
    var Lockid = Int()
    var Version = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        BtnView.layer.cornerRadius = 20
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func SendBtn(_ sender: UIButton) {
        
        if EnterName.text == "" {
        self.view.makeToast("Enter Name", duration: 3.0, position: .bottom)
        } else {
      self.view.makeToastActivity(.center)
           PostPasscodeOT()
   }}
    
}
extension PassOTVC {
   
func PostPasscodeOT() {
       
       let clientId = "091f3164004d424abebb4f9c0c822f26"
       let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
       let lockId = Lockid
       let keyboardPwdVersion = Version
       let keyboardPwdType = "1"
       let keyboardPwdName = EnterName.text!
       let startDate = Date().millisecondsSince1970
       let endDate = Date().millisecondsSince1970
       let date = Date().millisecondsSince1970
       
        let url = URL(string:self.url.GetPasscodeP)!
               var request = URLRequest(url: url)
               request.httpMethod = "POST"
               request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
               let parameters: [String : Any] = [
                   "clientId":clientId,
                   "accessToken":accessToken,
                   "lockId":lockId,
                   "keyboardPwdVersion":keyboardPwdVersion,
                   "keyboardPwdType":keyboardPwdType,
                   "keyboardPwdName":keyboardPwdName,
                   "startDate":startDate,
                   "endDate":endDate,
                   "date":date,
               ]
       
       request.httpBody = parameters.percentEscaped().data(using: .utf8)
       
       URLSession.shared.dataTask(with: request) { (data, response, error) in
               guard let data = data else {
               if error == nil{
           print(error?.localizedDescription ?? "Unknown Error")
              }
               return
          }
           if let response = response as? HTTPURLResponse{
           guard (200 ... 299) ~= response.statusCode else {
               print("Status code :- \(response.statusCode)")
               print(response)
               return
            }}
     do{
         let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
               print(json)
         let keyboardPwdId =  json.object(forKey: "keyboardPwdId") as! Int
         let keyboardPwd =  json.object(forKey: "keyboardPwd") as! String
       
           DispatchQueue.main.async {
            self.view.hideToastActivity()
       let Show = self.storyboard?.instantiateViewController(withIdentifier: "ShowPasscodePopUp") as! ShowPasscodePopUp
           Show.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
               Show.passcode = keyboardPwd
               Show.PasscodeID = keyboardPwdId
         self.present(Show, animated: true)
               }
           } catch let error{
       print(error.localizedDescription)
           }
       }.resume()
   }}

