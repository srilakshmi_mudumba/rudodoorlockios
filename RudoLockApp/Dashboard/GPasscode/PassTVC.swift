//
//  PassTVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class PassTVC: UIViewController {
   
    
    let url = Network()
    
    @IBOutlet weak var BtnView: UIView!
    @IBOutlet weak var EnterName: UITextField!

    @IBOutlet weak var StartTime: UITextField!
    @IBOutlet weak var EndTime: UITextField!
    
    var Lockid = Int()
    var Version = Int()

  
    var datePicker = UIDatePicker()
    var datePic2 = UIDatePicker()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         topStatus()
    let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
               
          creatdatePicker()
          creatdatePicker2()
        
        BtnView.layer.cornerRadius = 20
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func creatdatePicker() {
        datePicker.datePickerMode = .dateAndTime
        datePicker.backgroundColor = .white
        datePicker.minimumDate = Date()
        
        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.barTintColor = .white
        
        
        let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneac))
        toolbar.setItems([dponebutton], animated: true)
        StartTime.inputAccessoryView = toolbar
        StartTime.inputView = datePicker
    }
    
    @objc func doneac() {
        
        let formatter = DateFormatter()
        formatter.timeStyle = DateFormatter.Style.short
        formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
        StartTime.text = formatter.string(from: datePicker.date)
        self.view.endEditing(true)
    }
    
    func creatdatePicker2() {
        datePic2.datePickerMode = .dateAndTime
        datePic2.backgroundColor = .white
        datePic2.minimumDate = Date()

        let toolbar = UIToolbar()
        toolbar.sizeToFit()
        toolbar.barTintColor = .white
        
        
        let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
        toolbar.setItems([dponebutton], animated: true)
        EndTime.inputAccessoryView = toolbar
        EndTime.inputView = datePic2
    }
    
    @objc func done() {
        
         let formatter = DateFormatter()
         formatter.timeStyle = DateFormatter.Style.short
         formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
          if formatter.string(from: datePicker.date) >= formatter.string(from: datePic2.date) {
              DispatchQueue.main.async {
               self.view.endEditing(true)
           self.view.makeToast("Invalid ! End Time should be greater than Start Time")
              }
          } else {
        EndTime.text = formatter.string(from: datePic2.date)
        self.view.endEditing(true)
    }}
    
    
    func convertDateInMiliseconds(date: Date) -> Int {
        let since1970 = date.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    
    @IBAction func SendBtn(_ sender: UIButton) {
        
        if StartTime.text! == "" || EndTime.text! == "" {
    self.view.makeToast("Select Dates", duration: 3.0, position: .bottom)
    } else if EnterName.text == "" {
    self.view.makeToast("Enter Name", duration: 3.0, position: .bottom)
    } else {
    self.view.makeToastActivity(.center)
       GetPassTime()
    }}
       
}
extension PassTVC {
   
 func GetPassTime() {
       
       let clientId = "091f3164004d424abebb4f9c0c822f26"
       let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
       let lockId = Lockid
       let keyboardPwdVersion = Version
       let keyboardPwdType = "3"
       let keyboardPwdName = EnterName.text!
       let startDate = convertDateInMiliseconds(date:datePicker.date)
       let endDate = convertDateInMiliseconds(date:datePic2.date)
       let date = Date().millisecondsSince1970
    
       
        let url = URL(string:self.url.GetPasscodeP)!
               var request = URLRequest(url: url)
               request.httpMethod = "POST"
               request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
               let parameters: [String : Any] = [
                   "clientId":clientId,
                   "accessToken":accessToken,
                   "lockId":lockId,
                   "keyboardPwdVersion":keyboardPwdVersion,
                   "keyboardPwdType":keyboardPwdType,
                   "keyboardPwdName":keyboardPwdName,
                   "startDate":startDate,
                   "endDate":endDate,
                   "date":date,
               ]
       
       request.httpBody = parameters.percentEscaped().data(using: .utf8)
       
       URLSession.shared.dataTask(with: request) { (data, response, error) in
               guard let data = data else {
               if error == nil{
           print(error?.localizedDescription ?? "Unknown Error")
              }
               return
          }
           if let response = response as? HTTPURLResponse{
           guard (200 ... 299) ~= response.statusCode else {
               print("Status code :- \(response.statusCode)")
               print(response)
               return
            }}
     do{
         let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
               print(json)
          if json.object(forKey: "errmsg") as? String == "Invalid Parameter." {
             let msg = json.object(forKey: "description")
            DispatchQueue.main.async {
        self.view.hideToastActivity()
        self.view.makeToast("\(msg ?? " start Time should be greater than end Time")", duration: 3.0, position: .bottom)
            }
          } else {
         let keyboardPwdId =  json.object(forKey: "keyboardPwdId") as! Int
         let keyboardPwd =  json.object(forKey: "keyboardPwd") as! String
            
            DispatchQueue.main.async {
            self.view.hideToastActivity()
       let Show = self.storyboard?.instantiateViewController(withIdentifier: "ShowPasscodePopUp") as! ShowPasscodePopUp
           Show.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
               Show.passcode = keyboardPwd
               Show.PasscodeID = keyboardPwdId
         self.present(Show, animated: true)
               }
        }} catch let error{
       print(error.localizedDescription)
           }
       }.resume()
   }}

