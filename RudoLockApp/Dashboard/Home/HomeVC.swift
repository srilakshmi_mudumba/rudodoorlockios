//
//  HomeVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 19/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift

class HomeVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var HomeTable: UITableView!
    @IBOutlet weak var ContainerView: UIView!
    
    let url = Network()
    
    var LockList = [LockModel]()
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topStatus()
        
        HomeTable.delegate = self
        HomeTable.dataSource = self
        
        self.HomeTable.register(UINib(nibName: "LockCell", bundle: nil), forCellReuseIdentifier: "LockCell")
        
        HomeTable.rowHeight = 100
        HomeTable.separatorStyle = .none
        
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        HomeTable.addSubview(refreshControl)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.makeToastActivity(.center)
        PostLockCommonKey()
        self.LockList.removeAll()
        self.HomeTable.reloadData()
    }
    
    
    @objc func refresh(_ sender: AnyObject) {
        self.view.makeToastActivity(.center)
        PostLockCommonKey()
        self.LockList.removeAll()
        self.HomeTable.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        LockList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "LockCell") as! LockCell
        cell.LBL1.text = "\(LockList[indexPath.row].lockName)"
        cell.LBL2.text = "\(LockList[indexPath.row].lockId)"
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if LockList[indexPath.row].userType == "110301" {
            let GoToDashBoard = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
            GoToDashBoard.LockID = LockList[indexPath.row].lockId
            GoToDashBoard.LockBattery = LockList[indexPath.row].electricQuantity
            GoToDashBoard.LockData = LockList[indexPath.row].LockData
            GoToDashBoard.KeyID = LockList[indexPath.row].keyId
            GoToDashBoard.keyboardPwdVersion = LockList[indexPath.row].keyboardPwdVersion
            GoToDashBoard.Remark = LockList[indexPath.row].remarks
            GoToDashBoard.LockN = LockList[indexPath.row].lockName
            GoToDashBoard.specialValue = LockList[indexPath.row].specialValue
            
            
            self.navigationController?.pushViewController(GoToDashBoard, animated: true)
        } else {
            if LockList[indexPath.row].keyRight == Int(1) {
                let GoToDashBoard = self.storyboard?.instantiateViewController(withIdentifier: "DashboardVC") as! DashboardVC
                GoToDashBoard.LockID = LockList[indexPath.row].lockId
                GoToDashBoard.LockBattery = LockList[indexPath.row].electricQuantity
                GoToDashBoard.LockData = LockList[indexPath.row].LockData
                GoToDashBoard.KeyID = LockList[indexPath.row].keyId
                GoToDashBoard.keyboardPwdVersion = LockList[indexPath.row].keyboardPwdVersion
                GoToDashBoard.Remark = LockList[indexPath.row].remarks
                GoToDashBoard.KeyRight = LockList[indexPath.row].keyRight
                GoToDashBoard.LockN = LockList[indexPath.row].lockName
                GoToDashBoard.specialValue = LockList[indexPath.row].specialValue
                
                self.navigationController?.pushViewController(GoToDashBoard, animated: true)
            } else {
                let GoToDashBoard = self.storyboard?.instantiateViewController(withIdentifier: "DashBoard2VC") as! DashBoard2VC
                GoToDashBoard.LockID = LockList[indexPath.row].lockId
                GoToDashBoard.LockBattery = LockList[indexPath.row].electricQuantity
                GoToDashBoard.CULockData = LockList[indexPath.row].LockData
                GoToDashBoard.KeyID = LockList[indexPath.row].keyId
                GoToDashBoard.Remark = LockList[indexPath.row].remarks
                GoToDashBoard.LockN = LockList[indexPath.row].lockName
                
                self.navigationController?.pushViewController(GoToDashBoard, animated: true)
            }} }
    
    
    func currentDateTimeInMiliseconds() -> Int {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    
    @IBAction func SideMenuBtn(_ sender: UIBarButtonItem) {
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let startvc = storyboard.instantiateViewController(withIdentifier: "SideTableVC") as! SideTableVC
        self.addChild(startvc)
        self.view.addSubview(startvc.view)
        startvc.didMove(toParent: self)
    }
    
}

extension HomeVC {
    
    func PostLockCommonKey() {
        if Utils.isInternetAvailable() {
            
            let ClientId = "091f3164004d424abebb4f9c0c822f26"
            let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
            let date = Date().millisecondsSince1970
            let pageNo = "1"
            let pageSize = "100"
            
            let url = URL(string:self.url.KeyList)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "pageNo":pageNo,
                "pageSize":pageSize,
                "date":date,
            ]
            
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error != nil{
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.refreshControl.endRefreshing()
                        }
                        print(error?.localizedDescription ?? "Unknown Error")
                    }
                    return
                }
                if let response = response as? HTTPURLResponse{
                    guard (200 ... 299) ~= response.statusCode else {
                        print("Status code :- \(response.statusCode)")
                        print(response)
                        return
                    }}
                do{
                    if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
                        if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
                            if arrayJson.count == 0 {
                                DispatchQueue.main.async {
                                    self.ContainerView.isHidden = false
                                    self.view.hideToastActivity()
                                    self.refreshControl.endRefreshing()
                                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                    let startvc = storyboard.instantiateViewController(withIdentifier: "ButtonViewVC") as! ButtonViewVC
                                    self.addChild(startvc)
                                    self.ContainerView.addSubview(startvc.view)
                                    self.ContainerView.constrainToEdges(startvc.view)
                                    startvc.didMove(toParent: self)
                                }} else {
                                    for json in arrayJson {
                                        if arrayJson.count > 0 {
                                            self.LockList.append(LockModel(json))
                                            DispatchQueue.main.async {
                                                self.ContainerView.isHidden = true
                                                self.view.hideToastActivity()
                                                self.refreshControl.endRefreshing()
                                                self.HomeTable.reloadData()
                                            }}
                                    }}}
                    }} catch let error {
                        print(error.localizedDescription)
                    }
            }.resume()
        }else {
            self.view.hideToastActivity()
            self.view.makeToast("Check Your Network Connection")
        }}
}





