//
//  LockCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 19/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class LockCell: UITableViewCell {

    @IBOutlet weak var CellView: UIView!
    @IBOutlet weak var PicImage: UIImageView!
    
    @IBOutlet weak var PicImgView: UIView!
    
    @IBOutlet weak var LBL1: UILabel!
    @IBOutlet weak var LBL2: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
                
        CellView.layer.cornerRadius = 10
        CellView.layer.borderWidth = 1
        CellView.layer.borderColor = UIColor.black.cgColor
        
        PicImgView.layer.cornerRadius = PicImgView.frame.height/2
        PicImgView.layer.borderWidth = 1
        PicImgView.layer.borderColor = UIColor.white.cgColor
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    
    
}
