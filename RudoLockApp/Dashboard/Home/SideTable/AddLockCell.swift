//
//  AddLockCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 10/10/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class AddLockCell: UITableViewCell {

    @IBOutlet weak var NameLbl: UILabel!
    @IBOutlet weak var AddLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
