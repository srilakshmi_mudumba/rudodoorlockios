//
//  AddLockVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 24/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import TTLockGateway
import TTLockDFU
import Toast_Swift
import MJExtension


class AddLockVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate {
    
    
    let url = Network()
    
    var scanModelArray: [TTScanModel]?
    var dataArray: [TTScanModel]?
    
    var LockAlis = String()
    
    @IBOutlet weak var AddLockTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()
        AddLockTable.rowHeight = 50
        //AddLockTable.tableFooterView = UIView()
        setupData()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        TTLock.stopScan()
        
        RunLoop .cancelPreviousPerformRequests(withTarget: self)
    }
    
    func setupData() {
        self.view.makeToastActivity(.center)
        var scanModelArray: [AnyHashable]? = []
        self.scanModelArray = scanModelArray as? [TTScanModel]
        TTLock.startScan({ scanModel in
            var dataArrayHasContainScanModel = false
            for containModel in scanModelArray ?? [] {
                guard let containModel = containModel as? TTScanModel else {
                    continue
                }
                if containModel.lockMac == scanModel?.lockMac {
                    dataArrayHasContainScanModel = true
                    containModel.mj_setKeyValues(scanModel)
                    break
                }
            }
            if !dataArrayHasContainScanModel {
                if let scanModel = scanModel {
                    scanModelArray!.append(scanModel)
                }
            }
            
            
            //            var disappearDataArray: [AnyHashable]? = []
            //            for model in scanModelArray ?? [] {
            //                guard let model = model as? TTScanModel else {
            //                    continue
            //                }
            //                if model.date.timeIntervalSinceNow < -5 {
            //                    disappearDataArray?.append(model)
            //                }
            //            }
            DispatchQueue.main.async {
                self.view.hideToastActivity()
                self.scanModelArray = scanModelArray as? [TTScanModel]
            }
            //        })
            //            scanModelArray = scanModelArray?.filter({ !disappearDataArray!.contains($0) })
        })
        resetDataArrayEachSecond()
    }
    
    
    
    @objc func resetDataArrayEachSecond() {
        let sort0 = NSSortDescriptor(key: "isInited", ascending: true)
        let sort1 = NSSortDescriptor(key: "RSSI", ascending: false)
        var dataArray = self.scanModelArray
        if dataArray!.isEmpty {
            self.view.hideToastActivity()
            self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")
        }
        self.view.hideToastActivity()
        dataArray = (dataArray as NSArray?)?.sortedArray(using: [sort0, sort1]) as? [AnyHashable] as? [TTScanModel] ?? dataArray
        
        self.dataArray = dataArray
        self.AddLockTable.reloadData()
        perform(#selector(resetDataArrayEachSecond), with: nil, afterDelay: 1)
    }
    
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        return newLength <= 15
    }
    
    // table method:
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "AddLockCell") as! AddLockCell
        let scanModel = dataArray![indexPath.row]
        cell.NameLbl.text = scanModel.lockName
        cell.AddLbl.text = scanModel.isInited ? "" : "+"
        cell.NameLbl.textColor = scanModel.isInited ? UIColor.lightGray : UIColor.black
        cell.AddLbl.textColor = scanModel.isInited ? UIColor.white : UIColor.blue
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let scanModel = dataArray![indexPath.row]
        if scanModel.isInited == false {
            
            let alert = UIAlertController(title: "Set The LockName", message: "Enter a Lockname", preferredStyle: .alert)
            alert.addTextField { (textField) in
                textField.placeholder = "Enter Name"
                textField.text = scanModel.lockName
            }
            alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
                self.view.makeToastActivity(.center)
                let textField = alert?.textFields![0] as UITextField?
                if textField != nil {
                    self.LockAlis = "\(textField?.text! ?? scanModel.lockName)"
                } else {
                    self.LockAlis = scanModel.lockName
                }
                
                var dict: [AnyHashable : Any] = [:]
                dict["lockMac"] = scanModel.lockMac
                dict["lockName"] = scanModel.lockName
                dict["lockVersion"] = scanModel.lockVersion
                
                TTLock.initLock(withDict: dict, success: { lockData in
                    let LockAlias = self.LockAlis
                    let ClientId = "091f3164004d424abebb4f9c0c822f26"
                    let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
                    let date = Date().millisecondsSince1970
                    let url = URL(string:self.url.lockInit)!
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST"
                    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    
                    let parameters: [String : Any] = [
                        "clientId":ClientId,
                        "accessToken":AccessToken,
                        "lockData":lockData!,
                        "lockAlias":LockAlias,
                        "date":date,
                    ]
                    request.httpBody = parameters.percentEscaped().data(using: .utf8)
                    
                    URLSession.shared.dataTask(with: request) { (data, response, error) in
                        guard let data = data else {
                            if error != nil {
                                TTLock.resetLock(withLockData: lockData,success: {
                                    print("reset lock success")
                                },failure: { errorCode, errorMsg in
                                    self.view.makeToast("Error", duration: 0.3,position: .bottom)
                                    print("\(errorMsg ?? "")")
                                })
                                self.view.hideToastActivity()
                                self.view.makeToast("Error", duration: 0.3,position: .bottom)
                            }
                            return
                        }
                        if let response = response as? HTTPURLResponse {
                            guard (200 ... 299) ~= response.statusCode else {
                                self.view.hideToastActivity()
                                self.view.makeToast("Status code :- \(response.statusCode)", duration: 0.3,position: .bottom)
                                print(response)
                                return
                            }}
                        do{
                            let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                            print(json)
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                if let viewController = self.navigationController?.viewControllers.first(where: {$0 is HomeVC }) {
                                    self.navigationController?.popToViewController(viewController, animated: true)
                                }}
                        } catch let error{
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.navigationController?.popViewController(animated: true)
                                print(error.localizedDescription)
                            }
                        }
                    }.resume()
                }, failure: { errorCode, errorMsg in
                    self.view.hideToastActivity()
                    self.view.makeToast("Error", duration: 0.3,position: .bottom)
                    print("\(errorMsg ?? "")")
                })
            }))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
}
