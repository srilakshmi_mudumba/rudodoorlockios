//
//  EditProfileVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 09/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

protocol SendNickData {
    func userNick(NIC:String)
}

class EditProfileVC: UIViewController, UITableViewDelegate, UITableViewDataSource,SendNicknameData,UIImagePickerControllerDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var EditTable: UITableView!
    
    @IBOutlet weak var ImgView: UIView!
    
    @IBOutlet weak var ProfileImg: UIImageView!
    @IBOutlet weak var UserNameLBL: UILabel!
    @IBOutlet weak var AccountNoLBL: UILabel!
    
    var NickN = String()
    
    var delNIC: SendNickData?
    let imagePicker = UIImagePickerController()
    
    let titleArry = ["Nickname"]
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()
        
        let ProName = UserDefaults.standard.string(forKey: "Username")!
        AccountNoLBL.text = ProName
        
        ImgView.layer.cornerRadius = ImgView.frame.height/2
        ProfileImg.layer.borderWidth = 1
        ProfileImg.layer.borderColor = UIColor.white.cgColor
        ProfileImg.layer.backgroundColor = UIColor.white.cgColor
        self.ProfileImg.layer.cornerRadius = ProfileImg.frame.size.width / 2
        
        EditTable.tableFooterView = UIView()
        EditTable.rowHeight = 55
        
        
        let tapGesture = UITapGestureRecognizer()
        tapGesture.addTarget(self, action: #selector(EditProfileVC.openGallery(tapGesture:)))
        ProfileImg.isUserInteractionEnabled = true
        ProfileImg.addGestureRecognizer(tapGesture)
    }
    
    
    @objc func openGallery(tapGesture: UITapGestureRecognizer){
        setUpImagePicker()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        if NickN.isEmpty {
            let ProName = UserDefaults.standard.string(forKey: "Username")!
            UserNameLBL.text = ProName
        } else {
            
            UserNameLBL.text = NickN
        }
        delNIC?.userNick(NIC:NickN)
        
    }
    
    
    func setUpImagePicker(){
        if UIImagePickerController.isSourceTypeAvailable(.savedPhotosAlbum) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            imagePicker.isEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }}
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        ProfileImg.image = image
        dismiss(animated: true, completion: nil)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        titleArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "EditPCell") as! EditPCell
        cell.TitleLBL.text = titleArry[indexPath.row]
        cell.accessoryType = .disclosureIndicator
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let NicN = self.storyboard?.instantiateViewController(withIdentifier: "NickNameVC") as! NickNameVC
            NicN.nickname = UserNameLBL.text!
            NicN.deleN = self
            self.navigationController?.pushViewController(NicN, animated: true)
            
        //        case 1:
        //   let Email = self.storyboard?.instantiateViewController(withIdentifier: "EmailVC") as! EmailVC
        //     self.navigationController?.pushViewController(Email, animated: true)
        //
        //       case 2:
        //    let Reset = self.storyboard?.instantiateViewController(withIdentifier: "ResetPasswordVC") as! ResetPasswordVC
        //   self.navigationController?.pushViewController(Reset, animated: true)
        
        //       case 2:
        //    let Qa = self.storyboard?.instantiateViewController(withIdentifier: "SecurityQVC") as! SecurityQVC
        //    self.navigationController?.pushViewController(Qa, animated: true)
        default:
            break;
        }
    }
    
    
    func userTypeData(Name: String) {
        NickN = Name
    }
    
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
        
    }
    
}
