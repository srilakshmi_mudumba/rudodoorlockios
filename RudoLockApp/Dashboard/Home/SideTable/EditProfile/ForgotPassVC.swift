//
//  ForgotPassVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 09/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class ForgotPassVC: UIViewController {

    @IBOutlet weak var PhoneNumber: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var ConfirmPassword: UITextField!
    @IBOutlet weak var VerifyCode: UITextField!
    @IBOutlet weak var GetCodeOutlet: UIButton!
    
    let url = Network()
    
    var SessionID = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         topStatus()

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
           self.view.endEditing(true)
    }
       
    
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func GetCode(_ sender: UIButton) {
      
        if PhoneNumber.text == "" {
      self.view.makeToast("Enter Number")
        }
        else {
    self.view.makeToastActivity(.center)
       GenerateCode()
        }
}
    
   
    
    @IBAction func ResetPassword(_ sender: UIButton) {
        
         if PhoneNumber.text == "" {
    self.view.makeToast("Invalid Number")
        }
         else if Password.text == "" {
    self.view.makeToast("Password")
         } else if Password.text!.count <= 6 {
     self.view.makeToast("Password Must be >6 in character")
         } else if ConfirmPassword.text != Password.text {
    self.view.makeToast("confirm password and password are not same")
         } else if VerifyCode.text == "" {
    self.view.makeToast("Enter verify code")
        
         } else if self.SessionID == "" {
    self.view.makeToast("Generate code")
         } else {
    self.view.makeToastActivity(.center)
     VerificationCode()
        }
        
    }
    
    
    func GenerateCode() {
       
      let Password = PhoneNumber.text!
      let key = "d4ca29e1-ede5-11ea-9fa5-0200cd936042"
        
      URLSession.shared.dataTask(with: URL(string:"https://2factor.in/API/V1/"+key+"/SMS/+91"+Password+"/AUTOGEN")!) {
        (data, rsponse, error) in
         guard error == nil else {
        self.view.hideToastActivity()
                return
            }
         guard let data = data else {
           return
           }
      do{
        let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
          print(jsonData)
      let Session = jsonData.object(forKey: "Details") as! String
        self.SessionID = Session
        DispatchQueue.main.async {
        self.view.hideToastActivity()
          }
      }   catch{
       self.view.hideToastActivity()
        print("Error in get json data")
        }
                    
    }.resume()
 }
    
      func VerificationCode() {
       
      let key = "d4ca29e1-ede5-11ea-9fa5-0200cd936042"
      let SID = self.SessionID
      let OTP = VerifyCode.text!
        
     URLSession.shared.dataTask(with: URL(string:"https://2factor.in/API/V1/"+key+"/SMS/VERIFY/"+SID+"/"+OTP)!) {

        (data, rsponse, error) in
         guard error == nil else {
                return
            }
         guard let data = data else {
             return
           }
      do{
        let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
          print(jsonData)
      let Session = jsonData.object(forKey: "Details") as! String
        if Session == "OTP Matched" {
            DispatchQueue.main.async {
           self.PostResetPassword()
            }
        } else {
            DispatchQueue.main.async {
            self.view.hideToastActivity()
      self.view.makeToast("Invalid Code!")
        }}
            }catch{
                DispatchQueue.main.async {
         self.view.hideToastActivity()
           self.view.makeToast("Error")
                }
        }
                    
    }.resume()
        }
}
extension ForgotPassVC {

    func PostResetPassword() {
    
    let password = ConfirmPassword.text!.md5
    let username = PhoneNumber.text!
    let ClientId = "091f3164004d424abebb4f9c0c822f26"
    let ClientSecret = "3c815de27069538163d6c776036a8b3c"
    let date =  Date().millisecondsSince1970
    
     let url = URL(string:self.url.ForgotPassword)!
            var request = URLRequest(url: url)
        request.httpMethod = "POST"
  request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "clientSecret":ClientSecret,
                "username":"rudo_"+username,
                "password":password,
                "date":date,
            ]
        
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
         }}
  do{
      let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print(json)
    if json.object(forKey: "errmsg") as? String == "failed or means no" {
        DispatchQueue.main.async {
         self.view.hideToastActivity()
    self.view.makeToast("Invalid User",duration:3.0,position:.center)
        }
    } else {
         DispatchQueue.main.async {
         self.view.hideToastActivity()
    self.displayMessage(userMessage: "Reset Password Successfully")
        }
            }
    } catch let error{
        self.view.hideToastActivity()
    print(error.localizedDescription)
        }
    }.resume()
}
    
    func displayMessage(userMessage:String) -> Void {
             DispatchQueue.main.async {
         let alertController = UIAlertController(title: "Reset Password", message: userMessage, preferredStyle: .alert)
    
        let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
            DispatchQueue.main.async  {
        self.navigationController?.popViewController(animated: true)
        }
            }
        alertController.addAction(OKAction)
        self.present(alertController, animated: true, completion:nil)
         }
 }}
