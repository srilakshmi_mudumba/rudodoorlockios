//
//  NickNameVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 09/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift

protocol SendNicknameData {
    func userTypeData(Name:String)
}

class NickNameVC: UIViewController {

    
    @IBOutlet weak var NicknameTxt: UITextField!
    
    var nickname = String()
    
    var TypeN = String()
    
    var deleN : SendNicknameData?
    
    override func viewDidLoad() {
        super.viewDidLoad()
             topStatus()
        NicknameTxt.text = nickname

        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
               view.addGestureRecognizer(tap)
    }
    
    @IBAction func SaveBtn(_ sender: UIBarButtonItem) {
        if NicknameTxt.text == "" {
    self.view.makeToast("Enter NickName", duration: 0.3, position: .center)
        } else {
        TypeN  = NicknameTxt.text!
    UserDefaults.standard.set(TypeN, forKey: "Username")
      deleN?.userTypeData(Name:TypeN)
    self.navigationController?.popViewController(animated: true)
        }
    }
    
    @IBAction func backB(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    

}
