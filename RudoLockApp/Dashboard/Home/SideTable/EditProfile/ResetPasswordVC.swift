//
//  ResetPasswordVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 09/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift

class ResetPasswordVC: UIViewController {
    
    
    @IBOutlet weak var CurrentPassTxt: UITextField!
    @IBOutlet weak var NewPassTxt: UITextField!
    @IBOutlet weak var ConfirmPassTxt: UITextField!
    
    @IBOutlet weak var BtnnView: UIView!
        
    override func viewDidLoad() {
        super.viewDidLoad()
           topStatus()
        BtnnView.layer.cornerRadius = 20
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
        
    
    }
    
    
    @IBAction func SaveBtn(_ sender: UIButton) {
        if CurrentPassTxt.text == "" || NewPassTxt.text == "" || ConfirmPassTxt.text == "" {
    self.view.makeToast("Enter Details",duration:0.3,position: .center)
        }
        else {
           print("callApi")
        }
    }
    
    
    
    @IBAction func ForgotPassword(_ sender: UIButton) {
        
        let forgot = self.storyboard?.instantiateViewController(withIdentifier: "ForgotPassVC") as! ForgotPassVC
   self.navigationController?.pushViewController(forgot, animated: true)
    }
    
    @IBAction func Back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}
