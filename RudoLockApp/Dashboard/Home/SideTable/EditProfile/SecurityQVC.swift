//
//  SecurityQVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 09/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class SecurityQVC: UIViewController {

    @IBOutlet weak var Question1: UITextField!
    @IBOutlet weak var Question2: UITextField!
    @IBOutlet weak var Question3: UITextField!
    
    @IBOutlet weak var Answer1: UITextField!
    @IBOutlet weak var Answer2: UITextField!
    @IBOutlet weak var Answer3: UITextField!
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
           topStatus()
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)

        let tap1 = UITapGestureRecognizer(target: self, action:#selector(LBL1Tap))
        View1.addGestureRecognizer(tap1)
        
        let tap2 = UITapGestureRecognizer(target: self, action:#selector(LBL2Tap))
        View2.addGestureRecognizer(tap2)
        
        let tap3 = UITapGestureRecognizer(target: self, action:#selector(LBL3Tap))
        View3.addGestureRecognizer(tap3)
        
     }

    @objc func LBL1Tap() {
    let alertaction  =  UIAlertController.init(title:"Please Select a Question", message:nil, preferredStyle: .actionSheet)
        let backView = alertaction.view.subviews.last?.subviews.last
               // backView?.layer.cornerRadius = 10.0
                backView?.backgroundColor = UIColor.white
                backView?.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
        let action1 = UIAlertAction(title: "Where did you go the first time you flew on a plane?", style: .default) { (_) -> Void in
            self.Question1.text = "Where did you go the first time you flew on a plane?"
        }
              
        let action2 = UIAlertAction(title: "Which phone number do you remember most?", style: .default) { (_) -> Void in
            self.Question1.text = "Which phone number do you remember most from your childhood?"
                 }
        let action3 = UIAlertAction(title: "What was the name of your first pet?", style: .default) { (_) -> Void in
            self.Question1.text = "What was the name of your first pet?"
        }
              
        let action4 = UIAlertAction(title: "In which year was your father born?", style: .default) { (_) -> Void in
            self.Question1.text = "In which year was your father born?"
                 }
        let action5 = UIAlertAction(title: "What was the first film you saw in the theater?", style: .default) { (_) -> Void in
            self.Question1.text = "What was the first film you saw in the theater?"
        }
        
        let action6 =  UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
          action6.setValue(UIColor.red, forKey: "titleTextColor")
              
                    alertaction.addAction(action1)
                    alertaction.addAction(action2)
                    alertaction.addAction(action3)
                    alertaction.addAction(action4)
                    alertaction.addAction(action5)
                    alertaction.addAction(action6)
    self.present(alertaction, animated: true, completion: nil)
          }
   
    
    @objc func LBL2Tap() {
       let alertaction  =  UIAlertController.init(title:"Please Select a Question", message:nil, preferredStyle: .actionSheet)
           let backView = alertaction.view.subviews.last?.subviews.last
                  // backView?.layer.cornerRadius = 10.0
                   backView?.backgroundColor = UIColor.white
                   backView?.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
           let action1 = UIAlertAction(title: "Where did you go the first time you flew on a plane?", style: .default) { (_) -> Void in
               self.Question2.text = "Where did you go the first time you flew on a plane?"
           }
                 
           let action2 = UIAlertAction(title: "Which phone number do you remember most?", style: .default) { (_) -> Void in
               self.Question2.text = "Which phone number do you remember most from your childhood?"
                    }
           let action3 = UIAlertAction(title: "What was the name of your first pet?", style: .default) { (_) -> Void in
               self.Question2.text = "What was the name of your first pet?"
           }
                 
           let action4 = UIAlertAction(title: "In which year was your father born?", style: .default) { (_) -> Void in
               self.Question2.text = "In which year was your father born?"
                    }
           let action5 = UIAlertAction(title: "What was the first film you saw in the theater?", style: .default) { (_) -> Void in
               self.Question2.text = "What was the first film you saw in the theater?"
           }
           
           let action6 =  UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
             action6.setValue(UIColor.red, forKey: "titleTextColor")
                 
                       alertaction.addAction(action1)
                       alertaction.addAction(action2)
                       alertaction.addAction(action3)
                       alertaction.addAction(action4)
                       alertaction.addAction(action5)
                       alertaction.addAction(action6)
       self.present(alertaction, animated: true, completion: nil)
             }
    
    
    @objc func LBL3Tap() {
       let alertaction  =  UIAlertController.init(title:"Please Select a Question", message:nil, preferredStyle: .actionSheet)
           let backView = alertaction.view.subviews.last?.subviews.last
                  // backView?.layer.cornerRadius = 10.0
                   backView?.backgroundColor = UIColor.white
                   backView?.tintColor = #colorLiteral(red: 0.2549019754, green: 0.2745098174, blue: 0.3019607961, alpha: 1)
           let action1 = UIAlertAction(title: "Where did you go the first time you flew on a plane?", style: .default) { (_) -> Void in
               self.Question3.text = "Where did you go the first time you flew on a plane?"
           }
                 
           let action2 = UIAlertAction(title: "Which phone number do you remember most?", style: .default) { (_) -> Void in
               self.Question3.text = "Which phone number do you remember most from your childhood?"
                    }
           let action3 = UIAlertAction(title: "What was the name of your first pet?", style: .default) { (_) -> Void in
               self.Question3.text = "What was the name of your first pet?"
           }
                 
           let action4 = UIAlertAction(title: "In which year was your father born?", style: .default) { (_) -> Void in
               self.Question3.text = "In which year was your father born?"
                    }
           let action5 = UIAlertAction(title: "What was the first film you saw in the theater?", style: .default) { (_) -> Void in
               self.Question3.text = "What was the first film you saw in the theater?"
           }
           
           let action6 =  UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
             action6.setValue(UIColor.red, forKey: "titleTextColor")
                 
                       alertaction.addAction(action1)
                       alertaction.addAction(action2)
                       alertaction.addAction(action3)
                       alertaction.addAction(action4)
                       alertaction.addAction(action5)
                       alertaction.addAction(action6)
       self.present(alertaction, animated: true, completion: nil)
             }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
}
