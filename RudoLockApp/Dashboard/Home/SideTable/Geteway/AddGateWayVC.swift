//
//  AddGateWayVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 05/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import TTLockGateway
import TTLockDFU
import Toast_Swift
import CoreLocation
import SystemConfiguration.CaptiveNetwork

class AddGateWayVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate{
    
    var locationManager: CLLocationManager?
    
    var WifiName = String()
    
    @IBOutlet weak var ChooseGTabel: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
         
        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        
        ChooseGTabel.delegate = self
        ChooseGTabel.dataSource = self
        ChooseGTabel.rowHeight = 130
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
//        case 0:
//       let cell = tableView.dequeueReusableCell(withIdentifier: "SelectGatwayCell") as! SelectGatwayCell
//         cell.GImage.image = #imageLiteral(resourceName: "G1")
//         cell.GLbl.text = "G1"
//            return cell
        case 0:
       let cell = tableView.dequeueReusableCell(withIdentifier: "SelectGatwayCell") as! SelectGatwayCell
            cell.GImage.image = #imageLiteral(resourceName: "G2")
            cell.GLbl.text = "G2"
            return cell
        default:
            break;
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

       let Next =  self.storyboard?.instantiateViewController(withIdentifier: "AddG2VC") as! AddG2VC
        self.navigationController?.pushViewController(Next, animated: true)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedAlways {
            let ssid = self.getWiFiName()
            if ssid != nil {
            self.WifiName = ssid!
        } else {
             self.WifiName = "nil"
         }}
    }
    
    
    @IBAction func backB(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    
    func getWiFiName() -> String? {
        var ssid: String?

        if let interfaces = CNCopySupportedInterfaces() as NSArray? {
          for interface in interfaces {
            if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
              ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
              break
            }}}
        return ssid
      }
}
