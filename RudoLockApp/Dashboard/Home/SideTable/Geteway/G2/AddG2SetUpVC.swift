//
//  AddG2SetUpVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 08/12/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLockGateway
import TTLock

class AddG2SetUpVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SendSSID {

   let url = Network()
        
        var WifiN = String()
        var Uid = Int()
        var gatewayId = Int()
        var gatwayMac = String()
       

        @IBOutlet weak var NetworkTable: UITableView!
        @IBOutlet weak var EnterPassword: UITextField!
        @IBOutlet weak var GatewayName: UITextField!
            
        override func viewDidLoad() {
            super.viewDidLoad()
              topStatus()
         }
        
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            self.view.endEditing(true)
        }
        
        @IBAction func backbtn(_ sender: UIBarButtonItem) {
         self.navigationController?.popViewController(animated: true)
        }
        
        func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
         }
        
        func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "WiFinameCell") as! WiFinameCell
             cell.Wifiname.placeholder = WifiN
             cell.accessoryType = .disclosureIndicator
            return cell
        }
        
        func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
            DispatchQueue.main.async(execute: {
          let Show = self.storyboard?.instantiateViewController(withIdentifier: "ChooseWifiVC") as! ChooseWifiVC
                Show.dele = self
            Show.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
          self.present(Show, animated: true, completion:nil)
            })
        }
        
          @IBAction func NextButtom(_ sender: UIButton) {
            
            if WifiN == "" || EnterPassword.text == "" || GatewayName.text == "" {
            self.view.makeToast("Plz Enter Details",position:.center)
            } else {
             Progress()
         }}
        
        
        func SSID(ssid: String) {
            WifiN = ssid
          NetworkTable.reloadData()
        }
        
        
         func Progress() {
             self.view.makeToastActivity(.center)
            
             var ginfoDic: [AnyHashable : Any] = [:]
                 ginfoDic["SSID"] = self.WifiN
                 ginfoDic["wifiPwd"] = self.EnterPassword.text!
                 ginfoDic["uid"] = UserDefaults.standard.string(forKey: "uid")!
                 ginfoDic["userPwd"] = UserDefaults.standard.string(forKey: "Password")

                 ginfoDic["gatewayName"] = self.GatewayName.text!
                       
         TTGateway.initializeGateway(withInfoDic: ginfoDic, block: { systemInfoModel, status  in
            
            if status == TTGatewayStatus.notConnect || status == TTGatewayStatus.disconnect {
                self.view.hideToastActivity()
                self.notConnect()
             }
            else if status == TTGatewayStatus.success {
            TTGateway.disconnectGateway(withGatewayMac: self.gatwayMac, block: nil)
                self.isUploadSuccess(systemInfoModel)
            }
           else if status == TTGatewayStatus.wrongSSID {
                self.view.hideToastActivity()
                self.view.makeToast("WiFi name error")
             }
            else if status == TTGatewayStatus.wrongSSID {
                self.view.hideToastActivity()
                self.view.makeToast("WiFi password error")
                return
            } else {
                self.view.hideToastActivity()
                self.view.makeToast("Failure")
            }})
    //        self.view.hideToastActivity()
    //        self.view.makeToast("TO")
     }
        
        func notConnect() {
        if let viewController = self.navigationController?.viewControllers.first(where: {$0 is AddG2VC }) {
        self.navigationController?.popToViewController(viewController, animated:true)
            self.view.makeToast("Connection time out,exited add mode")
         }}
        
        func isUploadSuccess(_ systemInfoModel: TTSystemInfoModel?) {
                    
                let ClientId = "091f3164004d424abebb4f9c0c822f26"
                let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
                let gatewayNetMac = gatwayMac
                let date = Date().millisecondsSince1970
                      
                let url = URL(string:self.url.InitGateway)!
                    var request = URLRequest(url: url)
                        request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                    let parameters: [String : Any] = [
                        "clientId":ClientId,
                        "accessToken":AccessToken,
                        "gatewayNetMac":gatewayNetMac,
                        "date":date,
                ]
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
                      
         URLSession.shared.dataTask(with: request) { (data, response, error) in
                    guard let data = data else {
                    if error != nil{
                self.view.hideToastActivity()
            print(error?.localizedDescription ?? "Unknown Error")
                        }
            return
        }
           if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print(response)
                        return
                }}
        do{
            let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
            self.gatewayId = jsonData.object(forKey: "gatewayId") as! Int
            
                let url = URL(string:self.url.UplodeGatewayDetails)!
                    var request = URLRequest(url: url)
                    request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                let parameters: [String : Any] = [
                    "clientId":ClientId,
                    "accessToken":AccessToken,
                    "gatewayId":self.gatewayId,
                    "modelNum":systemInfoModel!.modelNum!,
                    "hardwareRevision":systemInfoModel!.hardwareRevision!,
                    "firmwareRevision":systemInfoModel!.firmwareRevision!,
                    "networkName":self.WifiN,
                    "date":date,
            ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
                                    
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                    guard let data = data else {
                    if error != nil{
                self.view.hideToastActivity()
        print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
            guard (200 ... 299) ~= response.statusCode else {
                    print(response)
            return
        }}
            do{
        let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
        if jsonData.object(forKey: "errmsg") as! String == "none error message or means yes" {
                    DispatchQueue.main.async {
                self.view.hideToastActivity()
            if let viewController = self.navigationController?.viewControllers.first(where: {$0 is GatewayVC }) {
            self.navigationController?.popToViewController(viewController, animated: false)
            }}
        } else {
            DispatchQueue.main.async {
                self.view.hideToastActivity()
                self.view.makeToast("Error in Uploading Details",position:.center)
            }
            }} catch let error {
                    self.view.hideToastActivity()
                print(error.localizedDescription)
            }}.resume()
                } catch let error {
                self.view.hideToastActivity()
            print(error.localizedDescription)
        }}.resume()
     }
        
     func GetUId() {
            
              let ClientId = "091f3164004d424abebb4f9c0c822f26"
              let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
              let date = Date().millisecondsSince1970
              
               let url = URL(string:self.url.GetUid)!
                      var request = URLRequest(url: url)
                      request.httpMethod = "POST"
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                      let parameters: [String : Any] = [
                          "clientId":ClientId,
                          "accessToken":AccessToken,
                          "date":date,
                      ]
              
              request.httpBody = parameters.percentEscaped().data(using: .utf8)
              
              URLSession.shared.dataTask(with: request) { (data, response, error) in
                      guard let data = data else {
                      if error != nil{
                  print(error?.localizedDescription ?? "Unknown Error")
                     }
                      return
            }
                 if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print(response)
                    return
                 }}
            do{
              let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                self.Uid = json.object(forKey: "uid") as! Int
                print(self.Uid)
             UserDefaults.standard.set(self.Uid, forKey: "PUid")
               }catch{
                print("Error in get json data")
                    }
                }.resume()
    }}
