//
//  AddG2VC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 11/11/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class AddG2VC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func NextBtn(_ sender: UIButton) {
        
        let Next = self.storyboard?.instantiateViewController(withIdentifier: "ChooseGatewayVC") as! ChooseGatewayVC
        self.navigationController?.pushViewController(Next, animated: true)
    }
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
