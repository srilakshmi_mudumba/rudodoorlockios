//
//  ChooseGatewayVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 11/11/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLockGateway
import TTLock
import SystemConfiguration.CaptiveNetwork
import CoreLocation


class ChooseGatewayVC: UIViewController,UITableViewDelegate,UITableViewDataSource,CLLocationManagerDelegate {
 
    var locationManager: CLLocationManager?

    @IBOutlet weak var ChooseGTable: UITableView!
    
     var dataArray: [AnyHashable]?
     var isUseful = false
    
     var WifiName = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
         topStatus()

        locationManager = CLLocationManager()
        locationManager?.delegate = self
        locationManager?.requestAlwaysAuthorization()
        
        ChooseGTable.rowHeight = 40
        if dataArray == nil {
            dataArray = [AnyHashable]()
        }
        TTGateway.startScanGateway({ model in
            if (model!.rssi == 127) {
               return ;
           }
            var contain = false
            var isExchangePosition = false
            for containModel in self.dataArray ?? [] {
                guard let containModel = containModel as? GatewayModel else {
                    continue
                }
                if containModel.gatewayMac == model!.gatewayMac {
                if containModel.rssi != model!.rssi {
                contain = false
            self.dataArray!.removeAll { $0 as AnyObject === containModel as AnyObject }
                    break
                }
             containModel.searchTime = Date()
                contain = true
                 break
            }}
                
            if !contain {
                let containModel = GatewayModel()
                containModel.gatewayName = model!.gatewayName
                containModel.gatewayMac = model!.gatewayMac
                containModel.isInited = false
                containModel.rssi = model!.rssi
                containModel.searchTime = Date()
                isExchangePosition = true
                   
                var sortIndex = 0
                for i in 0..<self.dataArray!.count {
                    let containmodel = self.dataArray![i] as? GatewayModel
                    if model!.rssi > containmodel!.rssi {
                        sortIndex = i
                        break
                    }
                    sortIndex = i + 1
                }
                self.dataArray!.insert(containModel, at: sortIndex)
            }})
         isUseful = true
         reloadView()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        isUseful = false
        TTGateway.stopScanGateway()
    }
    
    
    func reloadView() {

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(Int64(1 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC), execute: {
                 if self.isUseful == true {
            let tempArray = self.dataArray ?? []
                for model in tempArray {
                    guard let model = model as? GatewayModel else {
                        continue
                    }
        if model.searchTime!.timeIntervalSinceNow < -3 {
                self.dataArray!.removeAll { $0 as AnyObject === model as AnyObject }
                    }
                }
                self.reloadView()
            }})
        ChooseGTable.reloadData()
    }
    
    
    @IBAction func backBtn(_ sender: UIBarButtonItem) {
           self.navigationController?.popViewController(animated: true)
       }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedAlways {
                   let ssid = self.getWiFiName()
                    if ssid != nil {
                  self.WifiName = ssid!
                  } else {
                self.WifiName = "nil"
            }}
    }
    
    func getWiFiName() -> String? {
      var ssid: String?

      if let interfaces = CNCopySupportedInterfaces() as NSArray? {
        for interface in interfaces {
          if let interfaceInfo = CNCopyCurrentNetworkInfo(interface as! CFString) as NSDictionary? {
            ssid = interfaceInfo[kCNNetworkInfoKeySSID as String] as? String
            break
          }}}
      return ssid
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataArray!.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ChooseGCell") as! ChooseGCell
           let model =  dataArray![indexPath.row] as? GatewayModel
        cell.NameLbl.text = model!.gatewayName
        cell.AddLbl.text = model!.isInited ? "" : "+"
        cell.NameLbl.textColor = model!.isInited ? UIColor.lightGray : UIColor.black
        cell.AddLbl.textColor = model!.isInited ? UIColor.white : UIColor.blue

        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if WifiName == "nil" {
        self.view.makeToast("Please ensure your Phone is connected to the desired WiFi Network",position:.center)
                   } else {
        let model = dataArray![indexPath.row] as? GatewayModel
          self.view.makeToastActivity(.center)
        TTGateway.connectGateway(withGatewayMac: model?.gatewayMac, block: { connectstatus in
            if connectstatus == TTGatewayConnectStatus.success {
                TTGateway.stopScanGateway()
                DispatchQueue.main.async {
        let Next =  self.storyboard?.instantiateViewController(withIdentifier: "AddG2SetUpVC") as! AddG2SetUpVC
            Next.WifiN = self.WifiName
            Next.gatwayMac = model!.gatewayMac!
    self.navigationController?.pushViewController(Next, animated: true)
                }}
            else {
                self.view.hideToastActivity()
            }
        })}
    

}}
