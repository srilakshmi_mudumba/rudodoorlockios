//
//  ChooseWifiVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 30/11/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLockGateway
import TTLock


protocol SendSSID {
    func SSID(ssid:String)
}

class ChooseWifiVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    var ssidArr: [AnyHashable]?
    
    @IBOutlet weak var ChooseTable: UITableView!
    
    var SelectSSID = String()
    
    var dele : SendSSID?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.ssidArr = [AnyHashable]()
        self.ChooseTable.delegate = self
        self.ChooseTable.dataSource = self
        ChooseTable.rowHeight = 50
            ScanWifi()
        }

    @IBAction func CancelB(_ sender: UIBarButtonItem) {
        self.dismiss(animated: true, completion: nil)
    }
    
    func ScanWifi() {
         self.view.makeToastActivity(.center)

        TTGateway.scanWiFiByGateway({ isFinished, WiFiArr, status in
        if status == TTGatewayStatus.notConnect || status == TTGatewayStatus.disconnect {
                self.view.hideToastActivity()
        self.dismiss(animated: true, completion: nil)
        self.view.makeToast("connection time out")
            return
        }
             if WiFiArr!.count > 0 {
                let arr = WiFiArr as? [AnyHashable]
                    if isFinished == true {
                self.view.hideToastActivity()
                }
                    self.ssidArr = arr
                self.ChooseTable.reloadData()
    }})
}
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         ssidArr!.count
     }

     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "CWificell") as! CWificell
        let dic = ssidArr![indexPath.row] as? [AnyHashable : Any]
        cell.WifiLBL.text = dic?["SSID"] as? String
        
         return cell
     }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .checkmark
        let dic = ssidArr![indexPath.row] as? [AnyHashable : Any]
         SelectSSID = (dic?["SSID"] as? String)!
        let userSelectData = SelectSSID
         dele?.SSID(ssid: userSelectData)
        self.dismiss(animated: true, completion: nil)
    }

  func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        tableView.cellForRow(at: indexPath)?.accessoryType = .none
    }

}
