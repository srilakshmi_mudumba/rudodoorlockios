//
//  GatewayVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 05/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift

class GatewayVC: UIViewController,UITableViewDelegate,UITableViewDataSource{

    @IBOutlet weak var GateWayTable: UITableView!
    
    @IBOutlet weak var ContainerV: UIView!
    
     let url = Network()
    var GateWayList = [[String:AnyObject]]()
    
    override func viewDidLoad() {
           super.viewDidLoad()
             topStatus()
        GateWayTable.register(UINib(nibName: "eAccessCell", bundle: nil), forCellReuseIdentifier: "eAccessCell")
           
        GateWayTable.rowHeight = 100
        GateWayTable.separatorStyle = .none
           
       }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.makeToastActivity(.center)
                 PostGateWayList()
        self.GateWayList.removeAll()
        self.GateWayTable.reloadData()

    }
       
       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           GateWayList.count
       }
       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           let cell = tableView.dequeueReusableCell(withIdentifier: "eAccessCell") as! eAccessCell
        cell.NameLBL.text! = GateWayList[indexPath.row]["gatewayName"] as! String
        let Online = GateWayList[indexPath.row]["isOnline"] as! Int
        if Online == 0 {
        cell.LBL.text! = "Offline"
        } else {
        cell.LBL.text! = "Online"
        }
        cell.LBL2.text! = GateWayList[indexPath.row]["networkName"] as! String
           return cell
       }
    
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let move = self.storyboard?.instantiateViewController(withIdentifier: "G2selectedVC") as! G2selectedVC
        move.gatewayID = GateWayList[indexPath.row]["gatewayId"] as! Int
        move.gatewayName = GateWayList[indexPath.row]["gatewayName"] as! String
        move.isonline = GateWayList[indexPath.row]["isOnline"] as! Int
        move.Wifi = GateWayList[indexPath.row]["networkName"] as! String
        self.navigationController?.pushViewController(move, animated: true)
     }
    
    
    @IBAction func DotTap(_ sender: UIBarButtonItem) {
        
        let alertaction  =  UIAlertController.init(title:nil, message:nil, preferredStyle: .actionSheet)
            let backView = alertaction.view.subviews.last?.subviews.last
                backView?.layer.cornerRadius = 10.0
                backView?.backgroundColor = UIColor.white
                backView?.tintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        let action1 = UIAlertAction(title: "Add Gateway", style: .default) { (_) -> Void in
            let Add = self.storyboard?.instantiateViewController(withIdentifier: "AddGateWayVC") as! AddGateWayVC
        self.navigationController?.pushViewController(Add, animated:true )
            }
        let action2 =  UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        action2.setValue(UIColor.red, forKey: "titleTextColor")
        
                alertaction.addAction(action1)
                alertaction.addAction(action2)
        self.present(alertaction, animated: true, completion: nil)
    }
         
    

    @IBAction func back(_ sender: UIBarButtonItem) {
     self.navigationController?.popViewController(animated: true)
    }
    
}
extension GatewayVC {

 func PostGateWayList() {
  
  let ClientId = "091f3164004d424abebb4f9c0c822f26"
  let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
  let pageNo = "1"
  let pageSize = "20"
  let date = Date().millisecondsSince1970
  
   let url = URL(string:self.url.AccountGatewayList)!
          var request = URLRequest(url: url)
          request.httpMethod = "POST"
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
          let parameters: [String : Any] = [
              "clientId":ClientId,
              "accessToken":AccessToken,
              "pageNo":pageNo,
              "pageSize":pageSize,
              "date":date,
          ]
  
  request.httpBody = parameters.percentEscaped().data(using: .utf8)
  
  URLSession.shared.dataTask(with: request) { (data, response, error) in
          guard let data = data else {
          if error == nil{
      print(error?.localizedDescription ?? "Unknown Error")
         }
          return
     }
      if let response = response as? HTTPURLResponse{
      guard (200 ... 299) ~= response.statusCode else {
          print("Status code :- \(response.statusCode)")
          print(response)
          return
       }}
do{
  if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
      if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
        
         if arrayJson.count == 0 {
            DispatchQueue.main.async {
        self.view.hideToastActivity()
              let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let startvc = storyboard.instantiateViewController(withIdentifier: "NoDataVC") as! NoDataVC
              self.addChild(startvc)
              self.ContainerV.addSubview(startvc.view)
              self.ContainerV.constrainToEdges(startvc.view)
              startvc.didMove(toParent: self)
            }} else {
         for json in arrayJson {
        self.GateWayList.append(json)
            DispatchQueue.main.async {
         self.view.hideToastActivity()
        self.GateWayTable.reloadData()
            }}}}
   }}catch{
    print("Error in get json data")
        }
    }.resume()
}}








