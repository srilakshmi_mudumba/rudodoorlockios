//
//  AboutVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 10/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class AboutVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var TableVC: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()

        TableVC.separatorStyle = .none
        TableVC.tableFooterView = UIView()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "Cell")
        cell?.textLabel?.text = "Our Story"
        cell?.textLabel?.textColor = UIColor.darkGray
        cell?.textLabel?.textAlignment = .center
        cell?.accessoryType = .disclosureIndicator
        
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let story = self.storyboard?.instantiateViewController(withIdentifier: "OurStoryVC") as! OurStoryVC
  self.navigationController?.pushViewController(story, animated: true)
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }

}
