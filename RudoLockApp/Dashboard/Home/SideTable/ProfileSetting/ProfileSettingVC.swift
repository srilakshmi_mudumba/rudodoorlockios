//
//  ProfileSettingVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 05/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class ProfileSettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var PSeetingTable: UITableView!
    
    @IBOutlet weak var LogBtnView: UIView!
    
    let url = Network()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        PSeetingTable.rowHeight = 50
        LogBtnView.layer.cornerRadius = 20
    }
    
    @IBAction func back(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    switch indexPath.row {
//        case 0:
//    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSCell") as! PSettingCell
//      cell.textLabel?.text = "Sound"
//      cell.accessoryView = UISwitch()
//       return cell
            
//        case 1:
//    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSCell") as! PSettingCell
//      cell.textLabel?.text = "Transfer Lock(s)"
//      cell.accessoryType = .disclosureIndicator
//          return cell
//        case 2:
//    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSCell") as! PSettingCell
//      cell.textLabel?.text = "Transfer Gateway"
//      cell.accessoryType = .disclosureIndicator
//         return cell
        case 0:
    let cell = tableView.dequeueReusableCell(withIdentifier: "ProfileSCell") as! PSettingCell
      cell.textLabel?.text = "About"
      cell.accessoryType = .disclosureIndicator
        return cell
        default:
            break;
        }
        return UITableViewCell()
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
//        case 0:
//      print("switch ON")
//        case 1:
//    let TL = self.storyboard?.instantiateViewController(withIdentifier: "TransferLockVC") as! TransferLockVC
//    self.navigationController?.pushViewController(TL, animated: true)
//            
//        case 2:
//    let TG = self.storyboard?.instantiateViewController(withIdentifier: "TransferGatewayVC") as! TransferGatewayVC
//    self.navigationController?.pushViewController(TG, animated: true)
            
        case 0:
    let About = self.storyboard?.instantiateViewController(withIdentifier: "AboutVC") as! AboutVC
    self.navigationController?.pushViewController(About, animated: true)

        default:
            break;
        }
    }
    
    @IBAction func LogOut(_ sender: UIButton) {
        
      let alert = UIAlertController(title: "", message: "Are you sure you want to logOut? ", preferredStyle:.alert)
        let action = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
    UserDefaults.standard.removeObject(forKey:"accessToken")
    UserDefaults.standard.removeObject(forKey: "CretedPass")
          DispatchQueue.main.async {
    let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "NavController") as! NavController
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate
        appDel.window?.rootViewController = loginVC
    }
            }
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
            alert.addAction(action)
            alert.addAction(cancel)
        self.present(alert,animated: true,completion: nil)
    }
    
    
    @IBAction func DeleteAccount(_ sender: UIButton) {
       let alert = UIAlertController(title: "", message: " Sure you want to Delete Your Account? ", preferredStyle:.alert)
             let action = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
                self.view.makeToastActivity(.center)
                self.DeleteUserAccount()
        UserDefaults.standard.removeObject(forKey:"accessToken")
        UserDefaults.standard.removeObject(forKey: "CretedPass")
                    DispatchQueue.main.async {
        let loginVC = self.storyboard?.instantiateViewController(withIdentifier: "NavController") as! NavController
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

        appDel.window?.rootViewController = loginVC
                }
            }
            let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
                 alert.addAction(action)
                 alert.addAction(cancel)
        self.present(alert,animated: true,completion: nil)
    }
    
    func DeleteUserAccount() {
        
            let clientId = "091f3164004d424abebb4f9c0c822f26"
            let clientSecret = "3c815de27069538163d6c776036a8b3c"
            let username = UserDefaults.standard.string(forKey: "Username")!
            let date = Date().millisecondsSince1970
            
        let url = URL(string:self.url.DeleteUserAccount)!
            var request = URLRequest(url: url)
               request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":clientId,
                "clientSecret":clientSecret,
                "username":"rudo_"+username,
                "date":date,
        ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
    URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error == nil{
                print(error?.localizedDescription ?? "Unknown Error")
                   }
                    return
               }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
        }}
    do{
        let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
            print(jsonData)
            DispatchQueue.main.async {
      self.navigationController?.popToRootViewController(animated: true)
                }} catch let error{
            print(error.localizedDescription)
                }
        }.resume()
 }}
