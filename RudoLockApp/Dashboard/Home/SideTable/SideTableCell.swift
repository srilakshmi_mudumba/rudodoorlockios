//
//  SideTableCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 20/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class SideTableCell: UITableViewCell {

    @IBOutlet weak var IconImg: UIImageView!
    @IBOutlet weak var Name: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
