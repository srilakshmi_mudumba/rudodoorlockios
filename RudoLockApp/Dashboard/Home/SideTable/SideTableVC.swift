//
//  SideTableVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 20/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class SideTableVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SendNickData {
    
    @IBOutlet weak var ImgView: UIView!
    @IBOutlet weak var ProfileImage: UIImageView!
    
    @IBOutlet weak var ProfileName: UILabel!
    
    @IBOutlet weak var SideTable: UITableView!
    
    let nameArry = ["Add Lock","Gateway","Support","Settings"]
    
    let imgArray = [ #imageLiteral(resourceName: "icons8-plus-math-100"),#imageLiteral(resourceName: "icons8-wi-fi-100"),#imageLiteral(resourceName: "icons8-headset-100"),#imageLiteral(resourceName: "icons8-settings-100")]
    
    var UserN = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topStatus()
        
        ImgView.layer.cornerRadius = ImgView.frame.height/2
        ProfileImage.layer.borderWidth = 1
        ProfileImage.layer.borderColor = UIColor.white.cgColor
        ProfileImage.layer.backgroundColor = UIColor.white.cgColor
        self.ProfileImage.layer.cornerRadius = ProfileImage.frame.size.width / 2
        
        view.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        SideTable.rowHeight = 65
        SideTable.tableFooterView = UIView()
        
        let tap2 = UITapGestureRecognizer(target: self, action: #selector(ProfileTapped))
        ImgView.addGestureRecognizer(tap2)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if UserN.isEmpty {
            
            let ProName = UserDefaults.standard.string(forKey: "Username")!
            ProfileName.text = ProName
            
        } else {
            
            ProfileName.text = UserN
        }
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.isHidden = true
    }
    
    @objc func ProfileTapped() {
        let Prof = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as! EditProfileVC
        self.navigationController?.pushViewController(Prof, animated: true)
        Prof.delNIC = self
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        nameArry.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SideTableCell") as! SideTableCell
        cell.IconImg.image = imgArray[indexPath.row]
        cell.Name.text = nameArry[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let add = self.storyboard?.instantiateViewController(withIdentifier: "AddLockVC") as! AddLockVC
            self.navigationController?.pushViewController(add, animated: true)
        case 1:
            let Getway = self.storyboard?.instantiateViewController(withIdentifier: "GatewayVC") as! GatewayVC
            self.navigationController?.pushViewController(Getway, animated: true)
        case 2:
            let supp = self.storyboard?.instantiateViewController(withIdentifier: "SupportVC") as! SupportVC
            self.navigationController?.pushViewController(supp, animated: true)
        case 3:
            let Pst = self.storyboard?.instantiateViewController(withIdentifier: "ProfileSettingVC") as! ProfileSettingVC
            self.navigationController?.pushViewController(Pst, animated: true)
        default:
            break;
        }
    }
    
    
    func userNick(NIC: String) {
        UserN = NIC
    }
    
    
}
