//
//  FAQlist.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 05/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

struct cellData {
    var opened = Bool()
    var title = String()
    var sectionData = [String]()
}

class FAQlist: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var FAQTable: UITableView!
    
    var tableViewData = [cellData]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()

        FAQTable.tableFooterView = UIView()
        FAQTable.rowHeight = 80
        
       tableViewData = [cellData(opened: false, title: "Mobile App", sectionData:["Is it possible to UNLOCK the Smart Lock using the Application when the Phone is not Connected to 3G/4G/Wi-Fi?","Why an I unable tobOperate the Smart Lock using the Application on my Phone?"]),
        cellData(opened: false, title: "Locks", sectionData:["Introduction","Hoe to ADD Smart Lock?","What are the Different Methods to Operate the Smart Lock?","How do I adjust the clock on my smart Lock?"]),
           
        cellData(opened: false, title: "Keypad", sectionData:["Why do the keypad lights go out immediately after 'activating' the smart lock?","Why is the keypad flashing when operating the Smart Lock?","Why cant the keypad be activated?"]),
           
        cellData(opened: false, title: "Passcode", sectionData:["How long can a passcode be valid for?","What happens if the User and Smart Lock are in different Time Zones?","Why is my Passcode not working?"]),
           
        cellData(opened: false, title: "Others", sectionData:["How can I Invalidate an eAccess,Fingerprint,Passcode or IC Card?","What is the GATEWAY used for?"]),
        ]
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.tableViewData.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if tableViewData[section].opened == true {
            return tableViewData[section].sectionData.count + 1
        } else {
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let dataIndex = indexPath.row - 1
        if indexPath.row == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell") else { return UITableViewCell()}
            cell.textLabel?.text = tableViewData[indexPath.section].title
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.font = UIFont.boldSystemFont(ofSize: 18)
            return cell
            
        } else {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "FaqCell2") else { return UITableViewCell()}
            cell.textLabel?.text = tableViewData[indexPath.section].sectionData[dataIndex]
            cell.textLabel?.numberOfLines = 3
            cell.textLabel?.textColor = #colorLiteral(red: 0.3333333433, green: 0.3333333433, blue: 0.3333333433, alpha: 1)
            return cell
        }}
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if tableViewData[indexPath.section].opened == true {
                tableViewData[indexPath.section].opened = false
                let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)
            } else {
        tableViewData[indexPath.section].opened = true
        let sections = IndexSet.init(integer: indexPath.section)
        tableView.reloadSections(sections, with: .none)

            }
        } else {
         //let d = IndexSet.init(integer: indexPath.row)
            switch (indexPath.section,indexPath.row) {
     case (0,1):
          let about = self.storyboard?.instantiateViewController(withIdentifier: "AboutFAQVC") as! AboutFAQVC
          self.navigationController?.pushViewController(about, animated: true)
     case (0,2):
          let about = self.storyboard?.instantiateViewController(withIdentifier: "MAVC2") as! MAVC2
          self.navigationController?.pushViewController(about, animated: true)
     case (1,1):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "LockVC1") as! LockVC1
            self.navigationController?.pushViewController(about, animated: true)
     case (1,2):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "LockVC2") as! LockVC2
            self.navigationController?.pushViewController(about, animated: true)
     case (1,3):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "LockVC3") as! LockVC3
            self.navigationController?.pushViewController(about, animated: true)
     case (1,4):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "LockVC4") as! LockVC4
            self.navigationController?.pushViewController(about, animated: true)
     case (2,1):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "KeypadVC1") as! KeypadVC1
            self.navigationController?.pushViewController(about, animated: true)
     case (2,2):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "KeypadVC2") as! KeypadVC2
            self.navigationController?.pushViewController(about, animated: true)
     case (2,3):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "KeypadVC3") as! KeypadVC3
            self.navigationController?.pushViewController(about, animated: true)
     case (3,1):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "PasscodeVC1") as! PasscodeVC1
            self.navigationController?.pushViewController(about, animated: true)
     case (3,2):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "PasscodeVC2") as! PasscodeVC2
            self.navigationController?.pushViewController(about, animated: true)
     case (3,3):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "PasscodeVC3") as! PasscodeVC3
            self.navigationController?.pushViewController(about, animated: true)
     case (4,1):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "OtherVC1") as! OtherVC1
            self.navigationController?.pushViewController(about, animated: true)
     case (4,2):
            let about = self.storyboard?.instantiateViewController(withIdentifier: "OtherVC2") as! OtherVC2
            self.navigationController?.pushViewController(about, animated: true)
         default:
            break;
         }
    }}
    
    @IBAction func Back(_ sender: UIBarButtonItem) {
  self.navigationController?.popViewController(animated: true)
    }
    
    
}
    
   
