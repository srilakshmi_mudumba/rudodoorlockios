//
//  FeedBackVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 08/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class FeedBackVC: UIViewController,UITextViewDelegate {

    @IBOutlet weak var TxtView: UITextView!
    @IBOutlet weak var LBLcount: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()

    let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
     view.addGestureRecognizer(tap)
            
      TxtView.delegate = self
        
   }
    
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {

        let currentT = TxtView.text ?? ""
        guard let stringR = Range(range,in: currentT) else {
            return false
        }
        
        let updateT = currentT.replacingCharacters(in: stringR, with: text)
        LBLcount.text = "\(0 + updateT.count)/255"
        
        return updateT.count < 255
    }
    
    
    @IBAction func SubmitBtn(_ sender: UIButton) {
    }
    
    
    @IBAction func BackBtn(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    
}
