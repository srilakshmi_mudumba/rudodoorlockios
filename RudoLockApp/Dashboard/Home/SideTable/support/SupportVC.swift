//
//  SupportVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 05/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class SupportVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func FAQButton(_ sender: UIButton) {
        
    let Faq = self.storyboard?.instantiateViewController(withIdentifier: "FAQlist") as! FAQlist
    self.navigationController?.pushViewController(Faq, animated: true)
    }
    
    
    @IBAction func FeedbackButton(_ sender: UIButton) {
        let FeedB = self.storyboard?.instantiateViewController(withIdentifier: "FeedBackVC") as! FeedBackVC
    self.navigationController?.pushViewController(FeedB, animated: true)
    }
    
    

    @IBAction func Back(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    

}
