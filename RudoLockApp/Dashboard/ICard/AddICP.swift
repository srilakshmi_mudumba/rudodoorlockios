//
//  AddICP.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock

class AddICP: UIViewController {

    @IBOutlet weak var EnterName: UITextField!
    @IBOutlet weak var OKView: UIView!
    
     let url = Network()
    
     var LockID = Int()
     var LockData = String()
     var CardID = Int()
     var CardNum = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
          topStatus()
        OKView.layer.cornerRadius = 10
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func OKButton(_ sender: UIButton) {
        
        if EnterName.text == "" {
    self.view.makeToast("Plz EnterName",duration:3.0,position:.center)
        } else {
    self.view.makeToastActivity(.center)
          ADD()
        }
    }
    
    
    func ADD() {
        
        let startDate = 0
        let endDate = 0
        
        TTLock.addICCardStartDate(Int64(startDate), endDate: Int64(endDate), lockData: LockData, progress: { state in
          }, success: { cardNumber in
            //add the card success,then upload card number
              self.CardNum = cardNumber!
             self.PostAddCard()
        }, failure: { errorCode, errorMsg in
            self.view.hideToastActivity()
        self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration:3.0,position:.center)
        })
    }
    
    
    func PostAddCard() {
        
        let clientId = "091f3164004d424abebb4f9c0c822f26"
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockID
        let cardNumber = CardNum
        let cardName = EnterName.text!
        let addType = "1"
        let startDate = "0"
        let endDate = "0"
        let date = Date().millisecondsSince1970
         
            
        let url = URL(string:self.url.AddICcard)!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                let parameters: [String : Any] = [
                "clientId":clientId,
                "accessToken":accessToken,
                "lockId":lockId,
                "cardNumber":cardNumber,
                "cardName":cardName,
                "addType":addType,
                "startDate":startDate,
                "endDate":endDate,
                "date":date,
            ]
            
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                    guard let data = data else {
                    if error == nil{
                print(error?.localizedDescription ?? "Unknown Error")
                   }
                    return
               }
                if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                 }}
          do{
              let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                    print(json)
        let ID =  json.object(forKey: "cardId") as! Int
               self.CardID = ID
            DispatchQueue.main.async {
            self.view.hideToastActivity()
        self.displayAlertMessage(userMessage: "IC Card Added Successfully")
            }} catch let error{
            print(error.localizedDescription)
                }
            }.resume()
}
    
    
    func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
        let myAlert = UIAlertController(title:"IC Card", message:userMessage, preferredStyle: UIAlertController.Style.alert);
            
        let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
         self.navigationController?.popViewController(animated: true)
       }
          myAlert.addAction(action);
     self.present(myAlert, animated:true, completion:nil);
    }}
}

