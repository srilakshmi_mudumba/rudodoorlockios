//
//  AddIcCardVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Parchment

class AddIcCardVC: UIViewController {

     @IBOutlet weak var ContainView: UIView!
        
        var LockID = Int()
        var LockData = String()
        var KeyVersion = Int()
        
        
        override func viewDidLoad() {
            super.viewDidLoad()
            topStatus()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let first = storyboard.instantiateViewController(withIdentifier: "AddICP") as! AddICP
            first.LockID = LockID
            first.LockData = LockData
            let second = storyboard.instantiateViewController(withIdentifier: "AddICT") as! AddICT
            second.LockID = LockID
            second.LockData = LockData

    let pagingViewController = PagingViewController(viewControllers: [
              first,second ])
            addChild(pagingViewController)
            ContainView.addSubview(pagingViewController.view)
            ContainView.constrainToEdges(pagingViewController.view)
            pagingViewController.didMove(toParent: self)
            
        }
        
        @IBAction func BackB(_ sender: UIBarButtonItem) {
            
        self.navigationController?.popViewController(animated: true)
        }
        

    }
