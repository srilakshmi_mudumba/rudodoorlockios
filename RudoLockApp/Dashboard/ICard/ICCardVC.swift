//
//  ICCardVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 23/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift

class ICCardVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var IcCardTable: UITableView!
    @IBOutlet weak var NoDataView: UIView!
    
    let url = Network()
    
    var refreshControl = UIRefreshControl()
    
    var LockId = Int()
    var LockData = String()
    var CardID = Int()
    var CardList = [[String:AnyObject]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()
        
        IcCardTable.register(UINib(nibName: "eAccessCell", bundle: nil), forCellReuseIdentifier: "eAccessCell")
        
        IcCardTable.rowHeight = 100
        IcCardTable.separatorStyle = .none
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        IcCardTable.addSubview(refreshControl)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.makeToastActivity(.center)
        PostCardList()
        self.CardList.removeAll()
        self.IcCardTable.reloadData()
    }
    
    
    @objc func refresh(_ sender: AnyObject) {
        self.view.makeToastActivity(.center)
        PostCardList()
        self.CardList.removeAll()
        self.IcCardTable.reloadData()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CardList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eAccessCell", for: indexPath as IndexPath) as! eAccessCell
        
        cell.NameLBL.text = CardList[indexPath.row]["cardName"] as? String
        cell.LBL.text = CardList[indexPath.row]["cardNumber"] as? String
        let endDate = CardList[indexPath.row]["endDate"] as? Int
        if endDate == 0 {
            cell.LBL2.text = "(Permanent)"
        } else {
            cell.LBL2.text = "(Timed)"
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == UITableViewCell.EditingStyle.delete {
            self.view.makeToastActivity(.center)
            CardID = CardList[indexPath.row]["cardId"] as! Int
            let Cardnum = CardList[indexPath.row]["cardNumber"] as! String
            TTLock.deleteICCardNumber(Cardnum, lockData:LockData, success: {
                self.DeleteCard()
                self.CardList.remove(at: indexPath.row)
                tableView.deleteRows(at: [indexPath], with: UITableView.RowAnimation.automatic)
            }, failure: { errorCode, errorMsg in
                self.view.hideToastActivity()
                self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",position: .center)
            })
        }}
    
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func DotTap(_ sender: UIBarButtonItem) {
        
        let alertaction  =  UIAlertController.init(title:"", message:"", preferredStyle: .actionSheet)
        let backView = alertaction.view.subviews.last?.subviews.last
        backView?.layer.cornerRadius = 10.0
        backView?.backgroundColor = UIColor.white
        backView?.tintColor = #colorLiteral(red: 0, green: 0.4784313725, blue: 1, alpha: 1)
        let action1 = UIAlertAction(title: "Add IC Card", style: .default) { (_) -> Void in
            let Add = self.storyboard?.instantiateViewController(withIdentifier: "AddIcCardVC") as! AddIcCardVC
            Add.LockID = self.LockId
            Add.LockData = self.LockData
            self.navigationController?.pushViewController(Add, animated:true )
        }
        
        let action2 = UIAlertAction(title: "Clear All", style: .default) { (_) -> Void in
            self.ClearAll()
        }
        let action4 =  UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil)
        action4.setValue(UIColor.red, forKey: "titleTextColor")
        
        alertaction.addAction(action1)
        alertaction.addAction(action2)
        alertaction.addAction(action4)
        self.present(alertaction, animated: true, completion: nil)
    }
    
    
    func ClearAll() {
        
        let alert = UIAlertController(title: "", message: "Sure you want to clear All", preferredStyle:.alert)
        let action = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            
            TTLock.clearAllICCards(withLockData: self.LockData, success: {
                self.view.makeToastActivity(.center)
                self.ClearCard()
            }, failure: { errorCode, errorMsg in
                self.view.hideToastActivity()
                self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",position: .center)
            })
        }
        let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
        alert.addAction(action)
        alert.addAction(cancel)
        self.present(alert,animated: true,completion: nil)
    }
    
}
extension ICCardVC {
    
    func PostCardList() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockId
        let pageNo = "1"
        let pageSize = "20"
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.IcCardList)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "lockId":lockId,
            "pageNo":pageNo,
            "pageSize":pageSize,
            "date":date,
        ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error == nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
                    if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
                        if arrayJson.count == 0 {
                            DispatchQueue.main.async {
                                self.view.hideToastActivity()
                                self.NoDataView.isHidden = false
                                self.refreshControl.endRefreshing()
                                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                                let startvc = storyboard.instantiateViewController(withIdentifier: "NoDataVC") as! NoDataVC
                                self.addChild(startvc)
                                self.NoDataView.addSubview(startvc.view)
                                self.NoDataView.constrainToEdges(startvc.view)
                                startvc.didMove(toParent: self)
                            }} else {
                                for json in arrayJson {
                                    self.CardList.append(json)
                                    DispatchQueue.main.async {
                                        self.NoDataView.isHidden = true
                                        self.IcCardTable.reloadData()
                                        self.refreshControl.endRefreshing()
                                        self.view.hideToastActivity()
                                    }}}
                    }}}catch{
                        print("Error in get json data")
                    }
        }.resume()
    }}

extension ICCardVC {
    
    func DeleteCard() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockId
        let cardId = CardID
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.DeleteCard)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "lockId":lockId,
            "cardId":cardId,
            "deleteType":1,
            "date":date,
        ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error != nil{
                    self.view.hideToastActivity()
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast("Delete successfully",position:.center)
                }
            } catch let error{
                self.view.hideToastActivity()
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func ClearCard() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockId
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.ClearICCard)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "lockId":lockId,
            "date":date,
        ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error == nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
                DispatchQueue.main.async {
                    self.view.hideToastActivity()
                    self.view.makeToast("Successfully clear all ICCards",position:.center)
                    self.PostCardList()
                    self.CardList.removeAll()
                    self.IcCardTable.reloadData()
                }
            } catch let error{
                self.view.hideToastActivity()
                print(error.localizedDescription)
            }
        }.resume()
    }
}













