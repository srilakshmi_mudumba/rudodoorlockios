//
//  DashBoard2VC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 23/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift
import TTLockGateway

class DashBoard2VC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate,CBPeripheralDelegate, CBPeripheralManagerDelegate{
    
    @IBOutlet weak var View1: UIView!
    
    @IBOutlet weak var CollectionView: UICollectionView!
    
    @IBOutlet weak var BatteryLabel: UILabel!
    @IBOutlet weak var BatteryPRogress: UIProgressView!
    
    @IBOutlet weak var LockIcon: UIView!
    @IBOutlet weak var LockImg: UIImageView!
    
    @IBOutlet weak var LockNameLBL: UILabel!
    
    var LockID = Int()
    var CULockData = String()
    var LockBattery = Int()
    var keyboardPwdVersion = Int()
    var Remark = String()
    var KeyID = Int()
    var LockN = String()
    
    let url = Network()
    
    var bluetoothPeripheralManager: CBPeripheralManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        topStatus()
        
        LockNameLBL.text = LockN
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.8
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.LockIcon.addGestureRecognizer(lpgr)
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x:305/1.5, y: 0), radius: View1.bounds.size.width, startAngle: 0, endAngle: .pi, clockwise: true)
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        View1.layer.mask = circleShape
        
        self.CollectionView.dataSource = self
        self.CollectionView.delegate = self
        self.CollectionView.register(UINib.init(nibName: "IconCollectionCell", bundle: nil), forCellWithReuseIdentifier: "IconCollectionCell")
        
        
        setBatteryProgress(value: LockBattery)
        BatteryLabel.text! = "\(LockBattery)%"
        
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            setupImageViewAnimation()
        }
        else {
            Unlock()
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let options = [CBCentralManagerOptionShowPowerAlertKey:0]
        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: options)
        
        LockIcon.layer.cornerRadius = LockIcon.frame.height/2
        LockImg.layer.borderWidth = 1
        LockImg.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        LockImg.layer.cornerRadius = LockImg.frame.height/2
        
        CollectionView.layer.cornerRadius = 10
        CollectionView.layer.borderWidth = 1
        CollectionView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    func gatwayinit() {
        
        TTGateway.initialize().self
    }
    
    
    
    func Unlock() {
        
        TTLock.controlLock(with: TTControlAction.actionUnlock, lockData:CULockData, success: { locktime, electricq, unique in
            self.LockImg.stopAnimating()
            DispatchQueue.main.async {
                let Show = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimePopUp") as! SelectTimePopUp
                Show.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                self.present(Show, animated: true)
            }
            self.UploadRecord()
            if self.Remark == "OneTime" {
                self.OneTimeKeyDelete()
            } else {
                print("NoRmark")
            }
        }, failure: { errorCode, errorMsg in
            self.LockImg.stopAnimating()
            self.view.makeToast("\(errorMsg ?? "Failed")",duration: 4.0,position: .center)
        })
    }
    
    
    func setupImageViewAnimation() {
        LockImg.animationImages = [#imageLiteral(resourceName: "icons8-unlock-96"),#imageLiteral(resourceName: "icons8-lock-96"),#imageLiteral(resourceName: "icons8-unlock-96")]
        LockImg.animationDuration = 1
        LockImg.startAnimating()
    }
    
    @IBAction func Back(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        if peripheral.state == .poweredOff {
            DispatchQueue.main.async {
                let myAlert1 = UIAlertController(title:"Bluetooth", message:"Turn On your Bluetooth to connect the App to Lock", preferredStyle: UIAlertController.Style.alert);
                
                let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                myAlert1.addAction(action);
                self.present(myAlert1, animated:true, completion:nil);
            }
        } else if peripheral.state == .unauthorized {
            DispatchQueue.main.async {
                let myAlert2 = UIAlertController(title:"Bluetooth", message:"Change the Settings to Allow Bluetooth to connect", preferredStyle: UIAlertController.Style.alert);
                
                let action = UIAlertAction(title: "Settings", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                }
                let action2 = UIAlertAction(title: "cancle", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                myAlert2.addAction(action)
                myAlert2.addAction(action2)
                self.present(myAlert2, animated:true, completion:nil);
            }}
    }
    
    
    func setBatteryProgress(value: Int) {
        BatteryPRogress.progress = Float(value) / 100
        if (BatteryPRogress.progress <= 0.3) {
            self.BatteryPRogress.progressTintColor = UIColor.red
        } else if (BatteryPRogress.progress <= 0.6) {
            self.BatteryPRogress.progressTintColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
            
        }  else {
            BatteryPRogress.progressTintColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
            
        }}
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 3-1")
            cell.IconLBL.text = "Records"
            return cell
        case 1:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 1-1")
            cell.IconLBL.text = "Setting"
            return cell
            
        default:
            break
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let Records = self.storyboard?.instantiateViewController(withIdentifier: "RecordsVC") as! RecordsVC
            self.navigationController?.pushViewController(Records, animated: true)
            Records.LockId = LockID
        case 1:
            let Setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            Setting.LockI = LockID
            Setting.CommonU = "CommonUser"
            Setting.LockData = CULockData
            self.navigationController?.pushViewController(Setting, animated: true)
        default:
            break;
        }
    }
    
    func OneTimeKeyDelete() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.DeleteEaccessKey)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "keyId":KeyID,
            "date":date,
        ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error == nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func UploadRecord() {
        
        TTLock.getOperationLog(with:TTOperateLogType.latest, lockData:CULockData, success: { operateRecord in
            
            let ClientId = "091f3164004d424abebb4f9c0c822f26"
            let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
            let date = Date().millisecondsSince1970
            
            let url = URL(string:self.url.RecordUplode)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "lockId":self.LockID,
                "records":operateRecord!,
                "date":date,
            ]
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error == nil{
                        print(error?.localizedDescription ?? "Unknown Error")
                    }
                    return
                }
                if let response = response as? HTTPURLResponse{
                    guard (200 ... 299) ~= response.statusCode else {
                        print("Status code :- \(response.statusCode)")
                        print(response)
                        return
                    }}
                do{
                    let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                    print(jsonData)
                } catch let error {
                    print(error.localizedDescription)
                }
            }.resume()
            
        }, failure: { errorCode, errorMsg in
            print("##############  Get log failed errorMsg: \(errorMsg ?? "")  ##############")
        })
    }
}
