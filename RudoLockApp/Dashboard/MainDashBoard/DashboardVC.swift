//
//  DashboardVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 13/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift
import TTLockDFU
import TTLockGateway
import CoreBluetooth


struct Dtails {
    let keyboardPwdVersion:Int
    let lockKey:String
    let passageMode:Int
    let aesKeyStr:String
    let lockName:String
    
    init(_ object:[String:AnyObject]){
        
        self.keyboardPwdVersion = object["keyboardPwdVersion"] as! Int
        self.lockKey = object["lockKey"] as! String
        self.passageMode = object["passageMode"] as! Int
        self.aesKeyStr = object["aesKeyStr"] as! String
        self.lockName = object["lockName"] as! String
        
    }}

class DashboardVC: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate,CBPeripheralDelegate, CBPeripheralManagerDelegate{
    
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var CollectionView: UICollectionView!
    @IBOutlet weak var BatteryLabel: UILabel!
    @IBOutlet weak var BatteryPRogress: UIProgressView!
    @IBOutlet weak var LockIcon: UIView!
    @IBOutlet weak var LockImg: UIImageView!
    @IBOutlet weak var LockNameLBL: UILabel!
    @IBOutlet weak var GatewayView: UIView!
    @IBOutlet weak var GatewayImg: UIImageView!
    
    var bluetoothPeripheralManager: CBPeripheralManager?
    
    
    var Lockkey = [[String:AnyObject]]()
    
    var LockData = String()
    
    let url = Network()
    
    var LockID = Int()
    var LockBattery = Int()
    var KeyID = Int()
    var keyboardPwdVersion = Int()
    var Remark = String()
    var LockN = String()
    var specialValue = Int()
    var wirelessKeypadFeatureValue = String()
    
    var KeyRight = Int()
    var hasGateway = Int()
    var LockDetails = [Dtails]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        topStatus()
        LockNameLBL.text = LockN
        
        PostLockList()
        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector (tap))
        self.LockIcon.addGestureRecognizer(tapGesture)
        
        let GtapGesture = UITapGestureRecognizer(target: self, action: #selector (tap2))
        self.GatewayView.addGestureRecognizer(GtapGesture)
        
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.8
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.LockIcon.addGestureRecognizer(lpgr)
        
        let GLong = UILongPressGestureRecognizer(target: self, action: #selector(handleGLongPress))
        GLong.minimumPressDuration = 0.8
        GLong.delaysTouchesBegan = true
        GLong.delegate = self
        self.GatewayView.addGestureRecognizer(GLong)
        
        
        let circlePath = UIBezierPath(arcCenter: CGPoint(x:305/1.5, y: 0), radius: View1.bounds.size.width, startAngle: 0, endAngle: .pi, clockwise: true)
        let circleShape = CAShapeLayer()
        circleShape.path = circlePath.cgPath
        View1.layer.mask = circleShape
        
        
        setBatteryProgress(value: LockBattery)
        BatteryLabel.text! = "\(LockBattery)%"
        
    }
    // Lock Method
    @objc func tap() {
        setupImageViewAnimation()
        TTLock.controlLock(with: TTControlAction.actionLock, lockData: LockData, success:{ lockTime, electricQuantity, uniqueId in
            self.LockImg.stopAnimating()
            DispatchQueue.main.async {
                self.displayMessage(userMessage: "Locked")
            }
        }, failure: { errorCode, errorMsg in
            self.LockImg.stopAnimating()
            self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration: 4.0,position: .center)
        })
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizer.State.ended {
            setupImageViewAnimation()
        }
        else {
            Unlock()
        }
    }
    
    @objc func tap2() {
        GatwayLock()
    }
    
    @objc func handleGLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state == UIGestureRecognizer.State.ended {
            GatwayUnlock()
        }
    }
    
    func ShowGatway() {
        
        let gatway =  Lockkey[0]["hasGateway"] as! Int
        if gatway == 1 {
            DispatchQueue.main.async {
                self.GatewayView.isHidden = false
            }
        } else {
            DispatchQueue.main.async {
                self.GatewayView.isHidden = true
            }
        }
    }
    
    //Unlock Method
    
    func Unlock() {
        
        TTLock.controlLock(with: TTControlAction.actionUnlock, lockData: LockData,
                           success:{ lockTime, electricQuantity, uniqueId in
                            
                            self.LockBattery = electricQuantity
                            self.LockImg.stopAnimating()
                            if self.Remark == "OneTime" {
                                self.OneTimeKeyDelete()
                            } else {
                                print("NoRmark")
                            }
                            DispatchQueue.main.async {
                                let Show = self.storyboard?.instantiateViewController(withIdentifier: "SelectTimePopUp") as! SelectTimePopUp
                                Show.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
                                self.present(Show, animated: true)
                            }
                            self.UploadRecord()
                           }, failure: { errorCode, errorMsg in
                            self.LockImg.stopAnimating()
                            self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration: 4.0,position: .center)
                           })
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.CollectionView.dataSource = self
        self.CollectionView.delegate = self
        self.CollectionView.register(UINib.init(nibName: "IconCollectionCell", bundle: nil), forCellWithReuseIdentifier: "IconCollectionCell")
        self.CollectionView.register(UINib.init(nibName: "Icon2CollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "Icon2CollectionViewCell")
        
        let options = [CBCentralManagerOptionShowPowerAlertKey:0]
        bluetoothPeripheralManager = CBPeripheralManager(delegate: self, queue: nil, options: options)
        
        LockIcon.layer.cornerRadius = LockIcon.frame.height/2
        LockImg.layer.borderWidth = 1
        LockImg.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        LockImg.layer.cornerRadius = LockImg.frame.height/2
        GatewayView.layer.cornerRadius = GatewayView.frame.height/2
        GatewayImg.layer.cornerRadius = GatewayImg.frame.height/2
        CollectionView.layer.cornerRadius = 10
        CollectionView.layer.borderWidth = 1
        CollectionView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        
    }
    
    func setupImageViewAnimation() {
        LockImg.animationImages = [#imageLiteral(resourceName: "icons8-unlock-96"),#imageLiteral(resourceName: "icons8-lock-96"),#imageLiteral(resourceName: "icons8-unlock-96")]
        LockImg.animationDuration = 1
        LockImg.startAnimating()
    }
    
    @IBAction func Back(_ sender: UIBarButtonItem) {
        
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func setBatteryProgress(value: Int) {
        BatteryPRogress.progress = Float(value) / 100
        if (BatteryPRogress.progress <= 0.3) {
            self.BatteryPRogress.progressTintColor = UIColor.red
            self.view.makeToast("Low Battery plz charge the lock")
        } else if (BatteryPRogress.progress <= 0.6) {
            self.BatteryPRogress.progressTintColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
            
        }  else {
            BatteryPRogress.progressTintColor = #colorLiteral(red: 0, green: 0.9768045545, blue: 0, alpha: 1)
            
        }}
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 8
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        switch indexPath.row {
        case 0:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 6-1")
            cell.IconLBL.text = "Share EAccess"
            return cell
        case 1:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.passcode)
            if !suportFunction {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Icon2CollectionViewCell", for: indexPath as IndexPath) as! Icon2CollectionViewCell
                cell.IconImage.image = #imageLiteral(resourceName: "ICONS 7-1")
                cell.IconLBL.text = "Generate Password"
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 7-1")
            cell.IconLBL.text = "Generate Password"
            return cell
        case 2:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 8-1")
            cell.IconLBL.text = "EAccess List"
            return cell
            
        case 3:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.passcode)
            if !suportFunction {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Icon2CollectionViewCell", for: indexPath as IndexPath) as! Icon2CollectionViewCell
                cell.IconImage.image = #imageLiteral(resourceName: "ICONS 14-1")
                cell.IconLBL.text = "Passcode List"
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 14-1")
            cell.IconLBL.text = "Passcode List"
            return cell
            
        case 4:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.icCard)
            if !suportFunction {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Icon2CollectionViewCell", for: indexPath as IndexPath) as! Icon2CollectionViewCell
                cell.IconImage.image = #imageLiteral(resourceName: "ICONS 10-1")
                cell.IconLBL.text = "IC Card"
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 10-1")
            cell.IconLBL.text = "IC Card"
            return cell
            
        case 5:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.fingerprint)
            if !suportFunction {
                let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Icon2CollectionViewCell", for: indexPath as IndexPath) as! Icon2CollectionViewCell
                cell.IconImage.image = #imageLiteral(resourceName: "ICONS 15-1")
                cell.IconLBL.text = "Finger Print"
                return cell
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 15-1")
            cell.IconLBL.text = "Finger Print"
            return cell
        case 6:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 3-1")
            cell.IconLBL.text = "Record List"
            return cell
        case 7:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "IconCollectionCell", for: indexPath as IndexPath) as! IconCollectionCell
            cell.IconImage.image = #imageLiteral(resourceName: "ICONS 1-1")
            cell.IconLBL.text = "Setting"
            return cell
        default:
            break
        }
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let GetEAccess = self.storyboard?.instantiateViewController(withIdentifier: "ShareEAccessVC") as! ShareEAccessVC
            self.navigationController?.pushViewController(GetEAccess, animated: true)
            GetEAccess.LockId = LockID
            GetEAccess.KeyId = KeyID
            GetEAccess.keyRight = KeyRight
            
        case 1:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.passcode)
            if !suportFunction {
                return
            }
            let GetPass = self.storyboard?.instantiateViewController(withIdentifier: "GeneratePasscodeVC") as! GeneratePasscodeVC
            self.navigationController?.pushViewController(GetPass, animated: true)
            GetPass.LockID = LockID
            GetPass.KeyVersion = keyboardPwdVersion
            GetPass.LockData = LockData
        case 2:
            let GetAccessList = self.storyboard?.instantiateViewController(withIdentifier: "EAccessListVC") as! EAccessListVC
            self.navigationController?.pushViewController(GetAccessList, animated: true)
            GetAccessList.LockId = LockID
        //GetAccessList.LockData = LockData
        case 3:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.passcode)
            if !suportFunction {
                return
            }
            let GetPassList = self.storyboard?.instantiateViewController(withIdentifier: "PasscodeListVC") as! PasscodeListVC
            self.navigationController?.pushViewController(GetPassList, animated: true)
            GetPassList.LockId = LockID
            GetPassList.LockData = LockData
            
        case 4:
            
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.icCard)
            if !suportFunction {
                return
            }
            let GetCardList = self.storyboard?.instantiateViewController(withIdentifier: "ICCardVC") as! ICCardVC
            self.navigationController?.pushViewController(GetCardList, animated: true)
            GetCardList.LockId = LockID
            GetCardList.LockData = LockData
            
        case 5:
            let suportFunction = TTUtil.lockSpecialValue(Int64(specialValue), suportFunction: TTLockSpecialFunction.fingerprint)
            if !suportFunction {
                return
            }
            let GetFingerList = self.storyboard?.instantiateViewController(withIdentifier: "FingerPrintVC") as! FingerPrintVC
            self.navigationController?.pushViewController(GetFingerList, animated: true)
            GetFingerList.LockId = LockID
            GetFingerList.LockData = LockData
            
        case 6:
            let RecordList = self.storyboard?.instantiateViewController(withIdentifier: "RecordsVC") as! RecordsVC
            self.navigationController?.pushViewController(RecordList, animated: true)
            RecordList.LockId = LockID
        case 7:
            
            let Setting = self.storyboard?.instantiateViewController(withIdentifier: "SettingVC") as! SettingVC
            Setting.LockI = LockID
            Setting.LockData = LockData
            Setting.KeyRight = KeyRight
            self.navigationController?.pushViewController(Setting, animated: true)
        default:
            break;
        }
    }
    
    //Mark: Check Bluetooth State And show Alert
    
    func peripheralManagerDidUpdateState(_ peripheral: CBPeripheralManager) {
        
        var statusMessage = ""
        
        switch peripheral.state {
        case .poweredOn:
            statusMessage = "Bluetooth Status: Turned On"
            
        case .poweredOff:
            statusMessage = "Bluetooth Status: Turned Off"
            
        case .resetting:
            statusMessage = "Bluetooth Status: Resetting"
            
        case .unauthorized:
            statusMessage = "Bluetooth Status: Not Authorized"
            
        case .unsupported:
            statusMessage = "Bluetooth Status: Not Supported"
            
        case .unknown:
            statusMessage = "Bluetooth Status: Unknown"
        @unknown default:
            break;
        }
        
        print(statusMessage)
        
        if peripheral.state == .poweredOff {
            DispatchQueue.main.async {
                let myAlert1 = UIAlertController(title:"Bluetooth", message:"Turn On your Bluetooth to connect the App to Lock", preferredStyle: UIAlertController.Style.alert);
                
                let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                myAlert1.addAction(action);
                self.present(myAlert1, animated:true, completion:nil);
            }
        } else if peripheral.state == .unauthorized {
            DispatchQueue.main.async {
                let myAlert2 = UIAlertController(title:"Bluetooth", message:"Change the Settings to Allow Bluetooth to connect", preferredStyle: UIAlertController.Style.alert);
                
                let action = UIAlertAction(title: "Settings", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                    UIApplication.shared.open(URL(string:UIApplication.openSettingsURLString)!)
                }
                let action2 = UIAlertAction(title: "cancle", style: .default) { (UIAlertAction) in
                    self.dismiss(animated: true, completion: nil)
                }
                myAlert2.addAction(action)
                myAlert2.addAction(action2)
                self.present(myAlert2, animated:true, completion:nil);
            }
        }
    }
    
    
    func currentDateTimeInMiliseconds() -> Int {
        let currentDate = Date()
        let since1970 = currentDate.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
    func displayMessage(userMessage:String) -> Void {
        DispatchQueue.main.async {
            let alertController = UIAlertController(title: "", message: userMessage, preferredStyle: .alert)
            
            let OKAction = UIAlertAction(title: "OK", style: .default) { (action:UIAlertAction!) in
                print("Ok button tapped")
                DispatchQueue.main.async
                {
                    self.dismiss(animated: true, completion: nil)
                }
            }
            alertController.addAction(OKAction)
            self.present(alertController, animated: true, completion:nil)
        }
    }}

extension DashboardVC {
    
    func OneTimeKeyDelete() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.DeleteEaccessKey)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "keyId":KeyID,
            "date":date,
        ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error == nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    
    func UploadRecord() {
        
        TTLock.getOperationLog(with:TTOperateLogType.latest, lockData:LockData, success: { operateRecord in
            
            let ClientId = "091f3164004d424abebb4f9c0c822f26"
            let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
            let date = Date().millisecondsSince1970
            
            let url = URL(string:self.url.RecordUplode)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "lockId":self.LockID,
                "records":operateRecord!,
                "date":date,
            ]
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error == nil{
                        print(error?.localizedDescription ?? "Unknown Error")
                    }
                    return
                }
                if let response = response as? HTTPURLResponse{
                    guard (200 ... 299) ~= response.statusCode else {
                        print("Status code :- \(response.statusCode)")
                        print(response)
                        return
                    }}
                do{
                    let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                    print(jsonData)
                } catch let error {
                    print(error.localizedDescription)
                }
            }.resume()
            
        }, failure: { errorCode, errorMsg in
            print("##############  Get log failed errorMsg: \(errorMsg ?? "")  ##############")
        })
    }
    
    func GatwayUnlock() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.UnlockGateway)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "lockId":self.LockID,
            "date":date,
        ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error != nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
                self.UploadRecord()
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    func GatwayLock() {
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let date = Date().millisecondsSince1970
        
        let url = URL(string:self.url.LockGateway)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "lockId":self.LockID,
            "date":date,
        ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error != nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                print(jsonData)
            } catch let error {
                print(error.localizedDescription)
            }
        }.resume()
    }
    
    
    func PostLockList() {
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let pageNo = "1"
        let pageSize = "20"
        let date = currentDateTimeInMiliseconds()
        
        let url = URL(string:self.url.GetLockList)!
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
            "clientId":ClientId,
            "accessToken":AccessToken,
            "pageNo":pageNo,
            "pageSize":pageSize,
            "date":date,
        ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error != nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                }
                return
            }
            if let response = response as? HTTPURLResponse{
                guard (200 ... 299) ~= response.statusCode else {
                    print("Status code :- \(response.statusCode)")
                    print(response)
                    return
                }}
            do{
                if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
                    if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
                        for json in arrayJson {
                            self.Lockkey.append(json)
                        }}
                    self.ShowGatway()
                }} catch let error{
                    print(error.localizedDescription)
                }
        }.resume()
    }
}
extension UIViewController {
    
    func topStatus() {
        if #available(iOS 13.0, *) {
            let app = UIApplication.shared
            let statusBarHeight: CGFloat = app.statusBarFrame.size.height
            
            let statusbarView = UIView()
            statusbarView.backgroundColor = #colorLiteral(red: 0.156640625, green: 0.148319485, blue: 0.391531808, alpha: 1)
            view.addSubview(statusbarView)
            statusbarView.translatesAutoresizingMaskIntoConstraints = false
            statusbarView.heightAnchor.constraint(equalToConstant: statusBarHeight).isActive = true
            statusbarView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 1.0).isActive = true
            statusbarView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
            statusbarView.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        } else {
            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
            statusBar?.backgroundColor = #colorLiteral(red: 0.156640625, green: 0.148319485, blue: 0.391531808, alpha: 1)
        }}
}












