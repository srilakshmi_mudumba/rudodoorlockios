//
//  Icon2CollectionViewCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/10/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class Icon2CollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var IconImage: UIImageView!
    @IBOutlet weak var IconLBL: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
