//
//  IconCollectionCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 23/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class IconCollectionCell: UICollectionViewCell {

    @IBOutlet weak var IconImage: UIImageView!
    @IBOutlet weak var IconLBL: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
