//
//  GatewayModel.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 11/11/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//
import Foundation

enum GatewayType : Int {
    case gatewayG1 = 1
    case gatewayG2
}

class GatewayModel: NSObject {
    var gatewayId: NSNumber?
    var gatewayMac: String?
    var gatewayVersion: GatewayType!
    var networkName: String?
    var lockNum: NSNumber?
    var isOnline = false
    //
    var isInited = false
    var gatewayName: String?
    var rssi = 0
    var searchTime: Date?
}
