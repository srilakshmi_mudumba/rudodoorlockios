//
//  LockModel.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import Foundation

struct LockModel {
    
 var lockId:Int
 var lockName:String
 var LockData:String
 var electricQuantity:Int
 var keyId:Int
 var keyboardPwdVersion: Int
 var userType:String
 var remarks:String
 var keyRight:Int
 var specialValue:Int


init(_ object:[String:AnyObject]){
    
    self.lockId = object["lockId"] as! Int
    self.lockName = object["lockAlias"] as! String
    self.LockData = object["lockData"] as! String
    self.electricQuantity = object["electricQuantity"] as! Int
    self.keyId = object["keyId"] as! Int
    self.keyboardPwdVersion = object["keyboardPwdVersion"] as! Int
    self.userType = object["userType"] as! String
    self.remarks = object["remarks"] as! String
    self.keyRight = object["keyRight"] as! Int
    self.specialValue = object["specialValue"] as! Int

   }
}
