//
//  PasscodeDetailsVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 22/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift
import TTLock

class PasscodeDetailsVC: UIViewController {

    @IBOutlet weak var Passcode: UILabel!
    @IBOutlet weak var Name: UILabel!
    @IBOutlet weak var Validity: UILabel!
    @IBOutlet weak var TimeIssued: UILabel!
    
    let url = Network()
    
    @IBOutlet weak var BtnView: UIView!
    @IBOutlet weak var ShareView: UIView!
    
    var passcode = String()
    var name = String()
    var valiDity = String()
    var timeIssue = Int()
    var LockID = Int()
    var KeyPassID = Int()
    var lockData = String()
    var endDate = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        BtnView.layer.cornerRadius = 10
        ShareView.layer.cornerRadius = 10

        Passcode.text = passcode
        Name.text = name
        
        let date2 = Date(timeIntervalSince1970: (Double(endDate) / 1000.0))
        let dateFormatter2 = DateFormatter()
         dateFormatter2.dateFormat = "YYYY-MM-dd HH:mm a"
        
        if valiDity == "Time" {
       Validity.text = dateFormatter2.string(from: date2)
            if date2 < Date() {
         Validity.textColor = #colorLiteral(red: 0.6666666865, green: 0.6666666865, blue: 0.6666666865, alpha: 1)
        } else if date2 > Date().addingTimeInterval(-3600) {
        Validity.textColor = #colorLiteral(red: 0.9529411793, green: 0.6862745285, blue: 0.1333333403, alpha: 1)
            }
        } else {
         Validity.text = valiDity
        }
    
        
       let date = Date(timeIntervalSince1970: (Double(timeIssue) / 1000.0))
        let dateFormatter = DateFormatter()
         dateFormatter.dateFormat = "YYYY-MM-dd HH:mm a"
            
        TimeIssued.text = dateFormatter.string(from: date)
        
    }
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
        
    self.navigationController?.popViewController(animated: true)
   }
    
    
    @IBAction func ShareBtn(_ sender: UIButton) {
        
        let txt = "Here is the Passcode for Lock \(passcode)"
    let VC = UIActivityViewController(activityItems: [txt], applicationActivities: [])
        if let popoverController = VC.popoverPresentationController {
        popoverController.sourceView = self.view
        popoverController.sourceRect = self.view.bounds
        }
        self.present(VC, animated: true, completion: nil)
    }
    
    @IBAction func DeletePassBtn(_ sender: UIButton) {
        TTLock.deletePasscode(passcode, lockData: lockData, success: {
          self.view.makeToastActivity(.center)
            self.PostDelete()
        }, failure: { errorCode, errorMsg in
      self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.",duration: 3.0, position: .center)
        })
    }
    
}

extension PasscodeDetailsVC {

  func PostDelete() {
    
    let ClientId = "091f3164004d424abebb4f9c0c822f26"
    let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockID
    let keyboardPwdId = KeyPassID
    let deleteType = "1"
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.DeletePasscode)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "lockId":lockId,
                "keyboardPwdId":keyboardPwdId,
                "deleteType":deleteType,
                "date":date,
            ]
    
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
         }}
  do{
       let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
            print(jsonData)
         DispatchQueue.main.async {
         self.view.hideToastActivity()
    self.navigationController?.popViewController(animated: true)
        }} catch let error{
    print(error.localizedDescription)
        }
    }.resume()
}}






