//
//  PasscodeLCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 16/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class PasscodeLCell: UITableViewCell {

    @IBOutlet weak var OuterView: UIView!
    @IBOutlet weak var ImgView: UIView!
    @IBOutlet weak var UserImg: UIImageView!
    
    @IBOutlet weak var UserName: UILabel!
    @IBOutlet weak var LBL2: UILabel!
    @IBOutlet weak var LBL3: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        OuterView.layer.cornerRadius = 10
        OuterView.layer.borderWidth = 1
        OuterView.layer.borderColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
