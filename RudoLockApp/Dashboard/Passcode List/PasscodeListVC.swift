//
//  PasscodeListVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class PasscodeListVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var PasscodeLTable: UITableView!
    @IBOutlet weak var NoDAta: UIView!
    
    var LockId = Int()
    var LockData = String()
    let url = Network()
    
    var PasscodeList = [[String:AnyObject]]()
    
    var refreshControl = UIRefreshControl()
    
    override func viewDidLoad() {
           super.viewDidLoad()
           
             topStatus()
           PasscodeLTable.rowHeight = 100
           PasscodeLTable.separatorStyle = .none
        
        PasscodeLTable.register(UINib(nibName: "PasscodeLCell", bundle: nil), forCellReuseIdentifier: "PasscodeLCell")
        
        refreshControl = UIRefreshControl()
             refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        PasscodeLTable.addSubview(refreshControl)
        
       }
    
    @objc func refresh(_ sender: AnyObject) {
            self.view.makeToastActivity(.center)
                   PostPasscodeList()
               self.PasscodeList.removeAll()
        self.PasscodeLTable.reloadData()

        }
    
    override func viewWillAppear(_ animated: Bool) {
        self.view.makeToastActivity(.center)
        PostPasscodeList()
      self.PasscodeList.removeAll()
        self.PasscodeLTable.reloadData()
    }
    
           
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
             PasscodeList.count
    }
              
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "PasscodeLCell", for: indexPath as IndexPath) as! PasscodeLCell
            
            cell.UserName.text = PasscodeList[indexPath.row]["keyboardPwdName"] as? String
            let d = PasscodeList[indexPath.row]["sendDate"] as! Int
            let date = Date(timeIntervalSince1970: (Double(d) / 1000.0))
            let dateFormatter = DateFormatter()
             dateFormatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
            cell.LBL2.text = dateFormatter.string(from: date)
        let type = PasscodeList[indexPath.row]["keyboardPwdType"] as? Int
            if type == 1 {
           cell.LBL3.text = "(One Time)"
            } else if type == 2 {
           cell.LBL3.text = "(Permanent)"
            } else if type == 3 {
        if PasscodeList[indexPath.row]["endDate"] as? Int == 0 {
              cell.LBL3.text = "(Permanent)"
        } else {
           cell.LBL3.text = "(Timed)"
        }}
           return cell
       }
         
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     let ListDetails = self.storyboard?.instantiateViewController(withIdentifier:"PasscodeDetailsVC") as! PasscodeDetailsVC
     self.navigationController?.pushViewController(ListDetails, animated:true)
        ListDetails.lockData = LockData
        ListDetails.LockID = LockId
        ListDetails.KeyPassID = PasscodeList[indexPath.row]["keyboardPwdId"] as! Int
        ListDetails.passcode = PasscodeList[indexPath.row]["keyboardPwd"] as! String
        ListDetails.name = PasscodeList[indexPath.row]["keyboardPwdName"] as! String
        ListDetails.timeIssue = PasscodeList[indexPath.row]["sendDate"] as! Int
        ListDetails.endDate = PasscodeList[indexPath.row]["endDate"] as! Int
    let type = PasscodeList[indexPath.row]["keyboardPwdType"] as? Int
         if type == 1 {
        ListDetails.valiDity = "OneTime"
         } else if type == 2 {
        ListDetails.valiDity = "Permanent"
         } else if type == 3 {
    if PasscodeList[indexPath.row]["endDate"] as? Int == 0 {
        ListDetails.valiDity = "Permanent"
    } else {
        ListDetails.valiDity = "Time"
    }}
}
    
    @IBAction func BackArrow(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension PasscodeListVC {

  func PostPasscodeList() {
    
    let ClientId = "091f3164004d424abebb4f9c0c822f26"
    let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockId
    let pageNo = "1"
    let pageSize = "100"
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.GetPasscodeList)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "lockId":lockId,
                "pageNo":pageNo,
                "pageSize":pageSize,
                "date":date,
            ]
    
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
         }}
  do{
      if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
     if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
          if arrayJson.count == 0 {
              DispatchQueue.main.async {
          self.view.hideToastActivity()
          self.refreshControl.endRefreshing()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
    let startvc = storyboard.instantiateViewController(withIdentifier: "NoDataVC") as! NoDataVC
                self.addChild(startvc)
                self.NoDAta.addSubview(startvc.view)
                self.NoDAta.constrainToEdges(startvc.view)
                startvc.didMove(toParent: self)
            }} else {
            
             for json in arrayJson {
            self.PasscodeList.append(json)
                DispatchQueue.main.async {
            self.view.hideToastActivity()
            self.refreshControl.endRefreshing()
            self.PasscodeLTable.reloadData()
            }}}
    }}}catch{
        print("Error in get json data")
            }
        }.resume()
    }}




