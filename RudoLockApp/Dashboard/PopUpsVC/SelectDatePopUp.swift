//
//  SelectDatePopUp.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 21/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift


protocol SendPicData {
    func userSelectData(TValue:String)
}

class SelectDatePopUp: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    var pickerData = ["5s","10s","15s","30s","60s","custom"]
    
    @IBOutlet weak var PickerView: UIPickerView!
    
         var userSelectData = String()
        var TValuedel : SendPicData?

       override func viewDidLoad() {
            super.viewDidLoad()
        view.backgroundColor = UIColor(red: 0.1, green: 0.1, blue: 0.1, alpha: 0.2)

         self.PickerView.delegate = self
         self.PickerView.dataSource = self
//        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
//        self.view.addGestureRecognizer(tap)
         }
        
         @objc func touchTapped(_ sender: UITapGestureRecognizer) {
            self.dismiss(animated: true, completion: nil)
          }
    
    @IBAction func cancelBtn(_ sender: UIBarButtonItem) {
      self.dismiss(animated: true, completion: nil)
    }
    
  @IBAction func DoneBtn(_ sender: UIBarButtonItem) {
       let row = PickerView.selectedRow(inComponent: 0)
            userSelectData = pickerData[row]
            print(userSelectData)
         TValuedel?.userSelectData(TValue:userSelectData)
            self.dismiss(animated: true, completion: nil)
    }
    
    //MARK:- PickerView Delegate & DataSource

        func numberOfComponents(in pickerView: UIPickerView) -> Int {
            return 1
        }
        func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
            return pickerData.count
        }
        func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
            return pickerData[row]
        }
        func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
            self.userSelectData = pickerData[row]
        }
}
