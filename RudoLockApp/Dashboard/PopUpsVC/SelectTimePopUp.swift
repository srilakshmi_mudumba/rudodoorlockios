//
//  SelectTimePopUp.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 21/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class SelectTimePopUp: UIViewController {
        
     @IBOutlet weak var BackView: UIView!
     @IBOutlet weak var Bview: UIView!
     @IBOutlet weak var successlbl: UILabel!
    @IBOutlet weak var IDLBl: UILabel!

    
        override func viewDidLoad() {
            super.viewDidLoad()
            
            BackView.layer.cornerRadius = 10
            Bview.layer.cornerRadius = 10
            
            let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
                 self.view.addGestureRecognizer(tap)
         }
        
         @objc func touchTapped(_ sender: UITapGestureRecognizer) {
            self.dismiss(animated: true, completion: nil)
          }
    
    @IBAction func ok(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)

    }
}
