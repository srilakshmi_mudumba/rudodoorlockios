//
//  ShowPasscodePopUp.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 21/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class ShowPasscodePopUp: UIViewController {

    @IBOutlet weak var Passcode: UILabel!
    
    @IBOutlet weak var BackView: UIView!
    
    var passcode = String()
    var PasscodeID = Int()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Passcode.text! = "\(passcode)"

        view.backgroundColor = UIColor(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        
        BackView.layer.cornerRadius = 10
               
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.touchTapped(_:)))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func touchTapped(_ sender: UITapGestureRecognizer) {
           self.dismiss(animated: true, completion: nil)
         }

    @IBAction func OkBtn(_ sender: UIButton) {
        
        self.dismiss(animated: true, completion: nil)

    }
    
    @IBAction func ShareBtn(_ sender: UIButton) {
        
      let txt = "Here is the Passcode for Lock \(passcode)"
        let VC = UIActivityViewController(activityItems: [txt], applicationActivities: [])
               
        if let popoverController = VC.popoverPresentationController {
         popoverController.sourceView = self.view
        popoverController.sourceRect = self.view.bounds
               }
    self.present(VC, animated: true, completion: nil)

    }}
