//
//  RecordsVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class RecordsVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var RecordTable: UITableView!
   @IBOutlet weak var NoDataView: UIView!
    
    let url = Network()
    
    
    var LockId = Int()
    var RecordList = [[String:AnyObject]]()
    
    var refreshControl = UIRefreshControl()
    
    @IBOutlet weak var SearchBar: UISearchBar!
    
    override func viewDidLoad() {
        super.viewDidLoad()
           topStatus()
        RecordTable.register(UINib(nibName: "eAccessCell", bundle: nil), forCellReuseIdentifier: "eAccessCell")
              
        RecordTable.rowHeight = 100
        RecordTable.separatorStyle = .none
        
        SearchBar.isHidden = true
        
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        RecordTable.addSubview(refreshControl)
    }
    
    
    override func viewDidAppear(_ animated: Bool) {
          super.viewDidAppear(animated)
       self.view.makeToastActivity(.center)
            PostRecordList()
        self.RecordList.removeAll()
        self.RecordTable.reloadData()
       }
    
     @objc func refresh(_ sender: AnyObject) {
         self.view.makeToastActivity(.center)
             PostRecordList()
         self.RecordList.removeAll()
         self.RecordTable.reloadData()
    }
    
    @IBAction func search(_ sender: UIBarButtonItem) {
        
        if sender.isEnabled {
            SearchBar.isHidden = false
        } else {
        SearchBar.isHidden = true
            }
    }


     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return RecordList.count
    }
                 
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "eAccessCell", for: indexPath as IndexPath) as! eAccessCell
        
      let NLbl = RecordList[indexPath.row]["username"] as? String
//        //cell.NameLBL.text = String(NLbl?.dropFirst(5) ?? "")
//        if let index = (NLbl?.range(of: "_")?.upperBound) {
//          //prints "value"
        cell.NameLBL.text = NLbl?.replacingOccurrences(of: "rudo_", with: "")
            
//            String(NLbl?.suffix(from: index) ?? "Name")
      let d = RecordList[indexPath.row]["lockDate"] as! Int
     let date2 = Date(timeIntervalSince1970: (Double(d) / 1000.0))
     let dateFormatter2 = DateFormatter()
      dateFormatter2.dateFormat = "YYYY-MM-dd HH:mm a"
        
     cell.LBL.text = dateFormatter2.string(from: date2)

        return cell
    }
    
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
    self.navigationController?.popViewController(animated: true)
    }
    
    
    
}
extension RecordsVC {

  func PostRecordList() {
    
    let ClientId = "091f3164004d424abebb4f9c0c822f26"
    let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockId
    let startDate = "0"
    let endDate = "0"
    let pageNo = "1"
    let pageSize = "100"
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.RecordList)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "clientId":ClientId,
                "accessToken":AccessToken,
                "lockId":lockId,
                "startDate":startDate,
                "endDate":endDate,
                "pageNo":pageNo,
                "pageSize":pageSize,
                "date":date,
            ]
    
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error != nil {
                self.view.hideToastActivity()
                self.refreshControl.endRefreshing()
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
        if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
         }}
  do{
      if let jsonData = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String:Any] {
        if let arrayJson = jsonData["list"] as? [[String:AnyObject]] {
            if arrayJson.count == 0 {
                   DispatchQueue.main.async {
               self.view.hideToastActivity()
                    self.refreshControl.endRefreshing()
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                 let startvc = storyboard.instantiateViewController(withIdentifier: "NoDataVC") as! NoDataVC
                     self.addChild(startvc)
                     self.NoDataView.addSubview(startvc.view)
                     self.NoDataView.constrainToEdges(startvc.view)
                     startvc.didMove(toParent: self)
            }} else {
                  for json in arrayJson {
                 self.RecordList.append(json)
                     DispatchQueue.main.async {
                 self.view.hideToastActivity()
            self.refreshControl.endRefreshing()
                 self.RecordTable.reloadData()
                 }}}
        }}}catch{
             print("Error in get json data")
                 }
             }.resume()
         }}






