//
//  AutoLockVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift
import TTLock

class AutoLockVC: UIViewController,UITableViewDelegate,UITableViewDataSource,SendPicData{
   

    @IBOutlet weak var AutoLockTable: UITableView!
    @IBOutlet weak var SaveView: UIView!
    
    @IBOutlet weak var TimeSecText: UITextField!
    @IBOutlet weak var TimeView: UIView!
    
    let url = Network()
    var LockId = Int()
    var LockData = String()
    var Time = Int()
    
      var selectSetting = String()

    override func viewDidLoad() {
        super.viewDidLoad()
              topStatus()
        AutoLockTable.register(UINib(nibName: "AutoLockCell", bundle: nil), forCellReuseIdentifier: "AutoLockCell")
        
        self.AutoLockTable.delegate = self
        self.AutoLockTable.dataSource = self
        
        self.TimeView.isHidden = false
        
        AutoLockTable.rowHeight = 50
         self.TimeView.isHidden = true
    }
    
//        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
//            self.SaveView.endEditing(true)
//        }
//    
    //Mark: table Method
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "AutoLockCell") as! AutoLockCell
     cell.DetailTxt.text = selectSetting
        if selectSetting.isEmpty {
     cell.DetailTxt.text = UserDefaults.standard.string(forKey: "AutoTime")
        }
          return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        DispatchQueue.main.async(execute: {
            let Show = self.storyboard?.instantiateViewController(withIdentifier: "SelectDatePopUp") as! SelectDatePopUp
                          Show.TValuedel = self
            Show.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        self.present(Show, animated: true, completion: nil)
        })
    }
        
    func userSelectData(TValue: String) {
          selectSetting = TValue
         if TValue == "custom" {
       self.TimeView.isHidden = false
     } else {
        self.TimeView.isHidden = true
       }
      AutoLockTable.reloadData()
 }
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
     self.navigationController?.popViewController(animated: true)
       }

    @IBAction func SaveBtn(_ sender: UIButton) {
        
        if selectSetting == "custom" {
          if TimeSecText.text == "" {
      self.view.makeToast("Enter Your Time in Sec")
         } else if LockData.isEmpty{
       self.view.makeToast("Only Admin Can set LockTime")
            } else {
             Time = Int(TimeSecText.text!)!
              let Tm2 = 1...4
            if Tm2.contains(Time) {
      self.view.makeToast("Minimum Time should be 5s",position:.center)
            } else {
            self.view.makeToastActivity(.center)
        TTLock.setAutomaticLockingPeriodicTime(Int32(Time), lockData: LockData, success: {
                self.PostAutoLockTime()
        }, failure: { errorCode, errorMsg in
            self.view.hideToastActivity()
    self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")
        })
            }
   }}
        else {
               if LockData.isEmpty{
            self.view.makeToast("Only Admin Can set LockTime")
             } else if selectSetting.isEmpty {
            self.view.makeToast("set LockTime")
                 }
               else {
            self.view.makeToastActivity(.center)
                let St = selectSetting.dropLast()
                   Time = Int(St)!
             TTLock.setAutomaticLockingPeriodicTime(Int32(Time), lockData: LockData, success: {
                     self.PostAutoLockTime2()
                }, failure: { errorCode, errorMsg in
                  self.view.hideToastActivity()
            self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")
              })
        }
 }}

    
    func PostAutoLockTime() {
            
        let clientId = "091f3164004d424abebb4f9c0c822f26"
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockId
        let seconds = Int(TimeSecText.text!)!
        let type = "1"
        let date = Date().millisecondsSince1970
             
        let url = URL(string:self.url.AutoLock)!
        var request = URLRequest(url: url)
            request.httpMethod = "POST"
          request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
                    "clientId":clientId,
                    "accessToken":accessToken,
                    "lockId":lockId,
                    "seconds":seconds,
                    "type":type,
                    "date":date,
                ]
       request.httpBody = parameters.percentEscaped().data(using: .utf8)
                
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                        if error == nil{
                    print(error?.localizedDescription ?? "Unknown Error")
                       }
                        return
                   }
            if let response = response as? HTTPURLResponse{
            guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
                    print(response)
                        return
                     }}
            do{
        let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
        UserDefaults.standard.set(seconds, forKey: "AutoTime")
        DispatchQueue.main.async {
            self.view.hideToastActivity()
        self.displayAlertMessage(userMessage: "Set AutoLock success")
                  }
                
            } catch let error{
                self.view.hideToastActivity()
                print(error.localizedDescription)
                    }
          }.resume()
 }
    
    func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
        let myAlert = UIAlertController(title:"Auto Lock", message:userMessage, preferredStyle: UIAlertController.Style.alert);
            
        let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
         self.navigationController?.popViewController(animated: true)
       }
          myAlert.addAction(action);
     self.present(myAlert, animated:true, completion:nil);
    }}
    
    
    func PostAutoLockTime2() {
               
           let clientId = "091f3164004d424abebb4f9c0c822f26"
           let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
           let lockId = LockId
           let seconds = Time
           let type = "1"
           let date = Date().millisecondsSince1970
                
           let url = URL(string:self.url.AutoLock)!
           var request = URLRequest(url: url)
               request.httpMethod = "POST"
             request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
           let parameters: [String : Any] = [
                       "clientId":clientId,
                       "accessToken":accessToken,
                       "lockId":lockId,
                       "seconds":seconds,
                       "type":type,
                       "date":date,
                   ]
          request.httpBody = parameters.percentEscaped().data(using: .utf8)
                   
           URLSession.shared.dataTask(with: request) { (data, response, error) in
               guard let data = data else {
                           if error == nil{
                       print(error?.localizedDescription ?? "Unknown Error")
                          }
                           return
                      }
               if let response = response as? HTTPURLResponse{
               guard (200 ... 299) ~= response.statusCode else {
               print("Status code :- \(response.statusCode)")
                       print(response)
                           return
                        }}
               do{
           let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                   print(json)
         UserDefaults.standard.set(self.selectSetting, forKey: "AutoTime")
           DispatchQueue.main.async {
               self.view.hideToastActivity()
           self.displayAlertMessage(userMessage: "Set AutoLock success")
                     }
                   
               } catch let error{
                self.view.hideToastActivity()
                   print(error.localizedDescription)
                       }
             }.resume()
    }
}
