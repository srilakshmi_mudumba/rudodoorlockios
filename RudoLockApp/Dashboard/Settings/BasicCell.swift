//
//  BasicCell.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class BasicCell: UITableViewCell {

    
    @IBOutlet weak var TitleLbl: UILabel!
    @IBOutlet weak var TxtLbl: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
}
