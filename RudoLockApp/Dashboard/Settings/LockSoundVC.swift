//
//  LockSoundVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift

class LockSoundVC: UIViewController {
    
    @IBOutlet weak var CurrentMode: UILabel!
    @IBOutlet weak var BtnView: UIView!
    @IBOutlet weak var turnBtn: UIButton!
    
      var lockData = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        BtnView.layer.cornerRadius = 10
        
        getCurrentMode()
        self.view.makeToastActivity(.center)
    }
    
    
    @IBAction func TurnBtn(_ sender: UIButton) {
        
          if CurrentMode.text == "Off" {
            TTLock.setLockConfigWith(TTLockConfigType.lockSound, on:true, lockData: lockData, success: {
          self.view.makeToast("Success: LockSound is Turn On",position: .center)
        self.turnBtn.setTitle("Turn Off", for: .normal)
               self.CurrentMode.text = "On"
            }, failure: { errorCode, errorMsg in
        self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")})
        } else {
        TTLock.setLockConfigWith(TTLockConfigType.lockSound, on:false, lockData: lockData, success: {
          self.view.makeToast("Success: LockSound is Turn Off",position: .center)
            self.turnBtn.setTitle("Turn ON", for: .normal)
             self.CurrentMode.text = "Off"
            }, failure: { errorCode, errorMsg in
        self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")})
       }}
    
    
     func getCurrentMode() {
        TTLock.getConfigWith(TTLockConfigType.lockSound, lockData: lockData,success: { type, isOn in
            self.view.hideToastActivity()
            if isOn == true {
        self.CurrentMode.text! = "On"
            self.turnBtn.setTitle("Turn Off", for: .normal)
            } else {
         self.CurrentMode.text! = "Off"
            self.turnBtn.setTitle("Turn ON", for: .normal)
            }
        } ,failure: { errorCode, errorMsg in
            self.view.hideToastActivity()
         self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")})
        }

    @IBAction func Back(_ sender: UIBarButtonItem) {
     self.navigationController?.popViewController(animated: true)
       }
    
    
    
}
