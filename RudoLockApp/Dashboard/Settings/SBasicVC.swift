//
//  SBasicVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class SBasicVC: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    var LockId = Int()
    var LockNumber = String()
    var MacID = String()
    var Battery = Int()
    var ValidityP = Int()
    var LockName = String()
    var AdminPass = String()
    
    @IBOutlet weak var BasicTable: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
             topStatus()
        self.BasicTable.delegate = self
        self.BasicTable.dataSource = self
        
       BasicTable.register(UINib(nibName: "BasicCell", bundle: nil), forCellReuseIdentifier: "BasicCell")
        
        BasicTable.rowHeight = 55
        BasicTable.tableFooterView = UIView()
        
    }
    
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch indexPath.row {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as! BasicCell
          cell.TitleLbl.text = "Lock Number"
          cell.TxtLbl.text = LockNumber
            
            return cell
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as! BasicCell
          cell.TitleLbl.text = "MAC/ID"
          cell.TxtLbl.text = MacID
            
            return cell
        case 2:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as! BasicCell
            cell.TitleLbl.text = "Battery"
            cell.TxtLbl.text  = "\(Battery)"
            return cell
        case 3:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as! BasicCell
            cell.TitleLbl.text = "Validity Period"
            let END = ValidityP
            if END == 0 {
              cell.TxtLbl.text = "Permanent"
            } else {
        let Valid = Date(timeIntervalSince1970: (Double(END) / 1000.0))
            cell.TxtLbl.text = "\(Valid))"
         }
            return cell
        case 4:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as! BasicCell
            cell.TitleLbl.text = "Lock Name"
            cell.TxtLbl.text = LockName
            
            return cell
         case 5:
            let cell = tableView.dequeueReusableCell(withIdentifier: "BasicCell") as! BasicCell
            cell.TitleLbl.text = "Admin Passcode"
            cell.TxtLbl.text = AdminPass
                
            return cell
        default:
            break;
        }
        return UITableViewCell()
    }
    
    
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
