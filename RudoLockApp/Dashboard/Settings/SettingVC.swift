//
//  SettingVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 15/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import Toast_Swift

struct LockDtails {
 let lockName:String
 let lockMac:String
 let electricQuantity:Int
 let date:Int
 let lockAlias:String
 let noKeyPwd:String
 let specialValue:Int
    
init(_ object:[String:Any]){
    
    self.lockName = object["lockName"] as! String
    self.lockMac = object["lockMac"] as! String
    self.electricQuantity = object["electricQuantity"] as! Int
    self.date = object["date"] as! Int
    self.lockAlias = object["lockAlias"] as! String
    self.noKeyPwd = object["noKeyPwd"] as! String
    self.specialValue = object["specialValue"] as! Int


 }}


class SettingVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    
    @IBOutlet weak var SettingTable: UITableView!
    @IBOutlet weak var DelView: UIView!
    
    var LockI = Int()
    let url = Network()
    let LabelArray = ["Remote Unlock","Auto Lock","Lock Sound"]
    
    var ListKey = [LockDtails]()
    var CommonU = String()
    var LockData = String()
    
    var KeyRight = Int()
    var Password = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
           topStatus()
        
        if CommonU.isEmpty {
        DelView.isHidden = false
        } else {
         DelView.isHidden = true
        }
        
        if KeyRight == Int(1) {
          DelView.isHidden = true
        }else {
        DelView.isHidden = false
        }
        
        DelView.layer.cornerRadius = 20
        PostLockDetails()
    }
    
    @IBAction func BackB(_ sender: UIBarButtonItem) {
        
    self.navigationController?.popViewController(animated: true)
        
    }
    
    @IBAction func DeleteLock(_ sender: UIButton) {
        
    let alert = UIAlertController(title: "", message: "Sure for Delete Lock ", preferredStyle:.alert)
        let action = UIAlertAction(title: "Yes", style: .default) { (UIAlertAction) in
            self.TextPopAlert()
            }
    let cancel = UIAlertAction(title: "cancel", style: .cancel, handler: nil)
         alert.addAction(action)
         alert.addAction(cancel)
    self.present(alert,animated: true,completion: nil)
     }
    
    
    func TextPopAlert() {
        let alert = UIAlertController(title: "Enter your password", message: "enter password to delete lock", preferredStyle: .alert)
        alert.addTextField { (textField) in
            textField.placeholder = "Enter Password"
        }
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
        self.view.makeToastActivity(.center)
            let textField = alert?.textFields![0]
            if textField != nil {
        self.Password = "\(textField?.text! ?? "")"
            } else {
        self.view.makeToast("Unknow Password",position:.center)
            }
         self.PostLogIn()
        }))
        alert.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
        
         self.present(alert, animated: true, completion: nil)
    }
    
    func TTmethod() {
        
      TTLock.resetLock(withLockData: LockData,success: {
                print("reset lock success")
             self.DeleteLock()
        },failure: { errorCode, errorMsg in
            DispatchQueue.main.async {
        self.view.hideToastActivity()
    self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.", duration: 0.3,position: .center)
            }
        }) }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
         if section == 1 {
            return 3
         }
        else {
           return 1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch indexPath.section {
        case 0:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            cell.textLabel?.text = "Basic"
            cell.accessoryType = .disclosureIndicator
            
            return cell
            
        case 1:
            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
            cell.textLabel?.text = LabelArray[indexPath.row]
            cell.accessoryType = .disclosureIndicator
            
            return cell
            
//        case 2:
//            let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell") as! SettingCell
//            cell.textLabel?.text = "Unlock Notification"
//            cell.accessoryView = UISwitch()
            
           // return cell
            
        default:
            break;
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        if CommonU == "CommonUser" {
     switch (indexPath.section,indexPath.row) {
       case(0,0):
          return 0
       case(1,0):
          return 0
        default:
           break;
      }
        } else if KeyRight == Int(1) {
    switch (indexPath.section,indexPath.row) {
            case(0,0):
            return 0
         case(1,0):
            return 0
            default:
              break;
          }
        }
        return 50
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return " "
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        switch (indexPath.section,indexPath.row) {
        case(0,0):
    let Basic = self.storyboard?.instantiateViewController(withIdentifier: "SBasicVC") as! SBasicVC
        Basic.LockId = LockI
      Basic.LockNumber = ListKey[indexPath.row].lockName
      Basic.MacID = ListKey[indexPath.row].lockMac
      Basic.Battery = ListKey[indexPath.row].electricQuantity
      Basic.ValidityP = ListKey[indexPath.row].date
      Basic.AdminPass = ListKey[indexPath.row].noKeyPwd
      Basic.LockName = ListKey[indexPath.row].lockAlias
    self.navigationController?.pushViewController(Basic, animated: true)
        case(1,0):
    let Basic = self.storyboard?.instantiateViewController(withIdentifier: "UnlockRmotlyVC") as! UnlockRmotlyVC
        Basic.LockID = LockI
        Basic.LockData = LockData
     self.navigationController?.pushViewController(Basic, animated: true)
            
        case(1,1):
    let Basic = self.storyboard?.instantiateViewController(withIdentifier: "AutoLockVC") as! AutoLockVC
       Basic.LockId = LockI
       Basic.LockData = LockData
    self.navigationController?.pushViewController(Basic, animated: true)
            
     case(1,2):
//         let specialV = ListKey[indexPath.row].specialValue
//     let suportFunction = TTUtil.lockSpecialValue(Int64(specialV), suportFunction: TTLockSpecialFunction.audioSwitch)
//        if !suportFunction {
//        self.view.makeToast("This function is not supported in lock")
//             return
//         }
    let Basic = self.storyboard?.instantiateViewController(withIdentifier: "LockSoundVC") as! LockSoundVC
         Basic.lockData = LockData
        self.navigationController?.pushViewController(Basic, animated: true)
    
        default:
            break;
    }}

}

extension SettingVC {
    
  func PostLockDetails() {
       
 let ClientId = "091f3164004d424abebb4f9c0c822f26"
 let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
 let lockId = LockI
 let date = Date().millisecondsSince1970
       
    let url = URL(string:self.url.GetLockDetails)!
        var request = URLRequest(url: url)
   request.httpMethod = "POST"
     request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
        "clientId":ClientId,
        "accessToken":AccessToken,
        "lockId":lockId,
        "date":date,
            ]
   request.httpBody = parameters.percentEscaped().data(using: .utf8)
       
URLSession.shared.dataTask(with: request) { (data, response, error) in
        guard let data = data else {
        if error != nil{
            self.view.hideToastActivity()
    print(error?.localizedDescription ?? "Unknown Error")
        }
           return
        }
    if let response = response as? HTTPURLResponse{
    guard (200 ... 299) ~= response.statusCode else {
        print("Status code :- \(response.statusCode)")
        print(response)
        return
    }}
     do{
        let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String:Any]
        if  json["errmsg"] as? String == "not lock admin" {
            DispatchQueue.main.async {
                self.view.hideToastActivity()
       // self.view.makeToast("You are not Lock Admin", duration: 3.0 ,position: .center)
            }
        } else {
        self.ListKey.append(LockDtails(json))
        }} catch let error{
       print(error.localizedDescription)
           }
       }.resume()
}
    
    func DeleteLock() {
            
        let clientId = "091f3164004d424abebb4f9c0c822f26"
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockI
        let date = Date().millisecondsSince1970
             
        let url = URL(string:self.url.DeleteLock)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
    request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
           let parameters: [String : Any] = [
                "clientId":clientId,
                "accessToken":accessToken,
                "lockId":lockId,
                "date":date,
        ]
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
                
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
                if error != nil{
             self.view.hideToastActivity()
        print(error?.localizedDescription ?? "Unknown Error")
            }
                return
      }
    if let response = response as? HTTPURLResponse{
    guard (200 ... 299) ~= response.statusCode else {
         print("Status code :- \(response.statusCode)")
                print(response)
                return
        }}
    do{
        let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
        DispatchQueue.main.async {
        self.view.hideToastActivity()
    if let viewController = self.navigationController?.viewControllers.first(where: {$0 is HomeVC }) {
    self.navigationController?.popToViewController(viewController, animated: false)
            }
          }
        } catch let error{
      print(error.localizedDescription)
            }
        }.resume()
    }
    
    
    func PostLogIn() {
            
        let Username = UserDefaults.standard.string(forKey: "Username")!
        let password = Password.md5
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let ClientSecret = "3c815de27069538163d6c776036a8b3c"
        let GrantTy = "password"
        let Url = "yutuelectronics.com"
        
        let url = URL(string:self.url.LogIn)!
          var request = URLRequest(url: url)
            request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "client_id":ClientId,
                "client_secret":ClientSecret,
                "grant_type":GrantTy,
                "username": "rudo_"+Username,
                "password":password,
                "redirect_uri":Url,
        ]
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
        URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error != nil{
                self.view.hideToastActivity()
        print(error?.localizedDescription ?? "Unknown Error")
                }
            return
        }
         if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
            print("Status code :- \(response.statusCode)")
            print(response)
            return
      }}
        do{
         let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
        if json.object(forKey: "errmsg") as? String == "invalid account or invalid password" {
                DispatchQueue.main.async {
                 self.view.hideToastActivity()
        self.view.makeToast("Wrong Password Check Again", duration: 3.0, position: .bottom)
               }
        } else {
            self.TTmethod()
         }
            } catch let error{
        print(error.localizedDescription)
            }
        }.resume()
}}



