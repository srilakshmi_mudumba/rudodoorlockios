//
//  UnlockRmotlyVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 04/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import TTLockGateway
import Toast_Swift

class UnlockRmotlyVC: UIViewController {

    var LockID = Int()
    var LockData = String()
    
    let url = Network()
    
    @IBOutlet weak var CurrentMode: UILabel!
    @IBOutlet weak var btnName: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        getCurrentMode()
        self.view.makeToastActivity(.center)
    }
    
    
    
    @IBAction func ButtonUnlock(_ sender: UIButton) {
        
        if CurrentMode.text == "Off" {
    TTLock.setRemoteUnlockSwitchOn(true, lockData: LockData,
                success: { LockDataOn in
        UserDefaults.standard.set(LockDataOn, forKey: "RemoteLockD")
        self.btnName.setTitle("Turn Off", for: .normal)
                    self.UpdateLockData()
                 self.CurrentMode.text = "On"
                    
                 }, failure: { errorCode, errorMsg in
             self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")})
        } else {
            TTLock.setRemoteUnlockSwitchOn(false, lockData: LockData,
                    success: { isOn in
            self.btnName.setTitle("Turn ON", for: .normal)

            self.view.makeToast("Success: RemoteUnlock is Turn Off",position: .center)
                        self.CurrentMode.text = "Off"
                     }, failure: { errorCode, errorMsg in
        self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")})
        }
    }
    

    @IBAction func Back(_ sender: UIBarButtonItem) {
  self.navigationController?.popViewController(animated: true)
    }
    
    func getCurrentMode() {
        
        TTLock.getRemoteUnlockSwitch(withLockData: LockData,
                success: { isOn in
         self.view.hideToastActivity()
            if isOn == true {
        self.CurrentMode.text! = "On"
        self.btnName.setTitle("Turn Off", for: .normal)
            } else {
         self.CurrentMode.text! = "Off"
        self.btnName.setTitle("Turn ON", for: .normal)
            }
        } ,failure: { errorCode, errorMsg in
            self.view.hideToastActivity()
         self.view.makeToast("Operation Failed. Please ensure the Lock is within 2 meters of the Phone. Also, Switch your phone Bluetooth OFF, wait for 10 seconds and Turn Bluetooth ON.")})
        }
    
    func UpdateLockData() {
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let AccessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let date = Date().millisecondsSince1970
     let lockData = UserDefaults.standard.string(forKey: "RemoteLockD")!
                        
        let url = URL(string:self.url.UpdateLockData)!
            var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                let parameters: [String : Any] = [
                    "clientId":ClientId,
                    "accessToken":AccessToken,
                    "lockId":self.LockID,
                    "lockData":lockData,
                    "date":date,
            ]
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
                        
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                    guard let data = data else {
                    if error != nil{
            print(error?.localizedDescription ?? "Unknown Error")
                    }
                return
            }
                if let response = response as? HTTPURLResponse{
            guard (200 ... 299) ~= response.statusCode else {
                print("Status code :- \(response.statusCode)")
                    print(response)
                        return
                    }}
                    do{
                let jsonData = try JSONSerialization.jsonObject(with: data, options:[]) as! NSDictionary
                        print(jsonData)
                        DispatchQueue.main.async {
            self.view.makeToast("Success: RemoteUnlock is Turn On",position: .center)
                        }} catch let error {
                    print(error.localizedDescription)
                        }
                }.resume()
            }
    }
