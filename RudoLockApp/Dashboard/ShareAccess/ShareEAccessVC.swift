//
//  ShareEAccessVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 14/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Parchment

class ShareEAccessVC: UIViewController {
    
    let url = Network()
    
    @IBOutlet weak var ContainerView: UIView!
    
    var LockId = Int()
    
    var KeyId = Int()
    var keyRight = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let first = storyboard.instantiateViewController(withIdentifier: "SharePVC") as! SharePVC
            first.LockID = LockId
            first.KeyID = KeyId
            first.keyRight = keyRight
        let second = storyboard.instantiateViewController(withIdentifier: "ShareOTVC") as!
              ShareOTVC
            second.LockID = LockId
            second.keyRight = keyRight
        let Third = storyboard.instantiateViewController(withIdentifier: "ShareTVC") as! ShareTVC
            Third.LockID = LockId
            Third.KeyID = KeyId
            Third.keyRight = keyRight


        let pagingViewController = PagingViewController(viewControllers: [
          first,second,Third ])

        addChild(pagingViewController)
        ContainerView.addSubview(pagingViewController.view)
        ContainerView.constrainToEdges(pagingViewController.view)
        pagingViewController.didMove(toParent: self)
       
    }
    
    @IBAction func BackBtn(_ sender: UIBarButtonItem) {
        
     self.navigationController?.popViewController(animated: true)
 }}
