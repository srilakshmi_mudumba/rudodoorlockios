//
//  ShareOTVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 14/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class ShareOTVC: UIViewController {
    
    @IBOutlet weak var RecipientNum: UITextField!
    @IBOutlet weak var Name: UITextField!
    
    @IBOutlet weak var btnView: UIView!
    @IBOutlet weak var AuthView: UIView!
    
    
    let url = Network()
        
    var AllowRUnlock = Int()
    
    var LockID = Int()
    var KeyID = Int()
    var keyRight = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
             topStatus()

         if keyRight == Int(1) {
         AuthView.isHidden = true
         } else {
         AuthView.isHidden = false
         }
        
        btnView.layer.cornerRadius = 20
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func AllowRemoteU(_ sender: UISwitch) {
        if sender.isOn {
            AllowRUnlock = Int(1)
        } else {
            AllowRUnlock = Int(2)
    }}
    
    @IBAction func SendBtn(_ sender: UIButton) {
        if RecipientNum.text == "" || Name.text == "" {
         self.view.makeToast("Enter Details", duration: 3.0, position: .center)
            } else {
        self.view.makeToastActivity(.center)
            PostOTEaccess()
    }}
    
    func convertDateInMiliseconds(date: Date) -> Int {
           let since1970 = date.timeIntervalSince1970
           return Int(since1970 * 1000)
    }
    
    func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
        let myAlert = UIAlertController(title:"eAccessShare", message:userMessage, preferredStyle: UIAlertController.Style.alert);
            
        let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
         self.navigationController?.popViewController(animated: true)
       }
          myAlert.addAction(action);
     self.present(myAlert, animated:true, completion:nil);
    }}
}
extension ShareOTVC {

 func PostOTEaccess() {
    
    let clientId = "091f3164004d424abebb4f9c0c822f26"
    let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockID
    let receiverUsername = "rudo_"+RecipientNum.text!
    let keyName = Name.text!
    let startDate = Date()
    let Dt = Date()
    let dateMinus1Hours = Calendar.current.date(byAdding: .hour, value: 1, to: Dt)
    let endDate = convertDateInMiliseconds(date: dateMinus1Hours!)
    let remoteEnable = AllowRUnlock
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.EAccessKey)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
     request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
     let parameters: [String : Any] = [
             "clientId":clientId,
             "accessToken":accessToken,
             "lockId":lockId,
             "receiverUsername":receiverUsername,
             "keyName":keyName,
             "startDate":startDate,
             "endDate":dateMinus1Hours,
             "remarks": "OneTime",
             "remoteEnable":remoteEnable,
             "date":date,
     ]
 request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
    if let response = response as? HTTPURLResponse{
         guard (200 ... 299) ~= response.statusCode else {
        print("Status code :- \(response.statusCode)")
          print(response)
         return
        }}
     do{
        let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print(json)
       if json.object(forKey: "errmsg") as! String == "Invalid User Name." {
         DispatchQueue.main.async {
        self.view.hideToastActivity()
         self.view.makeToast("Invalid Recipient", duration: 4.0, position: .center)
      }}
       else if json.object(forKey: "errmsg") as! String == "You cannot send an eKey to Yourself." {
           DispatchQueue.main.async {
                  self.view.hideToastActivity()
           self.view.makeToast("You cannot send an eKey to Yourself.", duration: 4.0, position: .center)
      }} else {
         DispatchQueue.main.async {
       self.view.hideToastActivity()
       self.displayAlertMessage(userMessage: "Share eAccess Successfully")
     }
     }} catch let error{
         print(error.localizedDescription)
        }
    }.resume()
}}


