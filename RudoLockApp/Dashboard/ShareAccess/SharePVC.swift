//
//  SharePVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 14/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift
import TTLock

class SharePVC: UIViewController {
   
    let url = Network()
    
    @IBOutlet weak var BtnView: UIView!
    
    @IBOutlet weak var RecipientTxt: UITextField!
    @IBOutlet weak var Name: UITextField!
    
    @IBOutlet weak var AuthView: UIView!
    
    
    var AllowRemoteUnlock = Int()
    var Auth = String()
    
    var LockID = Int()
    var KeyID = Int()
    var keyRight = Int()
    
    var KeyIDAuth = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()
            topStatus()

        if keyRight == Int(1) {
        AuthView.isHidden = true
        } else {
        AuthView.isHidden = false
        }
        BtnView.layer.cornerRadius = 20

    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func AllowRemoteUnlocking(_ sender: UISwitch) {
        
        if sender.isOn {
            AllowRemoteUnlock = Int(1)
     } else {
          AllowRemoteUnlock = Int(2)
        }
    }
    
    @IBAction func AuthorisedAdmin(_ sender: UISwitch) {
        
      if sender.isOn {
          Auth = "isOn"
     } else {
          Auth = "isOFF"
        }
    }
    
    @IBAction func SendB(_ sender: UIButton) {
      
        if RecipientTxt.text == "" || Name.text == "" {
     self.view.makeToast("Enter Details", duration: 3.0, position: .center)
        } else {
    self.view.makeToastActivity(.center)
          PostPEaccess()
    }}
    
  func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
        let myAlert = UIAlertController(title:"eAccessShare", message:userMessage, preferredStyle: UIAlertController.Style.alert);
            
        let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
         self.navigationController?.popViewController(animated: true)
       }
          myAlert.addAction(action);
     self.present(myAlert, animated:true, completion:nil);
    }}

}
extension SharePVC {
   
    func PostPEaccess() {
       
       let clientId = "091f3164004d424abebb4f9c0c822f26"
       let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
       let lockId = LockID
       let receiverUsername = "rudo_"+RecipientTxt.text!
       let keyName = Name.text!
       let startDate = Int(0)
       let endDate =  Int(0)
       let remoteEnable = AllowRemoteUnlock
       let date = Date().millisecondsSince1970
       
        let url = URL(string:self.url.EAccessKey)!
               var request = URLRequest(url: url)
               request.httpMethod = "POST"
        request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        let parameters: [String : Any] = [
                "clientId":clientId,
                "accessToken":accessToken,
                "lockId":lockId,
                "receiverUsername":receiverUsername,
                "keyName":keyName,
                "startDate":startDate,
                "endDate":endDate,
                "remoteEnable":remoteEnable,
                "date":date,
        ]
       
    request.httpBody = parameters.percentEscaped().data(using: .utf8)
       
       URLSession.shared.dataTask(with: request) { (data, response, error) in
               guard let data = data else {
               if error == nil{
           print(error?.localizedDescription ?? "Unknown Error")
              }
               return
          }
    if let response = response as? HTTPURLResponse{
        guard (200 ... 299) ~= response.statusCode else {
        print("Status code :- \(response.statusCode)")
        print(response)
            return
        }}
     do{
         let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
               print(json)
     if json.object(forKey: "errmsg") as! String == "Invalid User Name." {
            DispatchQueue.main.async {
           self.view.hideToastActivity()
    self.view.makeToast("Invalid Recipient", duration: 4.0, position: .center)
    }}
     else if json.object(forKey: "errmsg") as! String == "You cannot send an eKey to Yourself." {
        DispatchQueue.main.async {
               self.view.hideToastActivity()
        self.view.makeToast("You cannot send an eKey to Yourself.", duration: 4.0, position: .center)
     }} else {
        self.KeyIDAuth = json.object(forKey: "keyId") as! Int
         if self.Auth == "isOn" {
            print(self.KeyIDAuth)
            self.PostAdminAccess()
         } else if self.Auth == "isOFF" {
            self.PostAdminCancelAccess()
         }
            DispatchQueue.main.async {
          self.view.hideToastActivity()
    self.displayAlertMessage(userMessage: "Share eAccess Successfully")
        }
    }} catch let error{
       print(error.localizedDescription)
           }
       }.resume()
   }
    
 func PostAdminAccess() {
    
    
    let clientId = "091f3164004d424abebb4f9c0c822f26"
    let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockID
    let keyId = KeyIDAuth
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.KeyAdminAutorized)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
     request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
     let parameters: [String : Any] = [
             "clientId":clientId,
             "accessToken":accessToken,
             "lockId":lockId,
             "keyId":keyId,
             "date":date,
     ]
    
 request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
  if let response = response as? HTTPURLResponse{
      guard (200 ... 299) ~= response.statusCode else {
      print("Status code :- \(response.statusCode)")
      print(response)
         return
     }}
  do{
      let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print(json)
        } catch let error{
    print(error.localizedDescription)
        }
    }.resume()
}

  func PostAdminCancelAccess() {
    
    let clientId = "091f3164004d424abebb4f9c0c822f26"
    let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockID
    let keyId = KeyIDAuth
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.CancelKeyAdminAuthorized)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
     request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
     let parameters: [String : Any] = [
             "clientId":clientId,
             "accessToken":accessToken,
             "lockId":lockId,
             "keyId":keyId,
             "date":date,
     ]
    
 request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
  if let response = response as? HTTPURLResponse{
      guard (200 ... 299) ~= response.statusCode else {
      print("Status code :- \(response.statusCode)")
      print(response)
         return
     }}
  do{
      let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print(json)
        } catch let error{
    print(error.localizedDescription)
        }
    }.resume()
}}







