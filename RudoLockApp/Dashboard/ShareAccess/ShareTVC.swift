//
//  ShareTVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 14/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class ShareTVC: UIViewController {
    
    @IBOutlet weak var Recipient: UITextField!
    @IBOutlet weak var Name: UITextField!
    @IBOutlet weak var StartTime: UITextField!
    @IBOutlet weak var EndTime: UITextField!
    
    @IBOutlet weak var AuthView: UIView!
    
         let url = Network()
       var AllowRUnlock = Int()
     
       var datePicker = UIDatePicker()
       var datePic2 = UIDatePicker()
    
       var LockID = Int()
       var KeyID = Int()
       var keyRight = Int()
    
       var Auth = String()
       var KeyIDAuth = Int()
    
    @IBOutlet weak var btnView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
           topStatus()
        btnView.layer.cornerRadius = 20
        
        if keyRight == Int(1){
        AuthView.isHidden = true
        } else {
        AuthView.isHidden = false
     }
        
        let tap = UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing))
        view.addGestureRecognizer(tap)
               
          creatdatePicker()
          creatdatePicker2()
   }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    func creatdatePicker() {
         datePicker.datePickerMode = .dateAndTime
           datePicker.backgroundColor = .white
          datePicker.minimumDate = Date()

           
           let toolbar = UIToolbar()
           toolbar.sizeToFit()
           toolbar.barTintColor = .white
           
           
           let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneac))
           toolbar.setItems([dponebutton], animated: true)
           StartTime.inputAccessoryView = toolbar
           StartTime.inputView = datePicker
       }
       
       @objc func doneac() {
        
          let formatter = DateFormatter()
          formatter.timeStyle = DateFormatter.Style.short
          formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
        StartTime.text = formatter.string(from: datePicker.date)
           self.view.endEditing(true)
       }
       
       func creatdatePicker2() {
           datePic2.datePickerMode = .dateAndTime
           datePic2.backgroundColor = .white
           datePic2.minimumDate = Date()

           let toolbar = UIToolbar()
           toolbar.sizeToFit()
           toolbar.barTintColor = .white
           
           
           let dponebutton = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(done))
           toolbar.setItems([dponebutton], animated: true)
           EndTime.inputAccessoryView = toolbar
           EndTime.inputView = datePic2
       }
       
    @objc func done() {
        let formatter = DateFormatter()
        formatter.timeStyle = DateFormatter.Style.short
        formatter.dateFormat = "YYYY-MM-dd HH:mm a"
        
        if formatter.string(from: datePicker.date) >= formatter.string(from: datePic2.date) {
            DispatchQueue.main.async {
         self.view.makeToast("Invalid ! End Time should be greater than Start Time")
            }
        } else {
        EndTime.text = formatter.string(from: datePic2.date)
        self.view.endEditing(true)
        }}
    
    func convertDateInMiliseconds(date: Date) -> Int {
        let since1970 = date.timeIntervalSince1970
        return Int(since1970 * 1000)
    }
          
    
    @IBAction func AllowRemote(_ sender: UISwitch) {
      
        if sender.isOn {
                AllowRUnlock = Int(1)
            } else {
                AllowRUnlock = Int(2)
        }}
    
    @IBAction func AuthorizedAdmin(_ sender: UISwitch) {
        if sender.isOn {
             Auth = "isOn"
        } else {
             Auth = "isOFF"
           }
     }
    
    @IBAction func SendBtn(_ sender: UIButton) {
    if StartTime.text! == "" || EndTime.text! == "" {
        self.view.makeToast("Select Dates", duration: 3.0, position: .center)
    } else if Name.text == "" || Recipient.text == "" {
        self.view.makeToast("Enter Details", duration: 3.0, position: .center)
    } else {
        self.view.makeToastActivity(.center)
           PostTEaccess()
    }}
    
   func displayAlertMessage(userMessage:String) {
        DispatchQueue.main.async{
    let myAlert = UIAlertController(title:"eAccessShare", message:userMessage, preferredStyle: UIAlertController.Style.alert);
                
    let action = UIAlertAction(title: "Ok", style: .default) { (UIAlertAction) in
    self.navigationController?.popViewController(animated: true)
        }
        myAlert.addAction(action);
    self.present(myAlert, animated:true, completion:nil);
    }}
}
extension ShareTVC {

 func PostTEaccess() {
    
    let clientId = "091f3164004d424abebb4f9c0c822f26"
    let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
    let lockId = LockID
    let receiverUsername = "rudo_"+Recipient.text!
    let keyName = Name.text!
    let startDate = convertDateInMiliseconds(date:datePicker.date)
    let endDate = convertDateInMiliseconds(date:datePic2.date)
    let remoteEnable = AllowRUnlock
    let date = Date().millisecondsSince1970
    
     let url = URL(string:self.url.EAccessKey)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
     request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
     let parameters: [String : Any] = [
             "clientId":clientId,
             "accessToken":accessToken,
             "lockId":lockId,
             "receiverUsername":receiverUsername,
             "keyName":keyName,
             "startDate":datePicker.date,
             "endDate":datePic2.date,
             "remoteEnable":remoteEnable,
             "date":date,
     ]
 request.httpBody = parameters.percentEscaped().data(using: .utf8)
    
    URLSession.shared.dataTask(with: request) { (data, response, error) in
            guard let data = data else {
            if error == nil{
        print(error?.localizedDescription ?? "Unknown Error")
           }
            return
       }
    if let response = response as? HTTPURLResponse{
         guard (200 ... 299) ~= response.statusCode else {
        print("Status code :- \(response.statusCode)")
          print(response)
         return
        }}
     do{
        let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            print(json)
       if json.object(forKey: "errmsg") as! String == "Invalid User Name." {
         DispatchQueue.main.async {
        self.view.hideToastActivity()
         self.view.makeToast("Invalid Recipient", duration: 4.0, position: .center)
      }}
       else if json.object(forKey: "errmsg") as! String == "You cannot send an eKey to Yourself." {
                 DispatchQueue.main.async {
                        self.view.hideToastActivity()
                 self.view.makeToast("You cannot send an eKey to Yourself.", duration: 4.0, position: .center)
        }} else {
        self.KeyIDAuth = json.object(forKey: "keyId") as! Int
             if self.Auth == "isOn" {
             self.PostAdminAccess()
        } else if self.Auth == "isOFF" {
            self.PostAdminCancelAccess()
            }
         DispatchQueue.main.async {
       self.view.hideToastActivity()
       self.displayAlertMessage(userMessage: "Share eAccess Successfully")
     }
     }} catch let error{
         print(error.localizedDescription)
        }
    }.resume()
}
    
    func PostAdminAccess() {
        
        let clientId = "091f3164004d424abebb4f9c0c822f26"
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockID
        let keyId = KeyIDAuth
        let date = Date().millisecondsSince1970
        
         let url = URL(string:self.url.KeyAdminAutorized)!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
         request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
         let parameters: [String : Any] = [
                 "clientId":clientId,
                 "accessToken":accessToken,
                 "lockId":lockId,
                 "keyId":keyId,
                 "date":date,
         ]
        
     request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                if error == nil{
            print(error?.localizedDescription ?? "Unknown Error")
               }
                return
           }
      if let response = response as? HTTPURLResponse{
          guard (200 ... 299) ~= response.statusCode else {
          print("Status code :- \(response.statusCode)")
          print(response)
             return
         }}
      do{
          let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
            } catch let error{
        print(error.localizedDescription)
            }
        }.resume()
 }

  func PostAdminCancelAccess() {
        
        let clientId = "091f3164004d424abebb4f9c0c822f26"
        let accessToken = UserDefaults.standard.string(forKey: "accessToken")!
        let lockId = LockID
        let keyId = KeyIDAuth
        let date = Date().millisecondsSince1970
        
         let url = URL(string:self.url.CancelKeyAdminAuthorized)!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
         request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
         let parameters: [String : Any] = [
                 "clientId":clientId,
                 "accessToken":accessToken,
                 "lockId":lockId,
                 "keyId":keyId,
                 "date":date,
         ]
        
     request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                if error == nil{
            print(error?.localizedDescription ?? "Unknown Error")
               }
                return
           }
      if let response = response as? HTTPURLResponse{
          guard (200 ... 299) ~= response.statusCode else {
          print("Status code :- \(response.statusCode)")
          print(response)
             return
         }}
      do{
          let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
            } catch let error{
        print(error.localizedDescription)
            }
        }.resume()
}}





