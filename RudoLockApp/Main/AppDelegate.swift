//
//  AppDelegate.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 13/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import TTLock
import TTLockGateway
import TTLockDFU

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

      var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self as? UNUserNotificationCenterDelegate

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
          application.registerUserNotificationSettings(settings)
        }

        application.registerForRemoteNotifications()

       if #available(iOS 13.0, *) {

            let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
        statusBar.backgroundColor = UIColor.red
            statusBar.tag = 100
            UIApplication.shared.keyWindow?.addSubview(statusBar)

    } else {

            let statusBar = UIApplication.shared.value(forKeyPath: "statusBarWindow.statusBar") as? UIView
        statusBar?.backgroundColor = UIColor.red

        }
       TTLock.setupBluetooth({ status in
                print(status)
            })

        splashScreen()
        return true
    }
    
//      func application(_ application: UIApplication,
//            didReceiveRemoteNotification notification: [AnyHashable : Any],
//            fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
//        if Auth.auth().canHandleNotification(notification) {
//                   completionHandler(.noData)
//                   return
//               }
//    }
    
    
    
    func splashScreen() {
        
    let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

        let launch = UIStoryboard.init(name: "LaunchScreen", bundle: nil)
        let rootVC = launch.instantiateViewController(withIdentifier: "LaunchScreen")
        appDel.window?.rootViewController = rootVC
        appDel.window?.makeKeyAndVisible()
        Timer.scheduledTimer(timeInterval: 5, target: self, selector: #selector(Check), userInfo: nil, repeats: false)
    }
    
     
    @objc func Check() {
    
        let appDel:AppDelegate = UIApplication.shared.delegate as! AppDelegate

       let ID =   UserDefaults.standard.string(forKey: "accessToken")
               if ID != nil {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "AppAuthenticationVC") as! AppAuthenticationVC
        let navigationController = UINavigationController(rootViewController: newViewController)
        //     let appdelegate = UIApplication.shared.delegate as! AppDelegate
            appDel.window?.rootViewController = navigationController
            navigationController.isNavigationBarHidden = true
            appDel.window?.makeKeyAndVisible()
        
               } else {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
            let newViewController2 = storyBoard.instantiateViewController(withIdentifier: "LogInVC") as! LogInVC
        let navigationController = UINavigationController(rootViewController: newViewController2)
            appDel.window?.rootViewController = navigationController
                navigationController.isNavigationBarHidden = true
            appDel.window?.makeKeyAndVisible()
        }

 }
    

}


