//
//  LaunchScreen.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 11/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit

class LaunchScreen: UIViewController {

    @IBOutlet weak var Image: UIImageView!
    @IBOutlet weak var BackV: UIView!
        
    var pulseLayers = [CAShapeLayer]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

       BackV.layer.cornerRadius = BackV.frame.height/2
              Image.layer.borderWidth = 1
              Image.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
              Image.layer.cornerRadius = Image.frame.height/2
        
//        let layer = PulsingHaloLayer()
//        halo = layer
//        Image.superview!.layer.insertSublayer(halo, below: Image.layer)
//
//        halo.start()
//        halo.haloLayerNumber = 3
//        halo.radius = 240.0
//        halo.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)

        createPulse()
    }

    
    func createPulse() {
        for _ in 0...2 {
        let circularPath = UIBezierPath(arcCenter: .zero, radius: UIScreen.main.bounds.size.width/2.0, startAngle: 0, endAngle: 2 * .pi, clockwise: true)
        let pulseLayer = CAShapeLayer()
        pulseLayer.path = circularPath.cgPath
        pulseLayer.lineWidth = 2.0
        pulseLayer.fillColor = UIColor.white.cgColor
        pulseLayer.strokeColor = UIColor.white.cgColor
        pulseLayer.lineCap = CAShapeLayerLineCap.round
        pulseLayer.position = CGPoint(x: BackV.frame.size.width/2.0 , y: BackV.frame.size.width/2.0 )
            BackV.layer.insertSublayer(pulseLayer, below: Image.layer)
            pulseLayers.append(pulseLayer)
        }
        animatePulse(index: 1)
        animatePulse(index: 2)

        DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
            self.animatePulse(index: 0)
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.4) {
            self.animatePulse(index: 1)
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.animatePulse(index: 2)
        }}}
    }

    func animatePulse(index:Int) {
        let scaleAnimation = CABasicAnimation(keyPath: "transform.scale")
        scaleAnimation.duration = 2.0
        scaleAnimation.fromValue = 0.0
        scaleAnimation.toValue = 0.9
        scaleAnimation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        scaleAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayers[index].add(scaleAnimation,forKey: "scale")

        let opacityAnimation = CABasicAnimation(keyPath: #keyPath(CALayer.opacity))
        opacityAnimation.duration = 2.0
        opacityAnimation.fromValue = 0.9
        opacityAnimation.toValue = 0.0
        opacityAnimation.timingFunction = CAMediaTimingFunction(name:CAMediaTimingFunctionName.easeOut)
        opacityAnimation.repeatCount = .greatestFiniteMagnitude
        pulseLayers[index].add(opacityAnimation,forKey: "opacity")

    }


}
