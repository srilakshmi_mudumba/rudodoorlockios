//
//  NetwokUrLFile.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 18/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import Foundation
import SystemConfiguration

class Utils : NSObject {
    

    static func share()
    {
        
    }
    
    static func isInternetAvailable() -> Bool
    {
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {zeroSockAddress in
                SCNetworkReachabilityCreateWithAddress(nil, zeroSockAddress)
            }
        }
        
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
}

class Network {
    
    // OAuth
 let Register = "https://api.ttlock.com/v3/user/register"
    
 let LogIn = "https://api.ttlock.com/oauth2/token"
    
 let GetLockList = "https://api.ttlock.com/v3/lock/list"
    
 let GetLockDetails = "https://api.ttlock.com/v3/lock/detail"
    
 let GetPasscodeP = "https://api.ttlock.com/v3/keyboardPwd/get"
    
 let GetCustomPasscode = "https://api.ttlock.com/v3/keyboardPwd/add"
    
 let GetPasscodeList = "https://api.ttlock.com/v3/lock/listKeyboardPwd"
    
 let DeletePasscode = "https://api.ttlock.com/v3/keyboardPwd/delete"
    
 let RecordList = "https://api.ttlock.com/v3/lockRecord/list"
    
 let IcCardList = "https://api.ttlock.com/v3/identityCard/list"
 
 let FingerPrintList = "https://api.ttlock.com/v3/fingerprint/list"
    
 let EAccessKey = "https://api.ttlock.com/v3/key/send"
 
 let KeyList = "https://api.ttlock.com/v3/key/list"
    
 let KeyAdminAutorized = "https://api.ttlock.com/v3/key/authorize"
    
 let CancelKeyAdminAuthorized = "https://api.ttlock.com/v3/key/unauthorize"
    
 let EAccessKeyList = "https://api.ttlock.com/v3/lock/listKey"
    
 let DeleteEaccessKey = "https://api.ttlock.com/v3/key/delete"
    
 let DeleteFingerPrint = "https://api.ttlock.com/v3/fingerprint/delete"
    
 let DeleteCard = "https://api.ttlock.com/v3/identityCard/delete"
    
 let ClearICCard = "https://api.ttlock.com/v3/identityCard/clear"
    
 let ClearFingerPrint = "https://api.ttlock.com/v3/fingerprint/clear"
    
 let AddICcard = "https://api.ttlock.com/v3/identityCard/add"
    
 let AddFingerPrint = "https://api.ttlock.com/v3/fingerprint/add"
    
 let DeleteUserAccount = "https://api.ttlock.com/v3/user/delete"
 
 let AutoLock = "https://api.ttlock.com/v3/lock/setAutoLockTime"
    
 let RecordUplode = "https://api.ttlock.com/v3/lockRecord/upload"
 
 let DeleteLock = "https://api.ttlock.com/v3/lock/delete"
    
 let ForgotPassword = "https://api.ttlock.com/v3/user/resetPassword"
    //
    
 let lockInit = "https://api.ttlock.com/v3/lock/initialize"
    
 let SetAutoTime = "https://api.ttlock.com/v3/lock/setAutoLockTime"
    
 let AccountGatewayList = "https://api.ttlock.com/v3/gateway/list"
    
    
    //Gateway
    
    let GetUid = "https://api.ttlock.com/v3/user/getUid"
    
    let InitGateway = "https://api.ttlock.com/v3/gateway/isInitSuccess"
    
    let UplodeGatewayDetails = "https://api.ttlock.com/v3/gateway/uploadDetail"
    
    let UnlockGateway = "https://api.ttlock.com/v3/lock/unlock"
    
    let LockGateway = "https://api.ttlock.com/v3/lock/lock"
    
    let DeleteGateway = "https://api.ttlock.com/v3/gateway/delete"
    
    let UpdateLockData = "https://api.ttlock.com/v3/lock/updateLockData"

}
