//
//  AppAuthenticationVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 05/09/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import LocalAuthentication
import SGDigitTextField

class AppAuthenticationVC: UIViewController,UITextFieldDelegate{
    
    
    @IBOutlet weak var ImageLock: UIImageView!
    @IBOutlet weak var LblTitle: UILabel!
    
    @IBOutlet weak var Box1: SGDigitTextField!
    
    var UN = UserDefaults.standard.string(forKey: "CretedPass")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        ImageLock.layer.cornerRadius = ImageLock.frame.height/2
        ImageLock.layer.borderWidth = 1
        ImageLock.layer.borderColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        
        Box1.keyboardType = .numberPad
        self.Box1.becomeFirstResponder()
        
        Box1.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged) 
        
        
        Auth()
        
        if UN != nil {
            LblTitle.text = "Enter Passcode"
        } else {
            LblTitle.text = "Create Passcode"
        }
    }
    
    @objc func textFieldDidChange(_ textField: UITextField) {
        if textField.text!.count  == 4 {
            if  UN != nil {
                Verify()
            } else {
                Create()
            }
        }
    }
    
    func Create() {
        let Passcode =  Box1.text!
        UserDefaults.standard.set(Passcode, forKey: "CretedPass")
        DispatchQueue.main.async {
            let Home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
            self.navigationController?.pushViewController(Home, animated: true)
        }
    }
    
    func Verify() {
        
        
        if Box1.text! == UserDefaults.standard.string(forKey: "CretedPass") {
            DispatchQueue.main.async {
                let Home = self.storyboard?.instantiateViewController(withIdentifier: "HomeVC") as! HomeVC
                self.navigationController?.pushViewController(Home, animated: true)
            }  } else {
                Box1.reset()
                self.view.makeToast("Enter Correct Passcode",duration: 0.7,position: .center)
            }}
    
    
    
    func Auth() {
        let authenticationObject = LAContext()
        var authError:NSError!
        
        authenticationObject.canEvaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, error: &authError)
        if authError != nil {
            print("Auth not available")
        } else {
            authenticationObject.evaluatePolicy(LAPolicy.deviceOwnerAuthenticationWithBiometrics, localizedReason: "Access your App with a touch of your finger!", reply: { (complete:Bool, error: Error!) -> Void in
                
                if error != nil {
                    print(error.localizedDescription)
                } else {
                    if complete == true {
                        print("auth success")
                        DispatchQueue.main.async {
                            let SignUpW = self.storyboard?.instantiateViewController(withIdentifier:"HomeVC") as! HomeVC
                            self.navigationController?.pushViewController(SignUpW, animated:true)
                        }} else {
                            self.navigationController?.popViewController(animated: true)
                        }}
            })}
        
    }
}
