//
//  LogInVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 13/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift

class LogInVC: UIViewController {
    
    let url = Network()
    
    var accessToken = String()
    
    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    
    @IBOutlet weak var IconView: UIView!
    @IBOutlet weak var IconImg: UIImageView!
    
    
    @IBOutlet weak var EnterUsername: UITextField!
    @IBOutlet weak var Password: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        View1.layer.cornerRadius = 20
        View2.layer.cornerRadius = 20
        View3.layer.cornerRadius = 20
        
    }
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    @IBAction func SignIn(_ sender: UIButton) {
        
        if EnterUsername.text == "" || Password.text == "" {
            self.view.makeToast("Enter your Login details", duration: 3.0, position: .bottom)
        } else {
            self.view.makeToastActivity(.center)
            PostLogIn()
        }}
    
    @IBAction func CircleSelected(_ sender: UIButton) {
        
        if sender.isSelected {
            sender.isSelected = false
            Password.isSecureTextEntry = false
        } else {
            sender.isSelected = true
            Password.isSecureTextEntry = true
            
        }}
    
    
    
    @IBAction func GoToCreatB(_ sender: UIButton) {
        let SignUpW = self.storyboard?.instantiateViewController(withIdentifier:"SignUpVC") as! SignUpVC
        self.navigationController?.pushViewController(SignUpW, animated:true)
    }
    
    @IBAction func ForgotPassword(_ sender: UIButton) {
        
        let Forgot = self.storyboard?.instantiateViewController(withIdentifier:"ForgotPassVC") as! ForgotPassVC
        self.navigationController?.pushViewController(Forgot, animated:true)
    }
    
}
extension LogInVC {
    
    func PostLogIn() {
        if Utils.isInternetAvailable() {
            
            let Username = EnterUsername.text!
            UserDefaults.standard.set(Username, forKey: "Username")
            let password = Password.text!.md5
            UserDefaults.standard.set(password, forKey: "Password")
            let ClientId = "091f3164004d424abebb4f9c0c822f26"
            let ClientSecret = "3c815de27069538163d6c776036a8b3c"
            let GrantTy = "password"
            let Url = "yutuelectronics.com"
            
            let url = URL(string:self.url.LogIn)!
            var request = URLRequest(url: url)
            request.httpMethod = "POST"
            request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
            let parameters: [String : Any] = [
                "client_id":ClientId,
                "client_secret":ClientSecret,
                "grant_type":GrantTy,
                "username": "rudo_"+Username,
                "password":password,
                "redirect_uri":Url,
            ]
            
            request.httpBody = parameters.percentEscaped().data(using: .utf8)
            
            URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                    if error != nil{
                        self.view.hideToastActivity()
                        print(error?.localizedDescription ?? "Unknown Error")
                    }
                    return
                }
                if let response = response as? HTTPURLResponse{
                    guard (200 ... 299) ~= response.statusCode else {
                        print("Status code :- \(response.statusCode)")
                        print(response)
                        return
                    }}
                do{
                    let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                    print(json)
                    if json.object(forKey: "errmsg") as? String == "invalid account or invalid password" {
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            self.view.makeToast("Login failed", duration: 3.0, position: .bottom)
                        }
                    } else {
                        self.accessToken = json.object(forKey: "access_token") as! String
                        UserDefaults.standard.set(self.accessToken, forKey: "accessToken")
                        
                        let Uid = json.object(forKey: "uid") as! Int
                        UserDefaults.standard.set(Uid, forKey: "uid")
                        
                        let openid = json.object(forKey: "openid") as! Int
                        let OId = openid.description
                        UserDefaults.standard.set(OId, forKey: "openId")
                        
                        let refresh_token = json.object(forKey: "refresh_token") as! String
                        UserDefaults.standard.set(refresh_token, forKey: "refreshToken")
                        
                        //               let expires_in = json.object(forKey: "expires_in") as! String
                        //            UserDefaults.standard.set(expires_in, forKey: "expires")
                        
                        DispatchQueue.main.async {
                            self.view.hideToastActivity()
                            let SignUpW = self.storyboard?.instantiateViewController(withIdentifier:"AppAuthenticationVC") as! AppAuthenticationVC
                            // SignUpW.UN = self.accessToken
                            self.navigationController?.pushViewController(SignUpW, animated:true)
                            
                        }
                    }
                } catch let error{
                    print(error.localizedDescription)
                }
            }.resume()
        } else{
            self.view.hideToastActivity()
            self.view.makeToast("Check Your Network Connection")
        } }
}

extension Dictionary {
    func percentEscaped() -> String {
        return map { (key, value) in
            let escapedKey = "\(key)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            let escapedValue = "\(value)".addingPercentEncoding(withAllowedCharacters: .urlQueryValueAllowed) ?? ""
            return escapedKey + "=" + escapedValue
        }
        .joined(separator: "&")
    }
}
extension CharacterSet {
    static let urlQueryValueAllowed: CharacterSet = {
        let generalDelimitersToEncode = ":#[]@" // does not include "?" or "/" due to RFC 3986 - Section 3.4
        let subDelimitersToEncode = "!$&'()*+,;="
        
        var allowed = CharacterSet.urlQueryAllowed
        allowed.remove(charactersIn: "\(generalDelimitersToEncode)\(subDelimitersToEncode)")
        return allowed
    }()
}
