//
//  SignUpVC.swift
//  RudoLockApp
//
//  Created by AkanshaDongre  on 13/08/20.
//  Copyright © 2020 AkanshaDongre . All rights reserved.
//

import UIKit
import Toast_Swift
import CommonCrypto

class SignUpVC: UIViewController,UITextFieldDelegate {

    @IBOutlet weak var View1: UIView!
    @IBOutlet weak var View2: UIView!
    @IBOutlet weak var View3: UIView!
    @IBOutlet weak var View4: UIView!
    @IBOutlet weak var View5: UIView!
    
    @IBOutlet weak var IconView: UIView!
    @IBOutlet weak var IconImg: UIImageView!
    
    
    @IBOutlet weak var Username: UITextField!
    @IBOutlet weak var MobileNumber: UITextField!
    @IBOutlet weak var Password: UITextField!
    @IBOutlet weak var ConformPass: UITextField!
    
    let url = Network()
    
    var isCircleSelected = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        View1.layer.cornerRadius = 20
        View2.layer.cornerRadius = 20
        View3.layer.cornerRadius = 20
        View4.layer.cornerRadius = 20
        View5.layer.cornerRadius = 20
        
        MobileNumber.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    
    @IBAction func SignUpButton(_ sender: UIButton) {
        if Username.text == "" || Password.text == "" || ConformPass.text == "" || MobileNumber.text == "" {
    self.view.makeToast("Enter your details", duration: 3.0, position: .bottom)
    } else if Password.text!.count <= 5 {
    self.view.makeToast("Password must be > 5 characters", duration: 3.0, position: .bottom)
      } else if ConformPass.text != Password.text {
    self.view.makeToast("confirm password and password are not same", duration: 3.0, position: .bottom)
        }
        else if(!isCircleSelected) {
    self.view.makeToast("Select License Agreement")
        } else {
        self.view.makeToastActivity(.center)
       PostRegistration()
    }}
    
    @IBAction func GoToLogIn(_ sender: UIButton) {
     self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func CircleSelected(_ sender: UIButton) {
           
           if sender.isSelected {
            sender.isSelected = false
       isCircleSelected = sender.isSelected
            } else {
            sender.isSelected = true
        isCircleSelected = sender.isSelected
                         }
       }
    
    @IBAction func PassEye(_ sender: UIButton) {
       if sender.isSelected {
            sender.isSelected = false
        Password.isSecureTextEntry = false
          } else {
            sender.isSelected = true
        Password.isSecureTextEntry = true
    }}
    
    @IBAction func ConfoEye(_ sender: UIButton) {
       if sender.isSelected {
           sender.isSelected = false
       ConformPass.isSecureTextEntry = false
         } else {
           sender.isSelected = true
       ConformPass.isSecureTextEntry = true
    }}
    
    
    @IBAction func AgreementUrlBtn(_ sender: UIButton) {
        
        if let url = URL(string: "https://yutuelectronics.com/eula/") {
            if #available(iOS 10, *){
                UIApplication.shared.open(url)
            }else{
                UIApplication.shared.openURL(url)
        }
}}
    
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let updatedString = (textField.text! as NSString).replacingCharacters(in: range, with: string)
        return updatedString.count <= 10
    }
    
    
    func currentDateTimeInMiliseconds() -> Int {
          let currentDate = Date()
          let since1970 = currentDate.timeIntervalSince1970
          return Int(since1970 * 1000)
    }
    
    

}

extension String {
    var isValidContact: Bool {
        let phoneNumberRegex = "^[6-9]\\d{9}$"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", phoneNumberRegex)
        let isValidPhone = phoneTest.evaluate(with: self)
        return isValidPhone
    }
}
extension SignUpVC {
    
    func PostRegistration() {
        
        if Utils.isInternetAvailable() {
        
        let username = MobileNumber.text!
        let password = Password.text!.md5
        
        let ClientId = "091f3164004d424abebb4f9c0c822f26"
        let ClientSecret = "3c815de27069538163d6c776036a8b3c"
        let date =  currentDateTimeInMiliseconds()
        
         let url = URL(string:self.url.Register)!
                var request = URLRequest(url: url)
                request.httpMethod = "POST"
                request.addValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
                let parameters: [String : Any] = [
                    "clientId":ClientId,
                    "clientSecret":ClientSecret,
                    "username":username,
                    "password":password,
                    "date":date,
                ]
        
        request.httpBody = parameters.percentEscaped().data(using: .utf8)
        
        URLSession.shared.dataTask(with: request) { (data, response, error) in
                guard let data = data else {
                if error == nil{
            print(error?.localizedDescription ?? "Unknown Error")
               }
                return
           }
            if let response = response as? HTTPURLResponse{
            guard (200 ... 299) ~= response.statusCode else {
                print("Status code :- \(response.statusCode)")
                print(response)
                return
             }}
      do{
          let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
                print(json)
        if json.object(forKey: "errmsg") as? String == "existing registered users" {
            DispatchQueue.main.async {
             self.view.hideToastActivity()
        self.view.makeToast("User alredy exist go to Login",duration:3.0,position:.center)
            }
        } else {
            DispatchQueue.main.async {
             self.view.hideToastActivity()
            let SignUpW = self.storyboard?.instantiateViewController(withIdentifier:"LogInVC") as! LogInVC
        self.navigationController?.pushViewController(SignUpW, animated:true)
                }
        } } catch let error{
        print(error.localizedDescription)
            }
        }.resume()
        }else {
            self.view.hideToastActivity()
            self.view.makeToast("Check Your Network Connection")
        }}}

extension String {
var md5: String {
    let data = Data(self.utf8)
    let hash = data.withUnsafeBytes { (bytes: UnsafeRawBufferPointer) -> [UInt8] in
        var hash = [UInt8](repeating: 0, count: Int(CC_MD5_DIGEST_LENGTH))
        CC_MD5(bytes.baseAddress, CC_LONG(data.count), &hash)
        return hash
    }
    return hash.map { String(format: "%02x", $0) }.joined()
}}
